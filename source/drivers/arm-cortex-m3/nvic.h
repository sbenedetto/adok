/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __NVIC_H__
#define __NVIC_H__

#include <stdint.h>

/**
 * \brief Nested Vectored Interrupt Controller class.
 *
 * The followig class implements the basic features common to
 * all Cortex-M3 NVIC. Every SoC must extend this class.
 */
namespace NVIC
{
  void  activeDelayMS(uint32_t delayMS);
  void  activeDelayUS(uint32_t delayUS);

  /**
   * \brief disable interrupt
   *
   * \param number number of the interrupt to disable
   */
  void   disableIRQ(uint8_t number);

  /**
   * \brief disable SysTick timer
   */
  void   disableSysTick();

  /**
   * \brief enable interrupt
   *
   * \param number number of the interrupt to enable  
   */
  void   enableIRQ(uint8_t number);

  /**
   * \brief enable SysTick timer along with interrupt generation
   */
  void   enableSysTick(uint32_t reloadValueInMS);

  /**
   * \brief get current SysTick timer value
   */
   uint32_t   currentSysTickValue();
   uint32_t   currentSysTickValueMS();

  /**
   * \brief Set pending PendSV special interrupt
   */
  void   setPendSV();

  /**
   * \brief Clear pending PendSV special interrupt
   */
  void   clearPendSV();

  /**
   * \brief set pending interrupt
   *
   * \param number number of the interrupt to set as pending  
   */
  void   setPendingIRQ(uint8_t number);

  /**
   * \brief clear pending interrupt
   *
   * \param number number of the interrupt to clear as pending
   */
  void   clearPendingIRQ(uint8_t number);

  /**
   * \brief check if the interrupt is active
   *
   * \param number number of the interrupt to check
   *
   * \return true if it is active, false otherwise
   */
  bool   IRQActive(uint8_t number);

  /**
   * \brief check if the interrupt is enabled
   *
   * \param number number of the interrupt to check  
   *
   * \return true if it is enabled, false otherwise
   */
  bool   IRQEnabled(uint8_t number);

  /**
   * \brief check if the interrupt is pending
   *
   * \param number number of the interrupt to check
   *
   * \return true if it is pending, false otherwise
   */
  bool   IRQPending(uint8_t number);
 
  /**
   * \brief set priority level for interrupt
   *
   * \param number number of the interrupt to set the priority level
   * \param priority priority level to set (0 highest - 255 lowest)
   * \param bitsUsed number of bits used for setting priority (it depends on
   *        the SoC implementation
   */
  void   setPriority(uint8_t number, uint8_t priority, uint8_t bitsUsed);

  /**
   * \brief align stack on 8 byte instead of 4
   *
   * If enabled, on exception entry, the SP used prior to the exception
   * is adjusted to be 8-byte aligned
   *
   * \param enable true to enable 8-byte alignment, false for 4-byte alignment  
   */
  void   stackAlign8(bool enable);

  /**
   * \brief trigger software interrupt
   *
   * It allows to pend an interrupt
   *
   * \param number number of the interrupt to trigger
   */
  void   triggerSoftwareInterrupt(uint8_t number);
};

#endif
