/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gpio.h"


static const uint8_t gkOffsetGPIOControlLow        = 0x00;
static const uint8_t gkOffsetGPIOControlHigh       = 0x04;
static const uint8_t gkOffsetGPIODataInput         = 0x08;
static const uint8_t gkOffsetGPIODataOutput        = 0x0C;
static const uint8_t gkOffsetGPIOSetReset          = 0x10;
static const uint8_t gkOffsetGPIOResetOnly         = 0x14;
static const uint8_t gkOffsetGPIOConfigurationLock = 0x18;


/* Note: GPIO register have to be accesed by words (32-bit) */


GPIO::GPIO(eBaseAddress port)
  : baseAddress_(reinterpret_cast<uint8_t *>(port))
{
}


GPIO::~GPIO()
{
}


bool
GPIO::currentState(ePinNumber pin)
{
  uint32_t value = *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIODataInput));
  return static_cast<bool>(value & (0x01 << pin));
}


void
GPIO::high(ePinNumber ePin, bool atomic)
{
  uint8_t pin = static_cast<uint8_t>(ePin);
  if (atomic)
    *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIOSetReset)) |= (0x01 << pin);
  else
    *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIODataOutput)) |= (0x01 << pin);
}


void
GPIO::low(ePinNumber ePin, bool atomic)
{
  uint8_t pin = static_cast<uint8_t>(ePin);
  if (atomic)
    /* When using the atomic register, we need to use the high part of the register for clearing */
    *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIOSetReset)) |= (0x01 << (pin + 16));
  else
    *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIODataOutput)) &= ~(0x01 << pin);
}


void
GPIO::setMode(ePinNumber ePin, eMode mode)
{
  uint8_t pin = static_cast<uint8_t>(ePin);
  uint8_t mode_ = static_cast<uint8_t>(mode);

  uint8_t offset;
  if (pin < static_cast<uint8_t>(ePin8)) {
    offset = gkOffsetGPIOControlLow;
  } else {
    pin = pin - static_cast<uint8_t>(ePin8);
    offset = gkOffsetGPIOControlHigh; 
  }

  /* Always clear first */
  uint8_t shiftAmount = pin << 2;
  uint32_t mask = 0x0f << shiftAmount;
  *((volatile uint32_t *)(baseAddress_ + offset)) &= ~mask;
  /* Finally set mode */
  mask = mode_ << shiftAmount;
  *((volatile uint32_t *)(baseAddress_ + offset)) |= mask;
}


void
GPIO::toggle(ePinNumber pin)
{
  *((volatile uint32_t *)(baseAddress_ + gkOffsetGPIODataOutput)) ^= (0x01 << pin);
}
