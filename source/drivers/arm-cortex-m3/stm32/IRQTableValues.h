/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef __IRQ_TABLE_VALUE_H__
#define __IRQ_TABLE_VALUE_H__

#include <stdint.h>


namespace IRQManager
{
  enum eIRQNumber_t : uint8_t {
    eWatchdog = 0,
    ePVD,
    eTamper,
    eRTC,
    eFlash,
    eRCC,
    eExternalInterrupt0,
    eExternalInterrupt1,
    eExternalInterrupt2,
    eExternalInterrupt3,
    eExternalInterrupt4,
    eDMA1Channel1,
    eDMA1Channel2,
    eDMA1Channel3,
    eDMA1Channel4,
    eDMA1Channel5,
    eDMA1Channel6,
    eDMA1Channel7,
    eADC1and2,
    eUSBHighPriority_CANtx,
    eUSBLowPriority_CANrx0,
    eCANrx1,
    eCAN_SCE,
    // TODO: add the rest from RM0008 chapter 10
    eI2C1Event,
    eI2C1Error,
    eI2C2Event,
    eI2C2Error,
    eSPI1Global,
    eSPI2Gloabl,
    eUSART1Global,
    eUSART2Global,
    eUSART3Global,
    // TODO: add the rest from RM0008 chapter 10
    eTimer2Global,
    eTimer3Global,
    eTimer4Global,
    eTimer5Global
  };

} // namespace

#endif
