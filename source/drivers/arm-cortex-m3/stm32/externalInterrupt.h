/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __EXTERNAL_INTERRUPT_H__
#define __EXTERNAL_INTERRUPT_H__


/**
 * \brief External interrupt utility class.
 *
 * The following class allows the management of the external
 * interrupt lines.
 */
class ExternalInterrupt
{
public:

  enum eEventLine {
    eLine0   = 0x00,
    eLine1,
    eLine2,
    eLine3,
    eLine4,
    eLine5,
    eLine6,
    eLine7,
    eLine8,
    eLine9,
    eLine10,
    eLine11,
    eLine12,
    eLine13,
    eLine14,
    eLine15,
    eLine16,
    eLine17,
    eLine18,
    eLine19,
  };

  /**
   * \brief Clear pending interrupt on given line
   *
   * \param lineNumber line number of the pending event to clear
   */
  void    clearPendingInterrupt(eEventLine lineNumber);

  /**
   * \brief Mask event on given line
   *
   * \param lineNumber line number of the event to mask
   */
  void    eventMask(eEventLine lineNumber);

  /**
   * \brief Unmask event on given line
   *
   * \param lineNumber line number of the event to unmask
   */
  void    eventUnmask(eEventLine lineNumber);

  /**
   * \brief Generate software interrupt on the given line
   *
   * \param lineNumber line number of the event to unmask
   *
   * \note The event line must be unmasked both as event
   *       and interrupt for the interrupt to be generated
   */
  void    generateSoftwareInterrupt(eEventLine lineNumber);

  /**
   * \brief Mask interrupt on given line
   *
   * \param lineNumber line number of the interrupt to mask
   */
  void    interruptMask(eEventLine lineNumber);

  /**
   * \brief Unmask interrupt on given line
   *
   * \param lineNumber line number of the interrupt to unmask
   */
  void    interruptUnmask(eEventLine lineNumber);

  /*
   *  \brief Get current pending status of the given line
   *
   * \param lineNumber line number of the interrupt to unmask
   * \return true if interrupt is pending, false otherwise
   */
  bool    currentPendingStatus(eEventLine lineNumber);

  /**
   * \brief Disable trigger on falling edge for the given line
   *
   * \param lineNumber line number of the interrupt to disable the trigger
   */
  void    triggerFallingEdgeDisable(eEventLine lineNumber);

  /**
   * \brief Enable trigger on falling edge for the given line
   *
   * \param lineNumber line number of the interrupt to unmask
   */
  void    triggerFallingEdgeEnable(eEventLine lineNumber);

  /**
   * \brief Disable trigger on rising edge for the given line
   *
   * \param lineNumber line number of the interrupt to disable the trigger
   */
  void    triggerRisingEdgeDisable(eEventLine lineNumber);

  /**
   * \brief Enable trigger on rising edge for the given line
   *
   * \param lineNumber line number of the interrupt to enable the trigger
   */
  void    triggerRisingEdgeEnable(eEventLine lineNumber);

};

#endif
