/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __USART_H__
#define __USART_H__

#include <stdint.h>
#include <kernel/semaphore.h>

/**
 * \brief USART class.
 *
 * The following class provides an easy interface for the USART
 * controller.
 */
class USART
{
public:

  /**
   * \brief Base address for USART controllers
   */
  enum eBaseAddress {
    eUSART1 = 0x40013800,
    eUSART2 = 0x40004400,
    eUSART3 = 0x40004800,
    eUSART4 = 0x40004C00,
    eUSART5 = 0x40005000
  };

  /**
   * \brief Stop bits to send on transmssion (0.5, 1, 1.5, 2)
   */
  enum eStopBit_t {
    eStopBit05 = 0x01,
    eStopBit1  = 0x00,
    eStopBit15 = 0x03,
    eStopBit2  = 0x02
  };

  USART(eBaseAddress baseAddress);
  ~USART();

  /**
   * \brief Set the clock phase on the first edge
   */
  void clockPhaseFirstEdge();

  /**
   * \brief Set the clock phase on the second edge
   */
  void clockPhaseSecondEdge();

  /**
   * \brief Disable clock pin
   */
  void clockPinDisable();

  /*
   * \brief Enable clock pin for synchronous mode communication
   */
  void clockPinEnable();

  /**
   * \brief Set the clock polarity idle high
   */
  void clockPolarityIdleHigh();

  /**
   * \brief Set the clock polarity idle low
   */
  void clockPolarityIdleLow();

  /**
   * \brief Disable interrupt on error on multibuffer mode
   */
  void DMAInterruptOnErrorDisable();

  /**
   * \brief Enable interrupt on error on multibuffer mode
   */
  void DMAInterruptOnErrorEnable();

  /**
   * \brief Disable DMA multibuffer mode on reception
   */
  void DMAReceiverDisable();

  /**
   * \brief Enable DMA multibuffer mode on reception
   */
  void DMAReceiverEnable();

  /**
   * \brief Disable DMA multibuffer mode on transmission
   */
  void DMATransmitterDisable();

  /**
   * \brief Enabled DMA multibuffer mode on transmission
   */
  void DMATransmitterEnable();

  /**
   * \brief Disable controller
   */
  void disable();

  /**
   * \brief Enable controller but does not enable transmitter nor receiver
   *
   * Before configuring the controller you have to enable it, but make sure
   * you enabled the receiver and/or transmitter only after you have fully
   * configured the controller.
   */
  void enable();

  static void interruptHandler(void *thisPointer);

  void interrupt();

  /**
   * \brief Disable interrupt on receive buffer not empty
   */
  void interruptOnReceiveDisable();

  /**
   * \brief Enable interrupt on receive buffer not empty
   */
  void interruptOnReceiveEnable();

  /**
   * \brief Disable interrupt on transmission complete
   */
  void interruptOnTransmissionCompleteDisable();

  /**
   * \brief Enable interrupt on transmission complete
   */
  void interruptOnTransmissionCompleteEnable();

  /**
   * \brief Disable interrupt on transmission buffer empty
   */
  void interruptOnTransmissionDisable();

  /**
   * \brief Enable interrupt on transmission buffer empty
   */
  void interruptOnTransmissionEnable();

  /**
   * \brief Disable interrupt on parity error
   */
  void interruptOnParityErrorDisable();

  /**
   * \brief Enable interrupt on parity error
   */
  void interruptOnParityErrorEnable();

  /**
   * \brief Sebd byte data
   *
   * This method is blocking. If the transfer buffer is not
   * empty yet, if wait for the data in it to be sent before
   * overwrite it
   */
  void operator<<(const uint8_t data);

  /**
   * \brief Receive byte data
   *
   * This method is not blocking, although it does wait
   * a bit if not data is ready
   */
  void operator>>(uint8_t& data);

  /**
   * \brief Disable parity control generation/check
   */
  void parityControlDisable();

  /**
   * \brief Enable parity control generation/check
   *
   * When enabled the parity bit is generated and detected at the MSB of
   * the data byte (9th bit if wordLength9(), 8th bit if wordLength8())
   */
  void parityControlEnable();

  /**
   * \brief Set even parity when parity generation/detection is enabled
   */
  void parityEven();

   /**
   * \brief Set odd parity when parity generation/detection is enabled
   */
  void parityOdd();

  /**
   * \brief Disable the receiver
   */
  void receiverDisable();

  /**
   * \brief Enable the receiver
   *
   * Make sure you call this only after you have fully configured the controller
   */
  void receiverEnable();

  void send(const char* buffer, uint16_t length);

  /**
   * \brief Send one break character
   *
   * If called multiply times before the previous character is sent, it has not
   * effect
   */
  void sendBreakCharacter();

  /**
   * \brief Set the baude rate
   *
   * Set the baud rate as is without modification of the given value.
   * Please refer to the technical reference for how to the derive this
   * value
   */
  void setBaudRate(uint16_t baudRate);

  /**
   * \brief Set stop bits to be sent after each data byte
   */
  void setStopBits(eStopBit_t stopBit);

  /**
   * \brief Disable transmitter
   */
  void transmitterDisable();

  /**
   * \brief Enable transmitter
   *
   * Make sure you call this only after you have fully configured the controller
   */
  void transmitterEnable();

  /**
   * \brief Set word length to 8 bits
   */
  void wordLength8();

  /**
   * \brief Set word length to 9 bits
   */
  void wordLength9();

private:
  uint8_t* const _baseAddress;
  Semaphore      _semaphore;
};

#endif
