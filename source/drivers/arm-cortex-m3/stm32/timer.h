//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef __TIMERS_H__
#define __TIMERS_H__

#include <stdint.h>

///
/// \brief Basic timer utility class.
///
/// The following class provide some very basic timer operation
///
class Timer
{
public:

  ///
  /// \brief Base address for timers
  ///
  enum eBaseAddress {
    eTimer2 = 0x40000000, // General Purpose Timer
    eTimer3 = 0x40000400, // General Purpose Timer
    eTimer4 = 0x40000800, // General Purpose Timer
    eTimer5 = 0x40000C00, // General Purpose Timer
    eTimer6 = 0x40001000, // Basic Timer
    eTimer7 = 0x40001400  // Basic Timer
  };

  Timer(eBaseAddress timer);
  ~Timer();

  ///
  /// \brief enable Enable timer
  ///
  void    enable();

  ///
  /// \brief enable Enable interrupt on timer event generation
  ///
  void    enableInterrupt();

  ///
  ///
  /// \brief disable Disable timer
  void    disable();

  ///
  /// \brief enable Disable interrupt on timer event generation
  ///
  void    disableInterrupt();

  static void interruptHandler(void* thisPointer);
  void interrupt();

  ///
  /// \brief setPrescaler set prescaler value
  ///
  /// \param value prescaler value
  void    setPrescaler(uint16_t value);

  ///
  /// \brief enableAutoReload enable auto-reload mode
  ///
  /// \param enable if true enable auto reload mode
  void    enableAutoReload(bool enable);

  ///
  /// \brief enableOnePulseMode enable one pulse mode
  ///
  void    enableOnePulseMode(bool enable);

  ///
  /// \brief setAutoReload set auto-reload value
  ///
  void    setAutoReload(uint16_t value);

  ///
  /// \brief setCounter set counter value
  ///
  void    setCounter(uint16_t value);

  void    generateUpdateEvent();

  ///
  /// \brief currentValue get current counter value
  ///
  uint16_t  currentValue() const;

  ///
  /// \brief setCountingDirection set direction of counting
  ///
  enum eDirection_t {
    eCountdown,
    eCountup
  };
  void    setCountingDirection(eDirection_t direction);

  // TODO: the following needs to be moved to utility:: and they will
  //       use a timer object
  ///
  /// \brief Wait the given milliseconds before returning
  ///
  /// \param millisecond number of millisecond to wait
  void    delayms(uint16_t millisecond);

  ///
  /// \brief Wait the given microseconds before returning
  ///
  /// \param microsecond number of microsecond to wait
  void    delayus(uint16_t microsecond);

private:
  uint8_t* const   _baseAddress;
};

#endif
