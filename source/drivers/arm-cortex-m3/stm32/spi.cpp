//
// This file is part of the opencortex-m project.
//
// Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "spi.h"

#include <kernel/IRQManager.h>
#include <kernel/IRQNestingLevelTracker.h>
#include <kernel/semaphore.h>

#include "IRQTableValues.h"


static const uint8_t gkOffsetSPIControl1        = 0x00;
static const uint8_t gkOffsetSPIControl2        = 0x04;
static const uint8_t gkOffsetSPIStatus          = 0x08;
static const uint8_t gkOffsetSPIData            = 0x0C;
// CRC polynomial register
static const uint8_t gkOffsetSPICRCpolynomial   = 0x10;
// Rx CRC register
static const uint8_t gkOffsetSPICRCReceived     = 0x14;
// Tx CRC register
static const uint8_t gkOffsetSPICRCTransmitted  = 0x18;
// I2S configuration register
static const uint8_t gkOffsetI2SConfiguration   = 0x1C;
// I2S prescaler register
static const uint8_t gkOffsetI2SPrescaler       = 0x20;


void
spi1_global_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eSPI1Global);
}


void
spi2_global_handler()
{
  while(1);
}


void
spi3_global_handler()
{
  while(1);
}


/* The peripheral registers can be accessed by half-words or words */

SPI::SPI(eBaseAddress baseAddress)
  : _baseAddress(reinterpret_cast<uint8_t *>(baseAddress)),
    _semaphore(0)
{
  // Make sure controller is disabled
  disable();

  // Register interrupt handler
  if (baseAddress == eSPI1)
    IRQManager::registerHandler(IRQManager::eIRQNumber_t::eSPI1Global, (void (*)(void *))SPI::interruptHandler, (void *)this);

}


SPI::~SPI()
{
}


void
SPI::baudRateControl(eBaudRateControl baudRate)
{
  /* BR[2:0] */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x07 << 3);
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (baudRate << 3);
}

void
SPI::clockPhaseFirstEdge()
{
  /* CPHA */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~0x01;
}


void
SPI::clockPhaseSecondEdge()
{
  /* CPHA */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= 0x01;
}


void
SPI::clockPolarityIdleLow()
{
  /* CPOL */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x01 << 1);
}


void
SPI::clockPolarityIdleHigh()
{
  /* CPOL */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x01 << 1);
}


void
SPI::CRCDisable()
{
  /* CRCEN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 13);
}


void
SPI::CRCEnable()
{
  /* CRCREN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 13);
}


void
SPI::dataFormatLSB()
{
  /* LSBFIRST */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x01 << 7);
}


void
SPI::dataFormatMSB()
{
  /* LSBFIRST */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x01 << 7);
}


void
SPI::dataFormat8()
{
  /* DFF */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 11);
}


void
SPI::dataFormat16()
{
  /* DFF */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 11);
}


void
SPI::disable()
{
  /* Make sure the BSY bit is zero before disabling */
  uint8_t dontLoopForEver = 0xFF;
  while ( *((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & (0x01 << 7)
          && --dontLoopForEver)
    ;

  /* SPE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x01 << 6);
}


void
SPI::DMAReceiveDisable()
{
  /* RXDMAEN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~0x01;
}


void
SPI::DMAReceiveEnable()
{
  /* RXDMAEN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= 0x01;
}


void
SPI::DMATransmissionDisable()
{
  /* TXDMAEN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~(0x01 << 1);
}


void
SPI::DMATransmissionEnable()
{
  /* TXDMAEN */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= (0x01 << 1);
}


void
SPI::enable()
{
  /* SPE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x01 << 6);
}


void
SPI::fullDuplex()
{
  /* BIDIMODE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 15);
  /* RXONLY */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 10);
}


void
SPI::interruptHandler(void* thisPointer)
{
  ((SPI *)thisPointer)->interrupt();
}


void
SPI::interrupt()
{
   // TODO: implement this properly
  interruptOnReceiveDisable();
  interruptOnTransmissionDisable();
  _semaphore.release();
}


void
SPI::interruptOnErrorDisable()
{
  /* ERRIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~(0x01 << 5);
}


void
SPI::interruptOnErrorEnable()
{
  /* ERRIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= (0x01 << 5);
}


void
SPI::interruptOnReceiveDisable()
{
  /* RXNEIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~(0x01 << 6);
}


void
SPI::interruptOnReceiveEnable()
{
  /* RXNEIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= (0x01 << 6);
}


void
SPI::interruptOnTransmissionDisable()
{
  /* TXEIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~(0x01 << 7);
}


void
SPI::interruptOnTransmissionEnable()
{
  /* TXEIE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= (0x01 << 7);
}


void
SPI::manageNSSByHardware(bool enable)
{
  /* SSM */
  if (enable)
    *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 9);
  else
    *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 9);
}


void
SPI::masterMode()
{
  /*
   * Set bit SSOE to use the NSS pin as chip select.
   * Multi master capality is not allowed for the moment.
   */

  /* SSOE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) |= (0x01 << 2);
  /* MSTR */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x01 << 2);
}


void
SPI::operator<<(const uint8_t& data)
{
  /* TXE */
  while ( !(*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & (0x01 << 1)))
    ;

  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData)) = data;
}


void
SPI::operator<<(const uint16_t& data)
{
  /* TXE */
  while ( !(*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & (0x01 << 1)))
    ;

  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData)) = data;
}


void
SPI::operator>>(uint8_t& data)
{
  /* Read garbage from previous transfer to clear RXNE */
  data = *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData));

  /* RXNE should now be cleared */
  while (*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & 0x01)
    ;

  /* send arbitrary data to get clock going */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData)) = 0xFF;

  /* RXNE hopefully will be set again */
  while (!(*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & 0x01))
    ;

  data = *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData));
}


void
SPI::operator>>(uint16_t& data)
{
  /* Read garbage from previous transfer to clear RXNE */
  data = *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData));

  /* RXNE should now be cleared */
  while (*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & 0x01)
    ;

  /* send arbitrary data to get clock going */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData)) = 0xFFFF;

  /* RXNE hopefully will be set again */
  while (!(*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & 0x01))
    ;

  data = *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData));

}


bool
SPI::receive(uint8_t& data)
{
  bool validData = false;
  /* RXNE */
  // Wait for data
  interruptOnReceiveEnable();
  _semaphore.acquire();

  validData = true;
  data = *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData));

  return validData;
}


bool
SPI::send(const uint8_t& data)
{
  /* TXE */
  bool result = false;
  uint8_t dontLoopForEver = 0xFF;
  while ( !(*((volatile uint16_t *)(_baseAddress + gkOffsetSPIStatus)) & (0x01 << 1))
          && --dontLoopForEver)
    ;

  /* Don't override the old data if it timed out */
  if (dontLoopForEver) {
    result = true;
    *((volatile uint16_t *)(_baseAddress + gkOffsetSPIData)) = data;
  }

  return result;
}


void
SPI::simplexBidirectionalReceive()
{
  /* BIDIMODE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 15);
  /* BIDIOE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 14);
}


void
SPI::simplexBidirectionalTransmit()
{
  /* BIDIMODE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 15);
  /* BIDIOE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 14);
}


void
SPI::simplexUnidirectionalReceiveOnly()
{
  /* BIDIMODE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 15);
  /* RXONLY */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 10);
}


void
SPI::slaveMode()
{
  /*
   * Clear bit SSOE to use the NSS pin as chip slave select (we are slave).
   */

  /* SSOE */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl2)) &= ~(0x01 << 2);
  /* MSTR */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x01 << 2);
}


void
SPI::slaveSelectLow()
{
  /* SSI - Note: SSM must be set */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) &= ~(0x0001 << 8);
}


void
SPI::slaveSelectHigh()
{
  /* SSI - Note: SSM must be set */
  *((volatile uint16_t *)(_baseAddress + gkOffsetSPIControl1)) |= (0x0001 << 8);
}
