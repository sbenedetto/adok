/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "externalInterrupt.h"

#include <stdint.h>

#include <utility/register.h>

static uint8_t* const gkBaseAddress = reinterpret_cast<uint8_t *>(0x40010400);

static const uint8_t gkOffsetInterruptMask     = 0x00;
static const uint8_t gkOffsetEventMask         = 0x04;
static const uint8_t gkOffsetRisingTrigger     = 0x08;
static const uint8_t gkOffsetFallingTrigger    = 0x0C;
static const uint8_t gkOffsetSoftwareInterrupt = 0x10;
static const uint8_t gkOffsetPendingRegister   = 0x14;


void
ExternalInterrupt::clearPendingInterrupt(eEventLine lineNumber)
{
  Register::set(gkBaseAddress, gkOffsetPendingRegister, (0x01 << lineNumber));
}


void
ExternalInterrupt::eventMask(eEventLine lineNumber)
{
  Register::clearBits(gkBaseAddress, gkOffsetEventMask, (0x01 << lineNumber));
}


void
ExternalInterrupt::eventUnmask(eEventLine lineNumber)
{
  Register::setBits(gkBaseAddress, gkOffsetEventMask, (0x01 << lineNumber));
}


void
ExternalInterrupt::generateSoftwareInterrupt(eEventLine lineNumber)
{
  Register::setBits(gkBaseAddress, gkOffsetSoftwareInterrupt, (0x01 << lineNumber));
}


void
ExternalInterrupt::interruptMask(eEventLine lineNumber)
{
  Register::clearBits(gkBaseAddress, gkOffsetInterruptMask, (0x01 << lineNumber));
}


void
ExternalInterrupt::interruptUnmask(eEventLine lineNumber)
{
  Register::setBits(gkBaseAddress, gkOffsetInterruptMask, (0x01 << lineNumber));
}


bool
ExternalInterrupt::currentPendingStatus(eEventLine lineNumber)
{
  // TODO: replace with a register utility function when available
  return *((volatile uint8_t *)(gkBaseAddress + gkOffsetPendingRegister)) & (0x01 << lineNumber);
}


void
ExternalInterrupt::triggerFallingEdgeDisable(eEventLine lineNumber)
{
  Register::clearBits(gkBaseAddress, gkOffsetFallingTrigger, (0x01 << lineNumber));
}


void
ExternalInterrupt::triggerFallingEdgeEnable(eEventLine lineNumber)
{
  Register::setBits(gkBaseAddress, gkOffsetFallingTrigger, (0x01 << lineNumber));
}


void
ExternalInterrupt::triggerRisingEdgeDisable(eEventLine lineNumber)
{
  Register::clearBits(gkBaseAddress, gkOffsetRisingTrigger, (0x01 << lineNumber));
}


void
ExternalInterrupt::triggerRisingEdgeEnable(eEventLine lineNumber)
{
  Register::setBits(gkBaseAddress, gkOffsetRisingTrigger, (0x01 << lineNumber));
}
