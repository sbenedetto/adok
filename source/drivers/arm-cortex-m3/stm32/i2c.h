//
// This file is part of the adok project.
//
// Copyright (C) 2011 Marco Minutoli <mminutoli@gmail.com>
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef _I2C_H_
#define _I2C_H_

#include <stdint.h>

/**
 * \brief I2C controller class.
 *
 * The following class implements the interface for using the I2C
 * controller of the stm32f1x
 */
class I2C
{
public:
  /**
   * \brief Base address for I2C controllers on stm32f1x.
   */
  enum eBaseAddress {
    eI2C1 = 0x40005400,
    eI2C2 = 0x40005800
  };

  /**
   * \brief Operating modes.
   */
  enum eOperatingMode {
    eStandard10Khz = 0,
    eStandard50Khz,
    eStandard100Khz,
    eFast400Khz
  };

  /**
   * \brief Constructor.
   */
  I2C(eBaseAddress address);

  /**
   * \brief Destructor.
   */
  ~I2C();

  /**
   * \brief Disable the i2c controller.
   *
   * \pre  I2C controller is either enabled or disabled.
   * \post I2C controller is disabled.
   */
  void disable();

  /**
   * \brief Disable acknowledge
   */
   void disableAcknowledge();

  /**
   * \brief Disable interrupt on buffer
   *
   * Interrupts are not generated when the transfer buffer
   * is empty or the receiver buffer is not empty
   */
  void disableInterruptBuffer();

  /**
   * \brief Disable interrupt on error
   */
  void disableInterruptOnError();

  /**
   * \brief Disable interrupt on event
   */
  void disableInterruptOnEvent();

  /**
   * \brief Enable the I2C controller.
   *
   * \pre  I2C controller is either enabled or disabled.
   * \post I2C controller is enabled.
   */
  void enable();

  /**
   * \brief Enable acknowledge
   */
  void enableAcknowledge();

  /**
   * \brief Enable interrupt on buffer
   *
   * Interrupts are generated when the transfer buffer
   * is empty or the receiver buffer is not empty
   */
  void enableInterruptBuffer();

  /**
   * \brief Enable interrupt on error
   */
  void enableInterruptOnError();

  /**
   * \brief Enable interrupt on event
   */
  void enableInterruptOnEvent();

  static void interruptHandler(void* thisPointer);

  void interrupt();

  /**
   * \brief Read bytes from slave device
   *
   * \param slaveAddressRead 8 bit slave address for reading mode
   * \param slaveAddressWrite 8 bit slave address for writing mode
   * \param registerAddress 8 bit subaddress. Usually register within the slave peripheral.
   * \param dataOut pointer to the buffer where data have to be saved
   * \param numberOfBytes number of bytes to read
   */
  bool readFrom(const uint8_t slaveAddressRead, const uint8_t slaveAddressWrite,
                const uint8_t registerAddress, int8_t* dataOut,
                const uint8_t numberOfBytes);

  /**
   * \brief Set the operating mode.
   *
   * The following set the controller either at 100Khz for
   * standard mode, or 400Khz for fast mode. While it might be limitating,
   * it hides the user from the details of the various registers.
   *
   * \param mode operating mode to set. Either standard or fast.
   */
  void setMode(eOperatingMode mode);

 /**
   * \brief Set the 10-bit address of the controller.
   *
   * \pre  I2C controller is disabled.
   * \param address The address to be set.
   * \post I2C controller is in 7bit addressing mode.
   */
  void set10bitAddress(uint16_t address);

  /**
   * \brief Set the 7-bit address of the controller.
   *
   * \pre  I2C controller is disabled.
   * \param address The address to be set.
   * \post I2C controller is in 10-bit addressing mode.
   */
  void set7bitAddress(uint8_t address);

  /**
   * \brief Write bytes from slave device
   *
   * \param slaveAddress 8 bit slave address
   * \param registerAddress 8 bit subaddress. Usually register within the slave peripheral.
   * \param dataOut pointer to the buffer where data to write are saved
   * \param numberOfBytes number of bytes to write
   */
  bool writeTo(const uint8_t slaveAddress, const uint8_t registerAddress,
                const int8_t* data, const uint8_t numberOfBytes);

private:

  /** Base interface address */
  volatile uint8_t* const _baseAddress;
};

#endif /* _I2C_H_ */
