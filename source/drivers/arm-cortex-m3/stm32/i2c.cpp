//
// This file is part of the adok project.
//
// Copyright (C) 2011 Marco Minutoli <mminutoli@gmail.com>
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "i2c.h"

#include <kernel/IRQManager.h>
#include <kernel/IRQNestingLevelTracker.h>
#include <kernel/semaphore.h>

#include <utility/register.h>

#include "IRQTableValues.h"
#include "../nvic.h"



static const uint8_t gkOffsetI2CControl1     = 0x00;
static const uint8_t gkOffsetI2CControl2     = 0x04;
// Own address registers
static const uint8_t gkOffsetI2COwnAddress1  = 0x08;
static const uint8_t gkOffsetI2COwnAddress2  = 0x0C;
static const uint8_t gkOffsetI2CData         = 0x10;
static const uint8_t gkOffsetI2CStatus1      = 0x14;
static const uint8_t gkOffsetI2CStatus2      = 0x18;
static const uint8_t gkOffsetI2CClockControl = 0x1C;
static const uint8_t gkOffsetI2CTimeRise        = 0x20;


void
i2c1_event_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eI2C1Event);
}


void
i2c1_error_handler()
{ 
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eI2C1Error);
}


void
i2c2_event_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eI2C2Event);
}


void
i2c2_error_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eI2C2Error);
}


/* Peripheral register can be accessed by half-word (16-bits) or word (32-bits) */

I2C::I2C(eBaseAddress address)
  : _baseAddress(reinterpret_cast<volatile uint8_t* const>(address))
{
  // disable the i2c controller to get in a known state.
  disable(); 

  // Registre interrupt handler
  if (address == eI2C1) {
    IRQManager::registerHandler(IRQManager::eIRQNumber_t::eI2C1Event,
      (void (*)(void *))I2C::interruptHandler, (void *)this);
    IRQManager::registerHandler(IRQManager::eIRQNumber_t::eI2C1Error,
      (void (*)(void *))I2C::interruptHandler, (void *)this);
  }

}


I2C::~I2C()
{
  disable();
}


void
I2C::disable()
{
  Register::clearBits(_baseAddress, gkOffsetI2CControl1, 0x01);
}


void
I2C::disableAcknowledge()
{
  Register::clearBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
}


void
I2C::disableInterruptBuffer()
{
  Register::clearBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 10));
}


void
I2C::disableInterruptOnError()
{
  Register::clearBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 8));
}


void
I2C::disableInterruptOnEvent()
{
  Register::clearBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 9));
}


void
I2C::enable()
{
  Register::setBits(_baseAddress, gkOffsetI2CControl1, 0x01);
}


void
I2C::enableAcknowledge()
{
  Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
}


void
I2C::enableInterruptBuffer()
{
  Register::setBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 10));
}


void
I2C::enableInterruptOnError()
{
  Register::setBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 8));
}


void
I2C::enableInterruptOnEvent()
{
  Register::setBits(_baseAddress, gkOffsetI2CControl2, (0x01 << 9));
}


void
I2C::interruptHandler(void* thisPointer)
{
  ((I2C *)thisPointer)->interrupt();
}


void
I2C::interrupt()
{
   // TODO
}


bool
I2C::readFrom(const uint8_t slaveAddressRead, const uint8_t slaveAddressWrite,
              const uint8_t registerAddress, int8_t* dataOut,
              const uint8_t numberOfBytes)
{
  uint16_t bytesRead = 0;

  if (numberOfBytes > 2) {

    // TODO: This is totally wrong. Needs to be fixed

    // Read data from data register until only 3 bytes are left to read
    while ((numberOfBytes - bytesRead) > 3)
    {
      // Wait for RXNE to be set
      while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
        ;
      dataOut[numberOfBytes - bytesRead]
        = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
      ++bytesRead;
    }
    // Three bytes left to read
    // Wait for RXNE to be set (N-2 in data register)
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
      ;
    // Wait for BTF to be set (N-1 in shift register)
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 2)) == false)
      ;
    // Clear the ACK bit before reading the N-2 byte
    Register::clearBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
    //TODO: disable interrupt
    // Read data register (N-2)
    dataOut[numberOfBytes - bytesRead]
        = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
    ++bytesRead;
    // Set STOP bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));
    // Wait for RXNE to be set (byte N-1 in data register)
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
      ;
    // Read data register (N-1)
    dataOut[numberOfBytes - bytesRead]
        = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
    ++bytesRead;
    //TODO: enable interrupt
    // Wait for RXNE to be set (byte N in data register)
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
      ;
    // Read data register (N)
    dataOut[numberOfBytes - bytesRead]
        = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
    ++bytesRead;
  } else if (numberOfBytes == 2) {

    // TODO: This is totally wrong. Needs to be fixed

    // Set POS bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 11));
    // Set ACK bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
    // Set START bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 8));
    // Wait for START bit (SB) to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01) == false)
      ;
    // Read status register 1 to clear START bit (SB)
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01);
    // Write slave address in data register
    Register::set8(_baseAddress, gkOffsetI2CData, slaveAddressWrite);
    // Wait for the ADDR flag to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == false)
      ;
    // TODO: disable interrupt
    // Clear ADDR flag by reading status register 1 and status register 2
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0xff);
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);
    // Make sure ADDR was cleared
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == true)
      ;
     // Clear ACK bit
    Register::clearBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
    // Wait for BTF
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 2)) == false)
      ;
    // Set STOP bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));

    dataOut[bytesRead]
      = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
    // Wait for the RXNE bit to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
      ;
    dataOut[bytesRead + 1]
      = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));

  } else {
    // Only one byte to read
    // Set start bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 8));
    // Wait for start bit (SB) to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01) == false)
      ;
    // Read status register 1 to clear start bit (SB)
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01);

    // Write the slave address in the data register (write mode)
    Register::set8(_baseAddress, gkOffsetI2CData, slaveAddressWrite);
    // Wait for the ADDR flag to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == false)
      ;
    // Clear ADDR flag by reading Status register 1 and status register 2
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0xff);
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);

    // Send the register address to read
    Register::set8(_baseAddress, gkOffsetI2CData, registerAddress);
    // Wait for BTF to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 2)) == false)
      ;

    // Send a restart
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 8));
    // Wait for start bit (SB) to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01) == false)
      ;
    // Read status register 1 to clear start bit (SB)
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01);

    // Write the slave address in the data register (read mode)
    Register::set8(_baseAddress, gkOffsetI2CData, slaveAddressRead);
    // Wait for the ADDR flag to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == false)
      ;

    // Clear ACK bit
    Register::clearBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
    // TODO: disable interrupt
    // Clear ADDR flag by reading status register 1 and status register 2
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0xff);
    Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);
    // Make sure ADDR was cleared
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == true)
      ;
    // Set STOP bit
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));
    // TODO: enable interrupt
    // Wait for the RXNE bit to be set
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 6)) == false)
      ;
    // Read data
    dataOut[bytesRead]
      = static_cast<uint8_t>(Register::get(_baseAddress, gkOffsetI2CData));
    // Wait for STOP to be cleared
    while (Register::isBitSet(_baseAddress, gkOffsetI2CControl1, (0x01 << 9)) == true)
      ;
    // Re-enable acknowledge
    Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 10));
  }

  return true;
}


void
I2C::setMode(eOperatingMode mode)
{
  /* Set mode */
  uint16_t mask = (static_cast<uint16_t>(mode) << 15);
  Register::clearBits(_baseAddress, gkOffsetI2CClockControl, (0x01 << 15));
  Register::setBits(_baseAddress, gkOffsetI2CClockControl, mask);

  /* Clear the lower 6 bits [5:0] of the register. */
  Register::clearBits(_baseAddress, gkOffsetI2CControl2, 0x3f);
  /* Clear control control */
  Register::clearBits(_baseAddress, gkOffsetI2CClockControl, 0x0FFF);
  /* Clear time rise register */
  Register::setBits(_baseAddress, gkOffsetI2CTimeRise, 0x3F);

  switch (mode) {

    /* In standard mode, T_low and T_high are given by the inverse value
     * in peripheral clock register multiplied by the value in clock control.
     * Thus 1/36Mhz x 180 (0xb4) = 50us for T_high (and T_low) */
    case eStandard10Khz:
      /* Set peripheral clock at 36Mhz */
      Register::setBits(_baseAddress, gkOffsetI2CControl2, 0x24);
      /* Set bus clock at 50Khz */
      Register::setBits(_baseAddress, gkOffsetI2CClockControl, 0x707);
      /* Set rise time plus 1 */
      Register::setBits(_baseAddress, gkOffsetI2CTimeRise, 0x25);
      break;

    /* In standard mode, T_low and T_high are given by the inverse value
     * in peripheral clock register multiplied by the value in clock control.
     * Thus 1/36Mhz x 180 (0xb4) = 10us for T_high (and T_low) */
    case eStandard50Khz:
      /* Set peripheral clock at 36Mhz */
      Register::setBits(_baseAddress, gkOffsetI2CControl2, 0x24);
      /* Set bus clock at 50Khz */
      Register::setBits(_baseAddress, gkOffsetI2CClockControl, 0x168);
      /* Set rise time plus 1 */
      Register::setBits(_baseAddress, gkOffsetI2CTimeRise, 0x25);
      break;

    /* In standard mode, T_low and T_high are given by the inverse value
     * in peripheral clock register multiplied by the value in clock control.
     * Thus 1/36Mhz x 180 (0xb4) = 5us for T_high (and T_low) */
    case eStandard100Khz:
      /* Set peripheral clock at 36Mhz */
      Register::setBits(_baseAddress, gkOffsetI2CControl2, 0x24);
      /* Set bus clock at 100Khz */
      Register::setBits(_baseAddress, gkOffsetI2CClockControl, 0xb4);
      /* Set rise time plus 1 */
      Register::setBits(_baseAddress, gkOffsetI2CTimeRise, 0x25);
      break;

    case eFast400Khz:
      /* TODO */
      /* Set peripheral clock */
      /* Set clock at 400Khz */
      /* Set rise time */
      break;

    default:
      /* TODO: do something. Like logging. */
      break;
  };
}


void
I2C::set10bitAddress(uint16_t address)
{
  /* Set 10bit address. Make sure we don't modify the reserved bits */
  Register::set(_baseAddress, gkOffsetI2COwnAddress1, (address & 0x03FF));
  /*
   * Set bit 14 and 15 to 1.
   * Bit 15 is ADDMODE. If 1 set 10bit addressing.
   * Bit 14 as stated in the reference manual should be kept to 1 by sw.
   */
  Register::setBits(_baseAddress, gkOffsetI2COwnAddress1, (0x03 << 14));
}


void
I2C::set7bitAddress(uint8_t address)
{
  /* Set the address */
  Register::set(_baseAddress, gkOffsetI2COwnAddress1, (address << 1));
  /* set 7 bit addressing mode */
  Register::clearBits(_baseAddress, gkOffsetI2COwnAddress1, (0x01 << 15));
  /* As stated in the reference manual bit 14 should be kept to 1 by sw */
  Register::setBits(_baseAddress, gkOffsetI2COwnAddress1, (0x01 << 14));
}


bool
I2C::writeTo(const uint8_t slaveAddressWrite, const uint8_t registerAddress,
             const int8_t* dataOut, const uint8_t numberOfBytes)
{
  uint8_t dataWritten = 0;

  // Generate Start condition
  Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 8));
  // Wait for the SB flag to be set
  while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01) == false)
    ;

  // Read status register 1 to clear start bit (SB)
  Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0x01);

  // uint64_t timeout = util::currentCounterValue();
  // Write the slave address in the data register
  Register::set8(_baseAddress, gkOffsetI2CData, slaveAddressWrite);

  // Wait for the ADDR flag to be set
  while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 1)) == false) {
    // Acknowledge failed
    if (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 10)) == true) {
      Register::clearBits(_baseAddress, gkOffsetI2CStatus1, (0x01 << 10));
      Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);
      // Generate STOP
      Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));
      return false;
    }

    // Timeout if bus was low for 25ms
    /*
    uint64_t timeElapsed = util::currentCounterValue() - timeout;
    if (timeElapsed > 25 * 1000) {
      Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);
      // Generate STOP
      Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));
      return false;
    }
    */
  }

  // Clear ADDR flag by reading Status register 1 and status register 2
  Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, 0xff);
  Register::isBitSet(_baseAddress, gkOffsetI2CStatus2, 0xff);
  // First send the register address
  Register::set8(_baseAddress, gkOffsetI2CData, registerAddress);

  while ((numberOfBytes - dataWritten) > 0) {

    // Wait for TXE
    while (Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 7)) == false)
      ;

    // Write data in the data register
    Register::set8(_baseAddress, gkOffsetI2CData, dataOut[dataWritten]);

    ++dataWritten;
  }

  // Wait for BTF to be set
  while (numberOfBytes && Register::isBitSet(_baseAddress, gkOffsetI2CStatus1, (0x01 << 2)) == false)
    ;

  // Set Stop condition
  Register::setBits(_baseAddress, gkOffsetI2CControl1, (0x01 << 9));
  // Wait for STOP bit to be cleared by hardware
  while (Register::isBitSet(_baseAddress, gkOffsetI2CControl1, (0x01 << 9)) == true)
    ;

  return true;
}
