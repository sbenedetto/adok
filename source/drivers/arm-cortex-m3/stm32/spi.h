//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef __SPI_H__
#define __SPI_H__

#include <stdint.h>

#include <kernel/semaphore.h>

///
/// \brief SPI controller class.
///
/// The following class implements an interface for using the SPI
/// controller of the stm32f1x
///
class SPI
{
public:

  /**
   * \brief base address for SPI controllers on stm32f1x
   */
  enum eBaseAddress {
    eSPI1 = 0x40013000,
    eSPI2 = 0x40003C00,
    eSPI3 = 0x40003800};

  enum eBaudRateControl {
    eFpclk2   = 0x00,
    eFpclk4   = 0x01,
    eFpclk8   = 0x02,
    eFpclk16  = 0x03,
    eFpclk32  = 0x04,
    eFpclk64  = 0x05,
    eFpclk128 = 0x06,
    eFpclk256 = 0x07};

  SPI(eBaseAddress address);
  ~SPI();

  /**
   * \brief Set baud rate control
   *
   * Don't change the following when a communication is ongoing
   */
   void   baudRateControl(eBaudRateControl baudRate);

  /**
   * \brief Set the clock phase on the first edge
   *
   * The first edge is the capture strobe:
   *  - falling edge if clock polarity is idle high
   *  - rising edge if clock polarity is idle low
   * Data are latched on the occurrence of the first clock transition.
   */
  void    clockPhaseFirstEdge();

  /**
   * \brief Set the clock phase on the second edge
   *
   * The second edge is the capture strobe:
   *  - rising edge if clock polarity is idle high
   *  - falling edge if clock polarity is idle low
   * Data are latched on the occurrence of the second clock transition.
   */
  void    clockPhaseSecondEdge();

  /**
   * \brief Set the clock polarity idle low
   *
   * This bit affects both master and slave modes. The clock is low-level
   * idle state
   */
  void    clockPolarityIdleLow();

  /**
   * \brief Set the clock polarity idle high
   *
   * This bit affects both master and slave modes. The clock is high-level
   * idle state
   */
  void    clockPolarityIdleHigh();

  /**
   * \brief Disable CRC
   */
  void    CRCDisable();

  /**
   * \brief Enable CRC
   *
   * Make sure controller is disabled before enabling this
   */
  void    CRCEnable();

 /**
   * \brief Set the least significant bit to be sent out first
   */
  void    dataFormatLSB();

  /**
   * \brief Set the most significant bit to be sent out first
   */
  void    dataFormatMSB();

  /**
   * \brief Set the data frame 8 bit long
   *
   * Make sure the controller is disabled before setting data format
   */
  void    dataFormat8();

  /**
   * \brief Set the data frame 16 bit long
   *
   * Make sure the controller is disabled before setting data format
   */
  void    dataFormat16();

  /**
   * \brief Disable controller
   */
  void    disable();

  /**
   * \brief Disable DMA in receive
   */
  void    DMAReceiveDisable();

  /**
   * \brief Enable DMA in receive
   */
  void    DMAReceiveEnable();

  /**
   * \brief Disable DMA in transmission
   */
  void    DMATransmissionDisable();

  /**
   * \brief Enable DMA in transmission
   */
  void    DMATransmissionEnable();

 /**
   * \brief Enable controller
   */
  void    enable();

  /**
   * \brief Set SPI for full duplex communication
   */
  void    fullDuplex();

static void    interruptHandler(void* thisPointer);

  void    interrupt();

  /**
   * \brief Disable interrupt on error
   */
  void    interruptOnErrorDisable();

  /**
   * \brief Enable interrupt on error
   */
  void    interruptOnErrorEnable();

  /**
   * \brief Disable interrupt when the receive buffer is full
   */
  void    interruptOnReceiveDisable();

  /**
   * \brief Enable interrupt when the receive buffer is full
   */
  void    interruptOnReceiveEnable();

  /**
   * \brief Disable interrupt when data transfer is complete
   */
  void    interruptOnTransmissionDisable();

  /**
   * \brief Enable interrupt when data transfer is complete
   */
  void    interruptOnTransmissionEnable();

  /**
   * \brief Enable hardware management for the NSS pin
   *
   * This is advised when there is only one slave on the bus.
   * The controller will automatically select the slave by pulling
   * down the signal when the communication starts and it disables
   * it when the controller is disabled.
   *
   * \param enable true if you want the hardware to manage the pin. False
   *              otherwise
   */
  void   manageNSSByHardware(bool enable);

  /**
   * \brief Set device in master mode
   *
   * \note: currently only the configuration one master one slave
   *        is allowed. NSS output enabled by default
   */
  void    masterMode();

  /**
   * \brief Send data - 8bit
   *
   * \param data data to send
   */
  void    operator<<(const uint8_t& data);

  /**
   * \brief Send data - 16bit
   *
   * \param data data to send
   */
  void    operator<<(const uint16_t& data);

  /**
    * \brief Receive data - 8 bit
    */
  void   operator>>(uint8_t& data);

  /**
    * \brief Receive data - 16 bit
    */
  void   operator>>(uint16_t& data);

  /**
   * \brief Receive data - 8 bit
   *
   * \param data data to to receive
   * \return true if received, false if timed out
   */
  bool    receive(uint8_t& data);

  /**
   * \brief Send data - 8 bit
   *
   * \param data data to send
   * \return true if sent, false if timed out due to the previous
   *         transfer not being complete
   */
  bool    send(const uint8_t& data);

  /**
   * \brief Set SPI for bidirectional simplex communication - receive
   *
   * This mode allows you to receive on pin MOSI when you are a master
   * (MISO if you are a slave) and use the other pin as a GPIO.
   */
  void    simplexBidirectionalReceive();

  /**
   * \brief Set SPI for bidirectional simplex communication - transmit
   *
   * This mode allows you to transmit on pin MOSI when you are a master
   * (MISO if you are a slave) and use the other pin as a GPIO.
   */
  void    simplexBidirectionalTransmit();

 /**
   * \brief Set SPI for simplex commincation - receive only
   */
  void    simplexUnidirectionalReceiveOnly();

  /**
   * \brief Set device in slave mode
   */
  void    slaveMode();

  /**
   * \brief Set NSS pin low
   *
   * In order to use the following, the NSS pin management
   * must be in software mode (e.g. call manageNSSByHardware(false);
   */
  void    slaveSelectLow();

  /**
   * \brief Set NSS pin high
   *
   * In order to use the following, the NSS pin management
   * must be in software mode (e.g. call manageNSSByHardware(false);
   */
  void    slaveSelectHigh();

private:
  uint8_t* const    _baseAddress;
  Semaphore         _semaphore;
};

#endif
