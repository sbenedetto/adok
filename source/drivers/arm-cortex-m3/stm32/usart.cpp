/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "usart.h"

#include <kernel/IRQManager.h>
#include <kernel/IRQNestingLevelTracker.h>
#include <kernel/semaphore.h>

#include "IRQTableValues.h"
#include "../nvic.h"


static const uint8_t gkOffsetStatus          = 0x00;
static const uint8_t gkOffsetData            = 0x04;
static const uint8_t gkOffsetBaudRate        = 0x08;
static const uint8_t gkOffsetControl1        = 0x0C;
static const uint8_t gkOffsetControl2        = 0x10;
static const uint8_t gkOffsetControl3        = 0x14;
static const uint8_t gkGuardTimeAndPrescaler = 0x18;


void
usart1_global_handler()
{
  IRQNestingLevelTracker _;

  // Call handler
  IRQManager::callHandler(IRQManager::eUSART1Global);
}


void
usart2_global_handler()
{
  while(1);
}


void
usart3_global_handler()
{
  while(1);
}


void
usart4_global_handler()
{
  while(1);
}


void
usart5_global_handler()
{
  while(1);
}


/* The peripheral registers can be accessed by half-words or words */

USART::USART(eBaseAddress baseAddress)
  :  _baseAddress(reinterpret_cast<uint8_t *>(baseAddress)),
     _semaphore(0)
{
  // Registre interrupt handler
  if (baseAddress == eUSART1)
    IRQManager::registerHandler(IRQManager::eIRQNumber_t::eUSART1Global, (void (*)(void *))USART::interruptHandler, (void *)this);
}


USART::~USART()
{
}


void
USART::clockPhaseFirstEdge()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) &= ~(0x01 << 9);
}


void
USART::clockPhaseSecondEdge()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) &= ~(0x01 << 9);
}


void
USART::clockPinDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) &= ~(0x01 << 11);
}


void
USART::clockPinEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) |= (0x01 << 11);
}


void
USART::clockPolarityIdleHigh()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) |= (0x01 << 10);
}


void
USART::clockPolarityIdleLow()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) &= ~(0x01 << 10);
}


void
USART::DMAInterruptOnErrorDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) &= ~(0x01);
}


void
USART::DMAInterruptOnErrorEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) |= (0x01);
}


void
USART::DMAReceiverDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) &= ~(0x01 << 6);
}


void
USART::DMAReceiverEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) |= (0x01 << 6);
}


void
USART::DMATransmitterDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) &= ~(0x01 << 5);
}


void
USART::DMATransmitterEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl3)) |= (0x01 << 5);
}


void
USART::disable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 13);
}


void
USART::enable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 13);
}


void
USART::interruptHandler(void *thisPointer)
{
  ((USART *)thisPointer)->interrupt();
}


void
USART::interrupt()
{
   // TODO: implement this properly
  if ( *(volatile uint16_t *)(_baseAddress + gkOffsetStatus) & (0x01 << 6)) {
    interruptOnTransmissionDisable();
    _semaphore.release();
  }
}


void
USART::interruptOnReceiveDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 5);
}


void
USART::interruptOnReceiveEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 5);
}


void
USART::interruptOnTransmissionCompleteDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 6);
}


void
USART::interruptOnTransmissionCompleteEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 6);
}


void
USART::interruptOnTransmissionDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 7);
}


void
USART::interruptOnTransmissionEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 7);
}


void
USART::interruptOnParityErrorDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 8);
}


void
USART::interruptOnParityErrorEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 8);
}


void
USART::operator<<(const uint8_t data)
{
  /* Wait for transfer register to be empty */
  while (!((*(volatile uint16_t *)(_baseAddress + gkOffsetStatus)) & (0x01 << 7)))
    ;

  *((volatile uint16_t *)(_baseAddress + gkOffsetData)) = data;
}


void
USART::operator>>(uint8_t& data)
{
  /* Wait for receive register to be full */
  while (!(*((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) & (0x01 << 5)))
    ;

  data = *((volatile uint16_t *)(_baseAddress + gkOffsetData));
}


void
USART::parityControlDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 10);
}


void
USART::parityControlEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 10);
}


void
USART::parityEven()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 9);
}


void
USART::parityOdd()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 9);
}


void
USART::receiverDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 2);
}


void
USART::receiverEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 2);
}


void
USART::send(const char* buffer, uint16_t length)
{
  for (uint16_t index = 0; index < length; ++index) {
    // Send one byte and block
    *((volatile uint16_t *)(_baseAddress + gkOffsetData)) = buffer[index];
    interruptOnTransmissionEnable();
    _semaphore.acquire();
  }

}

void
USART::sendBreakCharacter()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01);
}


void
USART::setBaudRate(uint16_t baudRate)
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetBaudRate)) = baudRate;
}


void
USART::setStopBits(eStopBit_t stopBit)
{
  /* Always clear first */
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) &= ~(0x03 << 12);
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl2)) |= (stopBit << 12);
}


void
USART::transmitterDisable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 3);
}


void
USART::transmitterEnable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 3);
}


void
USART::wordLength8()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 12);
}


void
USART::wordLength9()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 12);
}
