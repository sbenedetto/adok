//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "timer.h"

#include <kernel/IRQManager.h>
#include <kernel/IRQNestingLevelTracker.h>

#include "IRQTableValues.h"


static const uint8_t gkOffsetControl1        = 0x00;
static const uint8_t gkOffsetControl2        = 0x04;
static const uint8_t gkOffsetInterruptEnable = 0x0C;
static const uint8_t gkOffsetStatus          = 0x10;
static const uint8_t gkOffsetEventGeneration = 0x14;
static const uint8_t gkOffsetCounter         = 0x24;
static const uint8_t gkOffsetPrescaler       = 0x28;
static const uint8_t gkOffsetAutoReload      = 0x2C;


// Note: Timer registers may be accessed by half-word (16-bit) or word (32-bit)

void
timer3_global_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eTimer3Global);
}


void
timer4_global_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eTimer4Global);
}


void
timer5_global_handler()
{
  IRQNestingLevelTracker _;

  IRQManager::callHandler(IRQManager::eTimer5Global);
}


Timer::Timer(eBaseAddress baseAddress)
  : _baseAddress(reinterpret_cast<uint8_t *>(baseAddress))
{
  // Assuming we are running at full speed, divide prescaler by 72.
  // This way the counter will be counting at each microsecond
  // TODO: fix me
  setPrescaler(72);

  // Set auto-reload off
  enableAutoReload(false);

  // Set countdown mode
  setCountingDirection(eCountdown);

  // Set one pulse mode
  enableOnePulseMode(true);

  // Set underflow as the only source of interrupt generator
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 2);

  // Register interrupt handler
  using II = IRQManager::eIRQNumber_t;
  if (baseAddress == eTimer2)
    IRQManager::registerHandler(II::eTimer2Global, (void (*)(void *))Timer::interruptHandler, (void *)this);
  else if (baseAddress == eTimer3)
    IRQManager::registerHandler(II::eTimer3Global, (void (*)(void *))Timer::interruptHandler, (void *)this);
  else if (baseAddress == eTimer4)
    IRQManager::registerHandler(II::eTimer4Global, (void (*)(void *))Timer::interruptHandler, (void *)this);
  else if (baseAddress == eTimer5)
    IRQManager::registerHandler(II::eTimer5Global, (void (*)(void *))Timer::interruptHandler, (void *)this);
}


Timer::~Timer()
{
}


void
Timer::enable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= 0x01;
}


void
Timer::enableAutoReload(bool enable)
{
  if (enable) {
    // Set auto-reload on
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 7);
  } else {
    // Set auto-reload off
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 7);
  }
}


void
Timer::enableOnePulseMode(bool enable)
{
  if (enable) {
    // Set one pulse mode
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 3);
  } else {
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 3);
  }
}


void
Timer::enableInterrupt()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetInterruptEnable)) |= (0x01 << 6);
}


void
Timer::disable()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~0x01;
}


void
Timer::disableInterrupt()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetInterruptEnable)) &= ~(0x01 << 6);
}


void
Timer::interruptHandler(void* thisPointer)
{
  ((Timer *)thisPointer)->interrupt();
}


void
Timer::interrupt()
{
  // Clear interrupt flag
  *((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) &= ~(0x01 << 6);
}


void
Timer::setAutoReload(uint16_t value)
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetAutoReload)) = value;
}


void
Timer::setCounter(uint16_t value)
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetCounter)) = value - 1;
}


void
Timer::setPrescaler(uint16_t value)
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetPrescaler)) = value - 1;
}


void
Timer::generateUpdateEvent()
{
  *((volatile uint16_t *)(_baseAddress + gkOffsetEventGeneration)) |= 0x01 ;
}


uint16_t
Timer::currentValue() const
{
  uint16_t tmp = *((volatile uint16_t *)(_baseAddress + gkOffsetCounter));
  return tmp;
}


void
Timer::setCountingDirection(eDirection_t direction)
{
  if (direction == eCountup) {
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) &= ~(0x01 << 4);
  } else {
    // Countdown
    *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 4);
  }
}


void
Timer::delayms(uint16_t millisecond)
{
  // Set pre-scaler to count every 500us
  setPrescaler(36000);

  // Load counter
  // TODO: we should put a limit MAX_16_UINT / 2
  setCounter(millisecond * 2);

  // Set one-pulse mode
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 3);

  // Enable timer
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= 0x01;

  // Wait for the update flag (UIF)
  while ( !(*((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) & 0x01))
    ;

  // Clear status bit
  *((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) = 0x0;
}


void
Timer::delayus(uint16_t microsecond)
{
  // Set pre-scaler to count every microsecond
  setPrescaler(72);

  // Load value to match
  setCounter(microsecond);

  // Set one-pulse mode
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= (0x01 << 3);

  // Enable timer
  *((volatile uint16_t *)(_baseAddress + gkOffsetControl1)) |= 0x01;

  // Enable interrupt on update event generation
  *((volatile uint16_t *)(_baseAddress + gkOffsetEventGeneration)) |= (0x01 << 6);

  // Wait for the update flag (UIF)
  while ( !(*((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) & 0x01))
    ;

  // Clear status bit
  *((volatile uint16_t *)(_baseAddress + gkOffsetStatus)) = 0x0;
}
