/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "afio.h"

/* NOTE: All AFIO register have to be accessed by words (32 bits) */

static const volatile uint8_t* const gkBaseAddress = reinterpret_cast<uint8_t *>(0x40010000);

static const uint8_t gkOffsetEventOut = 0x00;
static const uint8_t gkOffsetRemapConfiguration1 = 0x04;
static const uint8_t gkOffsetExternalInterrupt1  = 0x08;
static const uint8_t gkOffsetExternalInterrupt2  = 0x0C;
static const uint8_t gkOffsetExternalInterrupt3  = 0x10;
static const uint8_t gkOffsetExternalInterrupt4  = 0x14;
static const uint8_t gkOffsetRemapConfiguration2 = 0x1C;


namespace afio {


void
eventOUTDisable()
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEventOut)) &= ~(0x01 << 7);
}


void
eventOUTEnable(eGPIOPort port, eGPIOPin pin)
{
  uint8_t mask = (0x01 << 7) | (port << 4) | pin;
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEventOut)) &= 0xFFFFFF00;
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEventOut)) |= mask;
}


void
selectSourceForEvent(eEventGroup1 event, eGPIOPort port)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt1)) &= ~(0x0F << event);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt1)) |= (port << event);
}


void
selectSourceForEvent(eEventGroup2 event, eGPIOPort port)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt2)) &= ~(0x0F << event);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt2)) |= (port << event);
}


void
selectSourceForEvent(eEventGroup3 event, eGPIOPort port)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt3)) &= ~(0x0F << event);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt3)) |= (port << event);
}


void
selectSourceForEvent(eEventGroup4 event, eGPIOPort port)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt4)) &= ~(0x0F << event);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetExternalInterrupt4)) |= (port << event);
}

} /* end of namespace */
