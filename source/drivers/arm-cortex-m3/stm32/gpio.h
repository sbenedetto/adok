/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __GPIO_H__
#define __GPIO_H__

#include <stdint.h>

/**
 * \brief GPIO utility class.
 *
 * The following class allows the manipulation of the various GPIO ports
 * and its relative pins, hopefully in an easy way.
 */
class GPIO
{
public:

  /**
   * \brief Base address for GPIO ports on stm32f1x
   */
  enum eBaseAddress {
    ePortA = 0x40010800,
    ePortB = 0x40010C00,
    ePortC = 0x40011000,
    ePortD = 0x40011400,
    ePortE = 0x40011800,
    ePortF = 0x40011C00,
    ePortG = 0x40012000
  };

  /**
   * \brief Configuration mode for a pin
   */
  enum eMode {
    /* General purpose ouput */
    eOutputPushPull2Mhz               = 0x02,
    eOutputPushPull10Mhz              = 0x01,
    eOutputPushPull50Mhz              = 0x03,
    eOutputOpenDrain2Mhz              = 0x06,
    eOutputOpenDrain10Mhz             = 0x05,
    eOutputOpenDrain50Mhz             = 0x07,
    /* Alternate functions */
    eAlternateFunctionPushPull2Mhz    = 0x0A,
    eAlternateFunctionPushPull10Mhz   = 0x09,
    eAlternateFunctionPushPull50Mhz   = 0x0B,
    eAlternateFunctionOpenDrain2Mhz   = 0x0E,
    eAlternateFunctionOpenDrain10Mhz  = 0x0D,
    eAlternateFunctionOpenDrain50Mhz  = 0x0F,
    /* Input mode */
    eInputAnalog                      = 0x00,
    eInputFloating                    = 0x04,
    eInputPullUpPullDown              = 0x08
  };

  /**
   * \brief The dummy enum for pin selection
   */
  enum ePinNumber {
    ePin0 = 0,
    ePin1,
    ePin2,
    ePin3,
    ePin4,
    ePin5,
    ePin6,
    ePin7,
    ePin8,
    ePin9,
    ePin10,
    ePin11,
    ePin12,
    ePin13,
    ePin14,
    ePin15
  };

  GPIO(eBaseAddress port);
  ~GPIO();

  /**
   * \brief Return the state of the given pin
   *
   * \param pin GPIO pin to set
   * \return state of the pin. True if it is high, false otherwise.
   */
  bool    currentState(ePinNumber pin);

  /**
   * \brief Set high the output
   *
   * \param pin GPIO pin to set as high
   * \param atomic if true garantees an atomic set
   */
  void    high(ePinNumber pin, bool atomic = false);

  /**
   * \brief Set low the output
   *
   * \param pin GPIO pin to set as low
   * \param atomic if true garantees an atomic set
   */
  void    low(ePinNumber pin, bool atomic = false);

  /**
   * \brief Set mode for GPIO port
   *
   * \param pin GPIO pin to set
   * \param mode Mode to set for the given gpio port
   */
  void    setMode(ePinNumber pin, eMode mode);

  /**
   * \brief Toggle the output
   *
   * \param pin GPIO pin to toggle
   */
  void    toggle(ePinNumber pin);


private:
  uint8_t* const   baseAddress_;
};

#endif
