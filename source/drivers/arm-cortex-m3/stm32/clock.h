/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CLOCK_H__
#define __CLOCK_H__

/**
 * \brief Clock control utility class.
 *
 * The following namespace provides an interface for enabling/disabling and
 * configuring the various clocks present in the stm32f1x SoC.
 */
namespace clock
{
  /**
   * \brief The following enum provides the values for enabling/disabling
   *        the peripheral on the AHB bus
   */
   enum eAHBPeripheral {
     eSDIO    = (0x01 << 10),
     eFSMC    = (0x01 << 8),
     eCRC     = (0x01 << 6),
     eFLITF   = (0x01 << 4),
     eSRAM    = (0x01 << 2),
     eDMA2    = (0x01 << 1),
     eDMA1    = (0x01)
   };

  /**
   * \brief The following enum provides the value to set the scale factor for
   *        the Advanced High-Performance Bus clock
   */
  enum eAHBScaler {
    eAHBScaleBy1   = 0x00,
    eAHBScaleBy2   = 0x08,
    eAHBScaleBy4   = 0x09,
    eAHBScaleBy8   = 0x0A,
    eAHBScaleBy16  = 0x0B,
    eAHBScaleBy64  = 0x0C,
    eAHBScaleBy128 = 0x0D,
    eAHBScaleBy256 = 0x0E,
    eAHBScaleBy512 = 0x0F
  };

  /**
   * \brief The following enum provide the values to set the scale factor for
   *        the Advanced Peripheral Bus clock
   */
  enum eAPBScaler {
    eAPBScaleBy1   = 0x00,
    eAPBScaleBy2   = 0x04,
    eAPBScaleBy4   = 0x05,
    eAPBScaleBy8   = 0x06,
    eAPBScaleBy16  = 0x07
  };

  /**
   * \brief The following enum provides the values for enabling/disabling
   *        the peripheral on the APB1 bus
   */
  enum eAPB1Peripheral {
    eDACInterface    = (0x01 << 29),
    ePowerInterface  = (0x01 << 28),
    eBackupInterface = (0x01 << 27),
    eCAN             = (0x01 << 25),
    eUSB             = (0x01 << 23),
    eI2C2            = (0x01 << 22),
    eI2C1            = (0x01 << 21),
    eUART5           = (0x01 << 20),
    eUART4           = (0x01 << 19),
    eUART3           = (0x01 << 18),
    eUART2           = (0x01 << 17),
    eSPI3            = (0x01 << 15),
    eSPI2            = (0x01 << 14),
    eWindowWatchdog  = (0x01 << 11),
    eTimer14         = (0x01 << 8),
    eTimer13         = (0x01 << 7),
    eTimer12         = (0x01 << 6),
    eTimer7          = (0x01 << 5),
    eTimer6          = (0x01 << 4),
    eTimer5          = (0x01 << 3),
    eTimer4          = (0x01 << 2),
    eTimer3          = (0x01 << 1),
    eTimer2          = 0x01,
  };
 
  /**
   * \brief The following enum provides the values for enabling/disabling
   *        the peripheral on the APB2 bus
   */
  enum eAPB2Peripheral {
    eTimer11           = (0x01 << 21),
    eTimer10           = (0x01 << 20),
    eTimer9            = (0x01 << 19),
    eADC3              = (0x01 << 15),
    eUSART1            = (0x01 << 14),
    eTimer8            = (0x01 << 13),
    eSPI1              = (0x01 << 12),
    eTimer1            = (0x01 << 11),
    eADC2              = (0x01 << 10),
    eADC1              = (0x01 << 9),
    eGPIOG             = (0x01 << 8),
    eGPIOF             = (0x01 << 7),
    eGPIOE             = (0x01 << 6),
    eGPIOD             = (0x01 << 5),
    eGPIOC             = (0x01 << 4),
    eGPIOB             = (0x01 << 3),
    eGPIOA             = (0x01 << 2),
    eAFIO              = 0x01
   };

  /**
   * \brief The following enum provide the values to set the system clock source
   */
  enum eClockSource {
    eHighSpeedInternal = 0x00,
    eHighSpeedExternal = 0x01,
    ePLL               = 0x02
  };

  /**
   * \brief The following enum provide the values to set the multiply factor
   *        for the PLL
   */
  enum eMultiplier {
    eMultiplyBy2  = 0x00,
    eMultiplyBy3  = 0x01,
    eMultiplyBy4  = 0x02,
    eMultiplyBy5  = 0x03,
    eMultiplyBy6  = 0x04,
    eMultiplyBy7  = 0x05,
    eMultiplyBy8  = 0x06,
    eMultiplyBy9  = 0x07,
    eMultiplyBy10 = 0x08,
    eMultiplyBy11 = 0x09,
    eMultiplyBy12 = 0x0A,
    eMultiplyBy13 = 0x0B,
    eMultiplyBy14 = 0x0C,
    eMultiplyBy15 = 0x0D,
    eMultiplyBy16 = 0x0E
  };

  /**
   * \brief The following enum provide the values to set the PLL input clock
   */
  enum ePLLSource {
    ePLLHighSpeedInternal           = 0x00,
    ePLLHighSpeedExternal           = 0x01,
    ePLLHighSpeedExternalDividedBy2 = 0x03
  };

  /**
   * \brief Return the current source used as system clock
   */
  eClockSource    currentSystemClockSource();

  /**
   * \brief Enable clock for given peripheral on the correct bus
   *
   * \param peripheral peripheral to enable clock for
   *
   */
  void    enableClockFor(eAHBPeripheral peripheral);
  void    enableClockFor(eAPB1Peripheral peripheral);
  void    enableClockFor(eAPB2Peripheral peripheral);

  /**
   * \brief Enable High Speed External oscillator
   *
   * The method returns only when the clock is stable
   *
   * \param bypass When using an external clock source instead of a resonator
   *               set this parameter to true, otherwise leave it false
   */
  void    enableHighSpeedExternal(bool bypass = false);

  /**
   * \brief Enable High Speed Internal oscillator
   *
   * The method returns only when the clock is stable.
   * NOTE: After a system reset, the HSI oscillator is selected as system clock
   */
  void    enableHighSpeedInternal();

  /**
   * \brief Enable Low Speed External crystal oscillator
   *
   * The method returns only when the clock is stable
   *
   * \param bypass When using an external clock source instead of a resonator
   *               set this parameter to true, otherwise leave it false
   */
  void    enableLowSpeedExternal(bool bypass = false);

  /**
   * \brief Enable Low Speed Internal RC oscillator
   *
   * The method returns only when the clock is stable
   */
  void    enableLowSpeedInternal();

  /**
   * \brief Enable PLL.
   *
   * The method returns only when the PLL is locked
   */
  void    enablePLL();

  /**
   * \brief Disable High Speed External oscillator
   */
  void    disableHighSpeedExternal();

  /**
   * \brief Disable High Speed Internal oscillator
   */
  void    disableHighSpeedInternal();

  /**
   * \brief Disable Low Speed Internal RC oscillator
   */
  void    disableLowSpeedInternal();

  /**
   * \brief Disable PLL.
   */
  void    disablePLL();

  /**
   * \brief Set HSE as PLL input
   *
   * \param source define the clock to use as PLL input
   *
   * \note You can call this only before enabling the PLL
   */
  void    setPLLInput(ePLLSource source);

  /**
   * \brief Set multiplier factor for PLL
   *
   * \param multiplierFactor define the multiplier factor for the PLL
   *
   * \warning: You must call this only before enabling the PLL
   *           MAKE SURE the PLL output frequency does not exceed 72Mhz
   */
  void    setPLLMultiplierFactor(eMultiplier multiplierFactor);

  /**
   * \brief Set the prescaler for the Advanced High-performance Bus
   *
   * \param scaleFactor define the scale factor for the Advanced High
   *                    speed bus
   */
  void    setPrescalerForAHB(eAHBScaler scaleFactor);

  /**
   * \brief Set the prescaler for the Advanced Peripheral Bus 1
   *
   * \param scaleFactor define the scale factor for the Advanced
   *                    peripheral high speed bus
   *
   * \warning Do not exceed the 36MHz on this bus
   */
  void    setPrescalerForAPBLow(eAPBScaler scaleFactor);

  /**
   * \brief Set the prescaler for the Advanced Peripheral Bus 2
   *
   * \param scaleFactor define the scale factor for the Advanced 
   *                    peripheral low speed bus
   *
   * Unlike the APB1 this bus can be driven up to 72MHz
   */
  void    setPrescalerForAPBHigh(eAPBScaler scaleFactor);

  /**
   * \brief Set system clock source
   *
   * \param source define the clock to use
   *
   * NOTE: The clock you set as source must be enabled before this call
   */
  void    setSystemClockSource(eClockSource source);
};

#endif
