/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "clock.h"

#include <stdint.h>

/* Base interface address */
static volatile uint8_t* const gkBaseAddress   = (uint8_t*)0x40021000;

static const uint8_t gkOffsetControl              = 0x00;
static const uint8_t gkOffsetConfiguration        = 0x04;
static const uint8_t gkOffsetInterrupt            = 0x08;
static const uint8_t gkOffsetResetAPB2            = 0x0C;
static const uint8_t gkOffsetResetAPB1            = 0x10;
static const uint8_t gkOffsetEnableAHB            = 0x14;
static const uint8_t gkOffsetEnableAPB2           = 0x18;
static const uint8_t gkOffsetEnableAPB1           = 0x1C;
static const uint8_t gkOffsetBackupDomainControl  = 0x20;
static const uint8_t gkOffsetStatus               = 0x24;


namespace clock {


eClockSource
currentSystemClockSource()
{
  eClockSource source
    = static_cast<eClockSource>((*(gkBaseAddress + gkOffsetConfiguration) & (0x03 << 2)) >> 2);
  return source;
}


void
enableClockFor(eAHBPeripheral peripheral)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEnableAHB)) |= peripheral;
}


void
enableClockFor(eAPB1Peripheral peripheral)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEnableAPB1)) |= peripheral;
}


void
enableClockFor(eAPB2Peripheral peripheral)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetEnableAPB2)) |= peripheral;
}


void
enableHighSpeedExternal(bool bypass /* false default */)
{
  if (bypass)
    *((volatile uint32_t *)(gkBaseAddress + gkOffsetControl)) |= (0x01 << 18);

  *((volatile uint32_t *)(gkBaseAddress + gkOffsetControl)) |= (0x01 << 16);
  /* Return when the clock is stable */
  while ( (*((volatile uint32_t *)(gkBaseAddress + gkOffsetControl)) & (0x01 << 17)) == 0) ;
}


void
enableHighSpeedInternal()
{
  *(gkBaseAddress + gkOffsetControl) |= 0x01;
  /* Return when the clock is stable */
  while ( (*(gkBaseAddress + gkOffsetControl) & (0x01 << 1)) == 0) ;
}


void
enableLowSpeedExternal(bool bypass)
{
  *(gkBaseAddress + gkOffsetBackupDomainControl) |= 0x01;
  /* Return when the clock is stable */
  while ( (*(gkBaseAddress + gkOffsetBackupDomainControl) & (0x01 << 1)) == 0) ;
}


void
enableLowSpeedInternal()
{
  *(gkBaseAddress + gkOffsetStatus) |= 0x01;
  /* Return when the clock is stable */
  while ( (*(gkBaseAddress + gkOffsetStatus) & (0x01 << 1)) == 0) ;
}


void
enablePLL()
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetControl)) |= (0x01 << 24);
  /* Return when PLL is locked */
  while ( (*((volatile uint32_t *)(gkBaseAddress + gkOffsetControl)) & (0x01 << 25)) == 0 ) ;
}


void
disableHighSpeedExternal()
{
  *((uint32_t *)(gkBaseAddress + gkOffsetControl)) &= ~(0x01 << 16);
}


void
disableHighSpeedInternal()
{
  *(gkBaseAddress + gkOffsetControl) &= ~0x01;
}


void
disableLowSpeedInternal()
{
  *(gkBaseAddress + gkOffsetStatus) &= ~0x01;
}


void
disablePLL()
{
  *((uint32_t *)(gkBaseAddress + gkOffsetControl)) &= ~(0x01 << 24);
}


void
setPLLInput(ePLLSource source)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetConfiguration)) &= ~(0x03 << 16);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetConfiguration)) |= (source << 16);
}


void
setPLLMultiplierFactor(eMultiplier multiplierFactor)
{
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetConfiguration)) &= ~(0x0F << 18);
  *((volatile uint32_t *)(gkBaseAddress + gkOffsetConfiguration)) |= (multiplierFactor << 18);
}


void
setPrescalerForAHB(eAHBScaler scaleFactor)
{
  *(gkBaseAddress + gkOffsetConfiguration) &= ~(0x0F << 4);
  *(gkBaseAddress + gkOffsetConfiguration) |= (scaleFactor << 4);
}


void
setPrescalerForAPBLow(eAPBScaler scaleFactor)
{
  *((uint16_t *)(gkBaseAddress + gkOffsetConfiguration)) &= ~(0x07 << 8);
  *((uint16_t *)(gkBaseAddress + gkOffsetConfiguration)) |= (scaleFactor << 8);
}


void
setPrescalerForAPBHigh(eAPBScaler scaleFactor)
{
  *((uint16_t *)(gkBaseAddress + gkOffsetConfiguration)) &= ~(0x07 << 11);
  *((uint16_t *)(gkBaseAddress + gkOffsetConfiguration)) |= (scaleFactor << 11);
}


void
setSystemClockSource(eClockSource source)
{
  *((volatile uint8_t *)(gkBaseAddress + gkOffsetConfiguration)) &= ~0x03;
  *((volatile uint8_t *)(gkBaseAddress + gkOffsetConfiguration)) |= source;
}

} /* end namespace */
