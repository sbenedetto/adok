/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __AFIO_H__
#define __AFIO_H__

#include <stdint.h>


/**
 * \brief Alternate Function I/O (AFIO) utility class.
 *
 * The following namespace provides functions for manipulating the alternate
 * function configuration.
 */

namespace afio
{
  /**
   * \brief The following enum provides the value to use for configuring
   *        various external event interrupt line
   */
  enum eEventGroup1 {
    eEXTI0 = 0,
    eEXTI1 = 4,
    eEXTI2 = 8,
    eEXTI3 = 12
  };
  enum eEventGroup2 {
    eEXTI4 = 0,
    eEXTI5 = 4,
    eEXTI6 = 8,
    eEXTI7 = 12
 };
  enum eEventGroup3 {
    eEXTI8  = 0,
    eEXTI9  = 4,
    eEXTI10 = 8,
    eEXTI11 = 12
 };
  enum eEventGroup4 {
    eEXTI12 = 0,
    eEXTI13 = 4,
    eEXTI14 = 8,
    eEXTI15 = 12
  };

  enum eGPIOPin {
    ePin0 = 0,
    ePin1,
    ePin2,
    ePin3,
    ePin4,
    ePin5,
    ePin6,
    ePin7,
    ePin8,
    ePin9,
    ePin10,
    ePin11,
    ePin12,
    ePin13,
    ePin14,
    ePin15
  };

  enum eGPIOPort {
    ePortA = 0x00,
    ePortB = 0x01,
    ePortC = 0x02,
    ePortD = 0x03,
    ePortE = 0x04,
    ePortF = 0x05,
    ePortG = 0x06 
  };

  /**
   * \brief Disable event out
   */
  void    eventOUTDisable();

  /**
   * \brief Enable event out on given pin of the given GPIO port
   *
   * \param port GPIO port whose pin is connect to event out
   * \param pin pin number to connect event out to
   */
  void    eventOUTEnable(eGPIOPort port, eGPIOPin pin);

  /**
   * \brief Select source for given event
   *
   * \param event event to select source for
   * \param port port to select as source for the given event
   */
  void    selectSourceForEvent(eEventGroup1 event, eGPIOPort port);
  void    selectSourceForEvent(eEventGroup2 event, eGPIOPort port);
  void    selectSourceForEvent(eEventGroup3 event, eGPIOPort port);
  void    selectSourceForEvent(eEventGroup4 event, eGPIOPort port);
};

#endif
