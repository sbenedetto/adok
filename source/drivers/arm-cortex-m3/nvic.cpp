/*******************************************************************************
  License goes here

  Auhors:
          Salvatore Benedetto <salvatore.benedetto@gmail.com>

*******************************************************************************/

#include "nvic.h"

#include <kernel/barriers.h>

// TODO: clean up this madness!
typedef volatile uint8_t* const register_t;

static const register_t gkInterruptControlType    = (register_t)0xE000E004; // read-only


/* IRQ registers */
static       register_t gkSetEnableIRQBase        = (register_t)0xE000E100;
static       register_t gkClearEnableIRQBase      = (register_t)0xE000E180;
static       register_t gkSetPendingIRQBase       = (register_t)0xE000E200;
static       register_t gkClearPendingIRQBase     = (register_t)0xE000E280;
static const register_t gkActiveIRQBase           = (register_t)0xE000E300; // read-only
static       register_t gkPriorityIRQBase         = (register_t)0xE000E400;

/* General registers */
static const register_t gkCPUIDBase               = (register_t)0xE000ED00; //read-only
static       register_t gkInterruptControlState   = (register_t)0xE000ED04;
static       register_t gkVectorTableOffset       = (register_t)0xE000ED08;
static       register_t gkApplicationAndReset     = (register_t)0xE000ED0C;
static       register_t gkSystemControl           = (register_t)0xE000ED10;
static       register_t gkConfigurationControl    = (register_t)0xE000ED14;

/* System handler priority registers */
static       register_t gkSystemHandlerPriorityBase = (register_t)0xE000ED18;
static       register_t gkSystemHandlerControlState = (register_t)0xE000ED24;

/* Fault status registers */
static       register_t gkConfigurableFaultStatus  = (register_t)0xE000ED28;
static       register_t gkHardFaultStatus          = (register_t)0xE000ED2C;
static       register_t gkDebugFaultStatus         = (register_t)0xE000ED30;
static       register_t gkMemoryManageFaultAddress = (register_t)0xE000ED34;
static       register_t gkBusFaultAddress          = (register_t)0xE000ED38;
static       register_t gkAuxiliaryFaultAddress    = (register_t)0xE000ED3C;

/* Software trigger interrupt register */
static       register_t gkSoftwareTriggerInterrupt = (register_t)0xE000EF00;


void
NVIC::enableIRQ(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  *((uint32_t*)gkSetEnableIRQBase + group) |= (1 << shift);
}


void
NVIC::activeDelayMS(uint32_t delayMS)
{
  volatile uint32_t counter = (10 * 1000 * delayMS);
  // Active delay
  asm volatile("   ldr  r0, %0\n\t"
               "1: subs r0, r0, #1\n\t"
               "   bne  1b"
               : /* output */
               : /* input */ "m" (counter)
               : /* clobber */ "r0");
}


void
NVIC::activeDelayUS(uint32_t delayUS)
{
  volatile uint32_t counter = (10 * delayUS);
  // Active delay
  asm volatile("   ldr  r0, %0\n\t"
               "1: subs r0, r0, #1\n\t"
               "   bne  1b"
               : /* output */
               : /* input */ "m" (counter)
               : /* clobber */ "r0");
}


void
NVIC::disableIRQ(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  *(gkClearEnableIRQBase + group) |= (1 << shift);
}


void
NVIC::setPendSV()
{
  *((volatile uint32_t *)gkInterruptControlState) |= (0x01 << 28);
}


void
NVIC::clearPendSV()
{
  *gkInterruptControlState &= ~(0x01 << 28);
}


void
NVIC::setPendingIRQ(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  *(gkSetPendingIRQBase + group) |= (1 << shift);
}


void
NVIC::clearPendingIRQ(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  *(gkClearPendingIRQBase + group) |= (1 << shift);
}


bool
NVIC::IRQActive(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  return *(gkActiveIRQBase + group) & (1 << shift);
}


bool
NVIC::IRQEnabled(uint8_t number)
{
	uint8_t group = number / 32;
	uint8_t shift = number % 32;
	return (*gkSetEnableIRQBase + group) & (1 << shift);
}


bool
NVIC::IRQPending(uint8_t number)
{
  uint8_t group = number / 32;
  uint8_t shift = number % 32;
  return *(gkSetPendingIRQBase + group) & (1 << shift);
}


void
NVIC::setPriority(uint8_t number, uint8_t priority, uint8_t numberOfBitsUsed)
{
  /*
   * This depends on the SoC implementation, that is,
   * STM32 could use 4 bits, while another SoC could
   * use 3 bits to implement priorities. Thus the caller
   * must pass in the number of bits used. See Cortex-M3
   * complete guide pg 117.
   */
  uint8_t group = number / 32;
  uint8_t element = number / 4;
  uint8_t shift = 8 - numberOfBitsUsed;
  uint8_t* const gkPriorityGroup = (uint8_t* const)((uint32_t*)gkPriorityIRQBase + group);
  *(gkPriorityGroup + element) |= (priority << shift);
}


void
NVIC::stackAlign8(bool enable)
{
  if (enable)
    *((volatile uint16_t *)gkConfigurationControl) |= 0x0200;
  else
    *((volatile uint16_t *)gkConfigurationControl) &= ~0x0200;
}


void
NVIC::triggerSoftwareInterrupt(uint8_t number)
{
  *(gkSoftwareTriggerInterrupt) |= number;
}
