//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <stdint.h>

#include <drivers/stm32/clock.h>

// TODO: this is currently included in porting/startup.cpp
void board_setup_clock()
{
  /* Switch to 72 Mhz */
  /* Enable high-speed external oscillator */
  clock::enableHighSpeedExternal();

  /* External oscillator is 8Mhz */
  clock::ePLLSource src = clock::ePLLHighSpeedExternal;
  clock::setPLLInput(src);

  /* Multiply by 9 in order to get full speed (72Mhz) */
  clock::setPLLMultiplierFactor(clock::eMultiplyBy9);

  clock::enablePLL();

  clock::setPrescalerForAHB(clock::eAHBScaleBy1);
  clock::setPrescalerForAPBHigh(clock::eAPBScaleBy1);
  clock::setPrescalerForAPBLow(clock::eAPBScaleBy2);

  /* Set access wait state at 2 */
  *(volatile uint32_t *)0x40022000 |= 0x02;

  /* Set PLL as system clock */
  clock::setSystemClockSource(clock::ePLL);

  /* Wait untill PLL is used a system clock */
  while (clock::eClockSource source = clock::currentSystemClockSource())
    if (source == clock::ePLL)
      break;
}
