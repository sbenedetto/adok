// TODO: move this to its own library

#include "converter.h"

namespace utility {

uint8_t fromUInt16ToASCII(char* output, uint16_t x)
{
  if (x > 0) {
    output[0] = '+';
  } else {
    output[0] = '0';
    output[1] = '\0';
    return 1;
  }

  // Prevent compiler for using __aeabi_memcpy
  // uint16_t divs[] = { 10 * 1000, 1000, 100, 10, 1, 0 };
  uint16_t divs[6];
  divs[0] = 10 * 1000;
  divs[1] = 1000;
  divs[2] = 100;
  divs[3] = 10;
  divs[4] = 1;
  divs[5] = 0;

  uint8_t currentDigit = 0;
  uint8_t i, j;
  for (i = 0, j = 1; divs[i] != 0; i++) {

    currentDigit = (x / divs[i]);
    /* We don't want to rule out zeros unless it's
     * the first digit.
     * Ex:
     *  - 543 would yell two zeros first, which we do not want
     *  - 10301 would yell two zeros too, but we want those
     */
    if (currentDigit > 0 || j > 1) {

      output[j++] = currentDigit + '0';
      x -= (currentDigit * divs[i]);
    }
  }
  output[j] = '\0';
  return j;
}


uint8_t fromInt16ToASCII(char* output, int16_t x)
{
  if (x < 0) {
    output[0] = '-';
    x = -x;
  } else if (x > 0) {
    output[0] = '+';
  } else {
    output[0] = '0';
    output[1] = '\0';
    return 1;
  }

  // Prevent compiler for using __aeabi_memcpy
  // uint16_t divs[] = { 10 * 1000, 1000, 100, 10, 1, 0 };
  uint16_t divs[6];
  divs[0] = 10 * 1000;
  divs[1] = 1000;
  divs[2] = 100;
  divs[3] = 10;
  divs[4] = 1;
  divs[5] = 0;

  uint8_t currentDigit = 0;
  uint8_t i, j;
  for (i = 0, j = 1; divs[i] != 0; i++) {

    currentDigit = (x / divs[i]);
    /* We don't want to rule out zeros unless it's
     * the first digit.
     * Ex:
     *  - 543 would yell two zeros first, which we do not want
     *  - 10301 would yell two zeros too, but we want those
     */
    if (currentDigit > 0 || j > 1) {

      output[j++] = currentDigit + '0';
      x -= (currentDigit * divs[i]);
    }
  }
  output[j] = '\0';
  return j;
}

} // namespace utility
