/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Marco Minutoli <mminutoli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _REGISTERUTILITY_H_
#define _REGISTERUTILITY_H_

#include <stdint.h>

namespace Register
{

/**
 * \brief Register set.
 *
 * Set the register at (base+offset) address to value.
 *
 * \pre  None.
 * \param base Base addres.
 * \param offset Offset to be added to the base.
 * \param value Value to be set into the register.
 * \post value is set into the proper register.
 */
//@{
  
inline
void
set8(volatile uint8_t * const base, const uint8_t offset, uint8_t value)
{
  *(reinterpret_cast<volatile uint8_t *>(base + offset)) = value;
}

/*
inline
void
set(volatile uint8_t * const base, const uint8_t offset, uint16_t value)
{
  *(reinterpret_cast<volatile uint16_t *>(base + offset)) = value;
}
*/

inline
void
set(volatile uint8_t * const base, const uint8_t offset, uint32_t value)
{
  *(reinterpret_cast<volatile uint32_t *>(base + offset)) = value;
}
//@}


/**
 * \brief Register get.
 *
 * Get the value of the register at (base+offset).
 *
 * \pre  None.
 * \param base Base addres.
 * \param offset Offset to be added to the base.
 * \post None.
 */
inline
uint32_t
get(volatile uint8_t * base, uint8_t offset)
{
  return *(reinterpret_cast<volatile uint32_t *>(base + offset));
}


/**
 * \brief Set bit into register using a mask.
 *
 * Set bit into register at address (base+offset) using a mask.
 *
 * \pre None.
 * \param base Base addres.
 * \param offset Offset to be added to the base.
 * \param mask Mask indicating which bit to set.
 * \post Every bit in mask set to 1 has been set into the register at address
 *       (base + offset)
 */
//@{
  /*
inline
void
setBits(volatile uint8_t * base, uint8_t offset,  uint8_t mask)
{
  *(reinterpret_cast<volatile uint8_t *>(base + offset)) |= mask;
}


inline
void
setBits(volatile uint8_t * base, uint8_t offset,  uint16_t mask)
{
  *(reinterpret_cast<volatile uint16_t *>(base + offset)) |= mask;
}

*/
inline
void
setBits(volatile uint8_t* base, uint8_t offset, uint32_t mask)
{
  *(reinterpret_cast<volatile uint32_t *>(base + offset)) |= mask;
}
//@}


inline
bool
isBitSet(volatile uint8_t* base, uint8_t offset, uint32_t mask)
{
  return *(reinterpret_cast<volatile uint32_t *>(base + offset)) & mask;
}

/**
 * \brief Clear bit into register using a mask.
 *
 * Clear bit into register at address (base+offset) using a mask.
 *
 * \pre None.
 * \param base Base addres.
 * \param offset Offset to be added to the base.
 * \param mask Mask indicating which bit to clear.
 * \post Every bit in mask set to 1 has been cleared into the register
 *       at address (base + offset).
 */
//@{
/*
inline
void
clearBits(volatile uint8_t * base, uint8_t offset, uint8_t mask)
{
  *(reinterpret_cast<volatile uint8_t *>(base + offset)) &= ~(mask);
}

inline
void
clearBits(volatile uint8_t * base, uint8_t offset, uint16_t mask)
{
  *(reinterpret_cast<volatile uint16_t *>(base + offset)) &= ~(mask);
}
*/


inline
void
clearBits(volatile uint8_t * base, uint8_t offset, uint32_t mask)
{
  *(reinterpret_cast<volatile uint32_t *>(base + offset)) &= ~(mask);
}
//@}

}

#endif /* _REGISTERUTILITY_H_ */
