#ifndef __CONVERTER_H__
#define __CONVERTED_H__

namespace utility {

  uint8_t fromInt16ToASCII(char* buffer, int16_t x);
  uint8_t fromUInt16ToASCII(char* buffer, uint16_t x);

}

#endif
