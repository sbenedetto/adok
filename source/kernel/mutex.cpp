//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "mutex.h"

#include "barriers.h"
#include "scheduler.h"
#include "system.h"
#include "task_base.h"


enum class Mutex::eState_t : uint8_t {eLocked, eUnlocked};


Mutex::Mutex()
  : _locker(eState_t::eUnlocked)
{
}


void
Mutex::lock()
{
  ATOMIC_SECTION_BEGIN;

  if (_locker == eState_t::eLocked) {
    // Block caller
    gCurrentRunningTask->currentState = taskState_t::eBlocked;
    _waitingTasks.push_back(gCurrentRunningTask);
    // The following re-enable interrups and won't come back
    Scheduler::scheduleNext();
    // We just woke up and got the mutex. Interrupts enabled.

  } else {
    _locker = eState_t::eLocked;
    ATOMIC_SECTION_END;
  }

}


void
Mutex::unlock()
{
  ATOMIC_SECTION_BEGIN;

  if (!_waitingTasks.empty()) {
    // Wake up waiting task
    TaskBase* task = _waitingTasks.front();
    _waitingTasks.pop_front();
    task->currentState = taskState_t::eReady;
    // If the following cause a pre-emption
    // it won't come back
    Scheduler::reschedule(task);

  } else {
    _locker = eState_t::eUnlocked;
  }

  ATOMIC_SECTION_END;
}


bool
Mutex::tryAndLock()
{
  bool result = false;

  ATOMIC_SECTION_BEGIN;

  if (_locker == eState_t::eUnlocked) {
    _locker = eState_t::eLocked;
    result = true;
  }

  ATOMIC_SECTION_END;

  return result;
}
