//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __SCHEDULER_ROUNDROBIN_H__
#define __SCHEDULER_ROUNDROBIN_H__


// TODO: this need to be decoupled in order to be architecture indipendent
#include <drivers/nvic.h>

#include "containerAllocator.h"
#include "itemlist.h"

#include "task.h"


template<class container_t>
SchedulerImp<container_t>::SchedulerImp()
{
}


template<class container_t>
SchedulerImp<container_t>::~SchedulerImp()
{
}


template<class container_t>
void
SchedulerImp<container_t>::init()
{
  // Ready...
  gCurrentRunningTask = _readyTasks.front();
  // Steady..
  _readyTasks.pop_front();
  // Go!
  gHighestPriorityReadyTask = gCurrentRunningTask;
}


template<class container_t>
void
SchedulerImp<container_t>::addTask(TaskBase* task)
{
  if (task->type != taskType_t::eIdle)
    _readyTasks.push_back(task);
  else
    _idleTask = task;
}


template<class container_t>
void
SchedulerImp<container_t>::removeTask(TaskBase* task)
{
  if (!_readyTasks.empty())
    _readyTasks.remove(task);
}


template<class container_t>
void
SchedulerImp<container_t>::reschedule()
{
  // The scheduler only set up the gHighestPriorityReadyTask.
  // Since we are called from an interrupt service routine,
  // on exit if the highest priority ready task is different
  // from the current running task, the context switch routine
  // will be called

  // First of all, disable the timer
  NVIC::disableSysTick();

  // Possible cases
  //   1) IdleTask is running and not other task are ready
  //   2) IdleTask is running but there are other task (finally) ready
  //   3) IdleTask is not involed

  // If there are not other task ready, just reschedule gCurrentRunningTask
  if (!_readyTasks.empty()) {

    // Idle task must not be pushed to the ready queue.
    if (gCurrentRunningTask != _idleTask)
      _readyTasks.push_back(gCurrentRunningTask);

    // Schedule the next ready
    gHighestPriorityReadyTask = _readyTasks.front();
    _readyTasks.pop_front();
  }

  // Re-enable the timer with a new quanto value
  NVIC::enableSysTick(gHighestPriorityReadyTask->msToPreemption);
}


template<class container_t>
void
SchedulerImp<container_t>::reschedule(TaskBase* task)
{
  // If and only if, the current running task is the idle task
  // we want to update the gHighestPriorityReadyTask, thus causing
  // a context switch
  if (gCurrentRunningTask == _idleTask) {
    // Schedule the given task
    NVIC::disableSysTick();
    gHighestPriorityReadyTask = task;
    NVIC::enableSysTick(gHighestPriorityReadyTask->msToPreemption);
  } else {
    // Simple push the task to the ready queue
    _readyTasks.push_back(task);
  }
}


template<class container_t>
void
SchedulerImp<container_t>::scheduleNext()
{
  NVIC::disableSysTick();

  if (!_readyTasks.empty()) {
    // Set the new highest ready task
    gHighestPriorityReadyTask = _readyTasks.front();
    _readyTasks.pop_front();
  } else {
    // If no other task is ready, schedule IdleTask
    gHighestPriorityReadyTask = _idleTask;
  }

  // Re-enable the timer with a new quanto value
  NVIC::enableSysTick(gHighestPriorityReadyTask->msToPreemption);
}


template<class container_t>
void
SchedulerImp<container_t>::endPeriodicCycle()
{
  // TODO
  while (1);
}


template<class container_t>
void
SchedulerImp<container_t>::endAperiodicCycle()
{
  // TODO
}


template<class container_t>
void
SchedulerImp<container_t>::lock()
{
  // TODO
}


template<class container_t>
void
SchedulerImp<container_t>::unlock()
{
  // TODO
}


TaskBase* gCurrentRunningTask;
TaskBase* gHighestPriorityReadyTask;

//SchedulerImp<std::list<TaskBase*, ContainerAllocator<TaskBase*>>> gScheduler;
SchedulerImp< ItemList<TaskBase*> > gScheduler;

#endif
