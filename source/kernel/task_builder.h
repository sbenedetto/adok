//
// This file is part of the adok project.
//
// Copyright (C) 2013 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __TASK_BUILDER_H__
#define __TASK_BUILDER_H__

#include "application.h"
#include "system.h"
#include "task_base.h"
#include "task_periodic.h"
#include "task_soft.h"


// Each task type will provide a specialization of the following templates.
// Defaults not provided on purpose.
template<taskType_t type> struct TaskData;
template<taskType_t type> inline void taskEndCycle();


// Task Periodic Specialization
template<>
struct TaskData<taskType_t::ePeriodic>
{
  typedef TaskPeriodic extraData;
};


template<>
inline
void taskEndCycle<taskType_t::ePeriodic>()
{
  System::waitForNextRun();
}


// Task Soft Specialization
template<>
struct TaskData<taskType_t::eSoft>
{
  typedef TaskSoft extraData;
};


template<>
inline
void taskEndCycle<taskType_t::eSoft>()
{
  // Do nothing. Keep running
}


// Task Sporadic Specialization
// TODO


///
/// \brief Idle task specialization
///
struct TaskIdle {
 // empty struct
};


template<>
struct TaskData<taskType_t::eIdle>
{
  typedef TaskIdle extraData;
};


template<>
inline
void taskEndCycle<taskType_t::eIdle>()
{
  // Do nothing
}


template<Application::eState_t state>
inline
void checkState()
{
  if (state & System::currentState()) {
    return; // keep running
  }

  System::waitForState();
}


template<>
inline
void checkState<EXECUTE_ALWAYS>()
{
  // Do nothing
  // execute_always
}


///
/// \def TASK_IDLE(idleTask)
/// \brief Macro used to define an idleTask
///
#define TASK_IDLE(idleBody)                                           \
static TaskIdle idleParam;                                            \
static TaskBuilder<idleBody, taskType_t::eIdle> _(idleParam);


// Type to be used for defining the body of a task
typedef void (*taskBody_t)();


///
/// \brief TaskBuilder template class
///
/// The following template allows to declare a task very easily (hopefully).
/// The user needs only to provide a task body defined like in this example
///
/// \code
/// void myTask() {
///   domeSomething;
/// }
///
/// TASK_PERIODIC_EVERY_MS(myTask, 10);
/// \endcode
///
template<
        taskBody_t              taskBody,
        taskType_t              taskType,
        Application::eState_t   state = EXECUTE_ALWAYS,
        uint16_t                stackSizeInBytes = 1024
        >
class TaskBuilder
{
public:
  TaskBuilder(typename TaskData<taskType>::extraData extraData)
    : _taskExtraData(extraData)
  {
    // Base
    // TODO: fix the ugliness
    _task.stackTop       = (porting::types::stack_t)(&_stack[stackSizeInBytes / sizeof(porting::types::stack_t)]);
    _task.entryPoint     = (porting::types::entryPoint_t)&run;
    _task.stackSize      = stackSizeInBytes;
    _task.id             = System::getUniqueTaskID();
    _task.type           = taskType;
    _task.currentState   = taskState_t::eReady;
    _task.executeOnState = state;

    // TODO: provide IF_DEBUG_ON or if_enable to debug the stack or other stuff

    System::registerTask(&_task);
  }

private:

  static void run()
  {
    while (true) {
      // For stateless task the compiler will optimize this out
      checkState<state>();

      taskBody();

      checkState<state>();
      taskEndCycle<taskType>();
    }
  }

  // Disable default constructor
  TaskBuilder();

  // The following two are layed out one after the other in order
  // to access the extraData structure given the pointer to the TaskBase
  // structure, by simply adding sizeof(TaskBase) to the pointer
  TaskBase                                   _task;
  typename TaskData<taskType>::extraData     _taskExtraData;

  // Stack used by the task
  porting::types::stack_t  _stack[stackSizeInBytes / sizeof(porting::types::stack_t)];
};


///
/// \def TASK_PERIODIC_EVERY_MS(taskBody, periodMS)
///
/// The following macro further simplify how to declare a task
///
#define TASK_PERIODIC_EVERY_MS(taskBody, periodMS)                    \
static TaskPeriodic periodicParam(periodMS);                          \
static TaskBuilder<taskBody, taskType_t::ePeriodic> _##taskBody(periodicParam);


///
/// \def TASK_PERIODIC_EVERY_MS_OFFSET(taskBody, periodMS, offset)
///
/// The following macro further simplify how to declare a task
///
#define TASK_PERIODIC_EVERY_MS_OFFSET(taskBody, periodMS, offset)        \
static TaskPeriodic periodicParam(periodMS, offset);                     \
static TaskBuilder<taskBody, taskType_t::ePeriodic> _##taskBody(periodicParam);

///
/// \def TASK_PERIOD_STATE(taskBody, periodMS, state)
///
#define TASK_PERIOD_STATE(taskBody, periodMS, state)          \
static TaskPeriodic periodicParam(periodMS);                  \
static TaskBuilder<taskBody, taskType_t::ePeriodic, state> _##taskBody(periodicParam);

#endif // __TASK_BUILDER_H__
