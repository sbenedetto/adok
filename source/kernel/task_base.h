//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __TASK_BASE_H__
#define __TASK_BASE_H__

#include <porting/cpu_defs.h>

/// \defgroup task Tasks related data

///
/// \enum taskType_t
/// \brief Describe the type of task. Each type is scheduled differently
/// \ingroup task
///
enum class taskType_t : uint8_t {
  ePeriodic       = 0,       ///< Periodic task scheduled with EDF
  eSporadic,                 ///< Sporadic task scheduled with ...
  eSoft,                     ///< Soft task are scheduled with Round Robin
  eIdle           = 0x80     ///< Idle task scheduled when no other task is ready
};


///
/// \enum taskState_t
/// \brief Describe the current state of the task
/// \ingroup task
///
/// \todo internal data
enum class taskState_t : uint8_t {
  eRunning = 0,   ///< Currently running
  eReady,         ///< Task is waiting in the ready queue of its scheduler
  eSleeping,      ///< Task is waiting for the next run
  eBlocked,       ///< Task is blocked on some synchronization primitive
  eNotInState     ///< Task can't run in the current system state
};


// Forward declaration of Application::eState_t
enum class Application::eState_t : uint8_t;

///
/// \brief Task Common Block
/// \ingroup task
///
/// Contains the data common to all task despite of their type and the scheduler
/// used for scheduling them
struct TaskBase {
  porting::types::stack_t        stackTop;      // This must always be the first
  porting::types::entryPoint_t   entryPoint;
  uint16_t                       stackSize;
  taskType_t                     type;
  uint8_t                        id;
  taskState_t                    currentState; // TODO: rename to currentStatus
  Application::eState_t          executeOnState;
};

#endif // __TASK_BASE_H__
