#ifndef __LIST_POINTER_H__
#define __LIST_POINTER_H__

#include <stdint.h>
// TODO: make the following a itemNodeAllocator
#include "memoryBlockAllocator.h"

#include "barriers.h"

// Default implementation is not provided on purpose
template<class Item> class ItemList;

// Pointer Template Specialization
template<class Item>
class ItemList<Item *>
{
public:
  ItemList() : _head(NULL), _tail(NULL), _items(0) {}

  bool empty() { return _head == NULL; }

  uint16_t items()
  {
    return _items;
  }

  Item* front()
  {
    // Caller garantees the list is not empty
    return _head->_item;
  }

  void pop_front()
  {
    Node* tmp = _head;
    // Caller garantees the list is not empty
    _head = _head->_next;

    if (_head == NULL)
      _tail = NULL;
    else
      _head->_prev = NULL;

    MemoryBlockAllocator::deallocate(tmp, sizeof(Node));

    --_items;
  }

  void push_front(Item* item)
  {
    Node* newNode = reinterpret_cast<Node *>(MemoryBlockAllocator::allocate(sizeof(Node)));

    newNode->_item = item;
    newNode->_next = NULL;
    newNode->_prev = NULL;

    if (_tail == NULL) {
      _tail = newNode;
    } else {
      _head->_prev = newNode;
      newNode->_next = _head;
    }

    _head = newNode;
    ++_items;
  }

  void push_back(Item* item)
  {
    Node* newNode = reinterpret_cast<Node *>(MemoryBlockAllocator::allocate(sizeof(Node)));

    newNode->_item = item;
    newNode->_next = NULL;
    newNode->_prev = NULL;

    if (_head == NULL) {
      _head = newNode;
    } else {
      _tail->_next = newNode;
      newNode->_prev = _tail;
    }

    _tail = newNode;
    ++_items;
  }


  // TODO: provide proper data containers (like priority queue)
  void push_sorted(Item* item, bool (*less)(Item* lhs, Item* rhs))
  {
    Node* newNode = reinterpret_cast<Node *>(MemoryBlockAllocator::allocate(sizeof(Node)));

    newNode->_item = item;
    newNode->_next = NULL;
    newNode->_prev = NULL;

    // Basic case, list is empty
    if (_head == NULL) {

      _head = newNode;
      _tail = newNode;

    } else {

      Node* current = _head;
      bool inserted = false;

      while (current) {

        // Are we already in the fucking queue?
        if (item == current->_item)
          ASSERT_BREAK;

        if (less(item, current->_item)) {
          // Insert before current
          newNode->_prev = current->_prev;
          newNode->_next = current;
          // Update current
          current->_prev = newNode;
          // If it was only one node, update head
          if (_head == current)
            _head = newNode;
          else // Update predecessor
            newNode->_prev->_next = newNode;

          // Stop cycling
          inserted = true;
          break;
        }
        current = current->_next;
      }

      if (!inserted) {
        // Place it at the end of the list
        _tail->_next = newNode;
        newNode->_prev = _tail;
        _tail = newNode;
      }
    }

    ++_items;
  }


  void remove(Item* item)
  {
     Node* tmp = _head;

     // Not need to check if it's actually in the queue
     while (item != tmp->_item)
       tmp = tmp->_next;

    // It it's on top of the list, modify head as well
    if (tmp == _head) {
      _head = _head->_next;

      if (_head == NULL)
        _tail = NULL;
      else
        _head->_prev = NULL;

    } else {

      tmp->_prev->_next = tmp->_next;
      if (tmp->_next)
        tmp->_next->_prev = tmp->_prev;
      else
        // It was the last one, modify tail
        _tail = tmp->_prev;

      --_items;
    }

    MemoryBlockAllocator::deallocate(tmp, sizeof(Node));
  }


private:

  struct Node
  {
    Node() : _item(NULL), _next(NULL), _prev(NULL) {}

    Item* _item;
    Node* _next;
    Node* _prev;
  };

  Node*    _head;
  Node*    _tail;
  uint16_t _items;
};

#endif
