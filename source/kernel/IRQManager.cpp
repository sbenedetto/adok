//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "IRQManager.h"

#include <stdint.h>


namespace IRQManager
{

struct IRQTable {
  void (*handler)(void *data);
  void* data;
};

// TODO: If we need a bigger table
//  1) Forward declare eIRQNumber_t as uint16_t
//  2) Request a global symbol gkTotalNumberOfISR from the libdrivers
static IRQTable gIRQTable[256];

void
callHandler(eIRQNumber_t ISRNumber)
{
  void* data = gIRQTable[ISRNumber].data;
  gIRQTable[ISRNumber].handler(data);
}


void
registerHandler(eIRQNumber_t ISRNumber, void (*handler)(void *), void* data)
{
  gIRQTable[ISRNumber].handler = handler;
  gIRQTable[ISRNumber].data = data;
}


void
relocateInterruptVector(uint32_t from, uint32_t to, uint16_t size)
{
  // TODO
}

} // namespace
