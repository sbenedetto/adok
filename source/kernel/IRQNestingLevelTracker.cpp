//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "IRQNestingLevelTracker.h"

#include <stdint.h>

#include <porting/porting.h>

#include "barriers.h"
#include "scheduler.h"
#include "system.h"


static uint8_t gNestedLevel = 0;


IRQNestingLevelTracker::IRQNestingLevelTracker()
{
  ATOMIC_SECTION_BEGIN;
  ++gNestedLevel;
  ATOMIC_SECTION_END;
}


IRQNestingLevelTracker::~IRQNestingLevelTracker()
{
  ATOMIC_SECTION_BEGIN;

  --gNestedLevel;
  if (gNestedLevel == 0)
  {
    if (gCurrentRunningTask != gHighestPriorityReadyTask)
      porting::arch::request_context_switch(true /* fromISR */);
  }

  ATOMIC_SECTION_END;
}


bool
IRQNestingLevelTracker::withinIRQ()
{
  return gNestedLevel > 0;
}
