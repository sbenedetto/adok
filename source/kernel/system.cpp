//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "system.h"

#include <porting/porting.h>

#include "adokInternalDefs.h"
#include "application.h"
#include "IRQNestingLevelTracker.h"
#include "scheduler.h"
#include "task_base.h"

// TODO: this is application specific!
//extern void board_setup_clock();

// Global objects
extern uint32_t _init_array_start;
extern uint32_t _init_array_end;

struct TaskTable {
  taskType_t     type;
  TaskBase*      baseAddress;
};

// System class context
static TaskTable gTaskTable[gkNumberOfTasks];
static uint8_t   gTaskID = 0;

static Application::eState_t gCurrentApplicationState;


uint8_t
System::getUniqueTaskID()
{
  return gTaskID++;
}


Application::eState_t
System::currentState()
{
  return gCurrentApplicationState;
}


void
System::setState(Application::eState_t state)
{
  gCurrentApplicationState = state;
  Application::onChangeState(state);
}


void
System::init()
{
  // Set up system clock (board specific)
  // TODO: currently done in startup function
  // board_setup_clock();

  // Initialize C++ enviroment. Call constructors of global objects
  auto start = reinterpret_cast<uint32_t*>(&_init_array_start);
  auto end = reinterpret_cast<uint32_t*>(&_init_array_end);
  while (start != end) {
    auto fn = reinterpret_cast<void(*)()>(*start);
    (*fn)();
    ++start;
  }

  // Init the application
  // Application specific code goes here.
  // (e.g. controllers setup used by the application)
  Application::init();

  // Let the scheduler initialize some data
  // if it needs to. Tasks are all registered
  // to the System by now, but not to the scheduler
  Scheduler::init();

  // Add all register tasks to the scheduler ready queue
  // that match the current system state
  for (uint8_t i = 0; i < gTaskID; i++) {
    TaskBase* task = gTaskTable[i].baseAddress;
    if (task->executeOnState == EXECUTE_ALWAYS
        || task->executeOnState & System::currentState() ) {
      Scheduler::addTask(task);
    } else {
      task->currentState = taskState_t::eNotInState;
    }
  }

  // The following is architecture dependent.
  // It never returs.
  Scheduler::start();
}


void
System::registerTask(TaskBase* task)
{
  // First of all, we need to set up the stack for the
  // the given task. This is architecture dependent.
  porting::arch::initialize_stack(task->stackTop, task->stackSize, task->entryPoint);

  //
  // Add task to system table as well
  //
  // -------------------
  // | Type   |  &task |
  // |--------|--------|
  // |  aaaa  |  xxxx  |
  // |--------|--------|
  // |  bbbb  |  yyyy  | <--- table[task.id]
  // |--------|--------|
  // .        .        .
  // .        .        .
  // -------------------
  //
  gTaskTable[task->id].type = task->type;
  gTaskTable[task->id].baseAddress = task;
}


void
System::registerForEvent(Event* event)
{
  // TODO;
}


void
System::requestContextSwitch()
{
  bool fromISR = IRQNestingLevelTracker::withinIRQ();
  porting::arch::request_context_switch(fromISR);
}


void
System::waitForNextEvent()
{
  // TODO: probably analogous to waitForNextRun
  //       endAperiodicCycle + scheduleNext
}


void
System::waitForNextRun()
{
  // Put the currently running task to sleep
  // and schedule the next ready
  Scheduler::endPeriodicCycle();
}


void
System::waitForState()
{
  // Block the caller
  gCurrentRunningTask->currentState = taskState_t::eNotInState;
  // The following will re-enable interrupts and won't return
  // until we have the semaphore
  Scheduler::scheduleNext();
}


void
System::wakeTasksForCurrentState()
{
  // Ugly as hell. TO BE IMPROVED
  for (uint8_t i = 0; i < gkNumberOfTasks; ++i) {
    TaskBase* currentTask = gTaskTable[i].baseAddress;

    if (currentTask->currentState == taskState_t::eNotInState
        && currentTask->executeOnState & gCurrentApplicationState) {

      // Activate task back
      // TODO: the following two must be atomic
      currentTask->currentState = taskState_t::eReady;
      Scheduler::addTask(currentTask);
      // Start the task right away otherwise it'll miss
      // the first deadline
      Scheduler::scheduleNext();
    }
  }
}
