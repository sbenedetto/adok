

// TODO: this need to be architecture indipendent

// Ensures that all memory accesses are completed before new
// memory access is committed
#define DATA_MEMORY_BARRIER asm volatile ("DMB");

// Ensures that all memory accesses are completed before
// next instruction is executed
#define DATA_SYNCHRONIZATION_BARRIER asm volatile ("DSB");

// Flushes the pipeline and ensures that all previous
// instruction are completed before executing new instructions
#define INSTRUCTION_SYNCHRONIZATION_BARRIER asm volatile ("ISB");

// TODO: add arch_critical_section_[begin|end]
#define ATOMIC_SECTION_BEGIN    asm volatile("cpsid i");
#define ATOMIC_SECTION_END      asm volatile("cpsie i");


#define ASSERT_BREAK            asm volatile("bkpt #0");
