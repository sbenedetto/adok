#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <stdint.h>

namespace Application
{

  // Each application defines the actual value of the states
  enum class eState_t : uint8_t;

  // Macro have not scopes.. and 0 means execute always
  #define EXECUTE_ALWAYS static_cast<Application::eState_t>(0)

  inline bool operator&(const eState_t rhs, const eState_t lhs)
  {
    return static_cast<uint8_t>(rhs) & static_cast<uint8_t>(lhs);
  }

extern void   init();

extern void   onChangeState(eState_t newState);

};

#endif
