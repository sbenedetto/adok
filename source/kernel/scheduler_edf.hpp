//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __SCHEDULER_EDF_H__
#define __SCHEDULER_EDF_H__

#include <porting/porting.h>
#include <porting/systick.h>
#include <porting/timer.h>

#include "barriers.h"
#include "debug.h"
#include "containerAllocator.h"
#include "itemlist.h"
#include "IRQNestingLevelTracker.h"
#include "system.h"
#include "task_base.h"
#include "task_periodic.h"


static bool lessAbsoluteDeadlineMS(TaskBase* left, TaskBase* right);
static bool lessNextActivationMS(TaskBase* left, TaskBase* right);


// DEBUG
#define SAMPLES 10
struct scheduling_info_t {
  uint32_t    deadlines[SAMPLES];
  uint32_t    nextActivationInTicks[SAMPLES];
  uint8_t     currentDeadline;
  uint8_t     currentActivation;
};
IF_DEBUG_ON(scheduling_info_t gScheduling_info[6]);

struct timers_info_t {
  uint32_t fireIn[SAMPLES];
  uint8_t  current;
};
IF_DEBUG_ON(timers_info_t gTimer_info);

struct activation_time_t {
  uint32_t activatedAt;
};
IF_DEBUG_ON(activation_time_t gActivationTime[6]);

// When not used is removed by compiler as dead-code
inline void track_deadline(uint32_t deadline)
{
  uint8_t id = gCurrentRunningTask->id;
  uint8_t sample = gScheduling_info[id].currentDeadline;
  gScheduling_info[id].deadlines[sample] = deadline;
  gScheduling_info[id].currentDeadline = ++sample % SAMPLES;
}

// When not used is removed by compiler as dead-code
inline void track_nextActivation(uint32_t nextActivationInTicks)
{
  uint8_t id = gCurrentRunningTask->id;
  uint8_t sample = gScheduling_info[id].currentActivation;
  gScheduling_info[id].nextActivationInTicks[sample] = nextActivationInTicks;
  gScheduling_info[id].currentActivation = ++sample % SAMPLES;
}

inline void track_timer(uint32_t fireAt)
{
  uint8_t current = gTimer_info.current;
  gTimer_info.fireIn[current] = fireAt;
  gTimer_info.current = ++current % SAMPLES;
}

inline void track_activation_time(uint32_t activatedAt)
{
  uint8_t id = gCurrentRunningTask->id;
  gActivationTime[id].activatedAt = activatedAt;
}


template<class container_t>
SchedulerImp<container_t>::SchedulerImp()
{
}


template<class container_t>
SchedulerImp<container_t>::~SchedulerImp()
{
}


template<class container_t>
void
SchedulerImp<container_t>::init()
{
  // Enable the timer used for keeping track of tasks deadline
  porting::timer::start();
}

template<class container_t>
void
SchedulerImp<container_t>::start()
{
  // Pop the first stack on ready list
  gCurrentRunningTask = _readyTasks.front();
  _readyTasks.pop_front();
  // Run it..
  gHighestPriorityReadyTask = gCurrentRunningTask;

  // TODO: this should go to the more generale Scheduler::start if more
  //       scheduler are used. It should be called after all schedulers
  //       start has been called
  porting::arch::start_scheduler();
}


template<class container_t>
void
SchedulerImp<container_t>::addTask(TaskBase* task)
{
  if (task->type != taskType_t::eIdle) {
    // Set the deadline
    TaskPeriodic* extra = TASK_PERIODIC(task);

    uint16_t numberOfTicks   = porting::timer::numberOfTicks();
    uint16_t periodInTicks   = porting::timer::MSToTicks(extra->periodMS);
    uint16_t startAfterTicks = porting::timer::MSToTicks(extra->startAfterMS);

    extra->deadlineInTicks
      = startAfterTicks + periodInTicks + numberOfTicks;

    _readyTasks.push_sorted(task, lessAbsoluteDeadlineMS);

    IF_DEBUG_ON(track_activation_time(numberOfTicks));

  } else {
    _idleTask = task;
  }
}


template<class container_t>
void
SchedulerImp<container_t>::removeTask(TaskBase* task)
{
  ATOMIC_SECTION_BEGIN;
  if (!_readyTasks.empty())
    _readyTasks.remove(task);
  ATOMIC_SECTION_END;
}


template<class container_t>
void
SchedulerImp<container_t>::reschedule()
{
  // This is the timer handler used to re-active periodic task
  porting::systick::stop();

  // If we got called there is someone in the _sleepingTask, not
  // need to check
  TaskBase* task = _sleepingTasks.front();
  _sleepingTasks.pop_front();

  // First of all, calculate the new deadline
  uint16_t numberOfTicks = porting::timer::numberOfTicks();

  // Did we woke up too early?
  TaskPeriodic* periodic = TASK_PERIODIC(task);
  // ICTOH: if (e1 - e2) < 0   ===> t(e1) < t(e2)
  if ((int16_t)((numberOfTicks + 2) - TASK_PERIODIC(task)->nextActivationInTicks) < 0)
    ASSERT_BREAK;

  // Calculate new deadline
  uint16_t periodInTicks = porting::timer::MSToTicks(TASK_PERIODIC(task)->periodMS);
  TASK_PERIODIC(task)->deadlineInTicks += periodInTicks;
  task->currentState = taskState_t::eReady;

  // Check for preemption
  if (gCurrentRunningTask == _idleTask) {

    // When preempting, always check if there was already another pre-emption
    // before us, in which case we must put the task back in the ready queue.
    // For example an mutex.unlock just happed right before us
    if (gHighestPriorityReadyTask != _idleTask)
      _readyTasks.push_sorted(gHighestPriorityReadyTask, lessAbsoluteDeadlineMS);

    gHighestPriorityReadyTask = task;

  } else if ((int16_t)(TASK_PERIODIC(task)->deadlineInTicks - TASK_PERIODIC(gHighestPriorityReadyTask)->deadlineInTicks) < 0) {

    // When preempting, always check if there was already another pre-emption
    // before us, in which case we must put the task back in the ready queue
    if (gHighestPriorityReadyTask != gCurrentRunningTask
        && gHighestPriorityReadyTask != _idleTask)
      _readyTasks.push_sorted(gHighestPriorityReadyTask, lessAbsoluteDeadlineMS);

    gHighestPriorityReadyTask = task;
    // gCurrentRunningTask can't be the idleTask. Save to push it.
    _readyTasks.push_sorted(gCurrentRunningTask, lessAbsoluteDeadlineMS);

  } else {
    // No pre-emption
    _readyTasks.push_sorted(task, lessAbsoluteDeadlineMS);
  }

  // If there are other tasks waiting for being activated
  // reset the timer.
  // Should be rare, but it can happen that the next on the line
  // has to wake up now too (and maybe even the one after him)
  while (!_sleepingTasks.empty()) {

    TaskBase* nextToActivate = _sleepingTasks.front();
    if ((TASK_PERIODIC(nextToActivate)->nextActivationInTicks) == TASK_PERIODIC(task)->nextActivationInTicks) {
      // Wake him up
      _sleepingTasks.pop_front();
      nextToActivate->currentState = taskState_t::eReady;

      // Calculate the new deadline
      // TODO: check for round up!
      periodInTicks = porting::timer::MSToTicks(TASK_PERIODIC(nextToActivate)->periodMS);
      TASK_PERIODIC(nextToActivate)->deadlineInTicks += periodInTicks;

      // Check for preemption. Not need to check if idleTask is running at this point.
      // TODO: check for round up!
      if ((int16_t)(TASK_PERIODIC(nextToActivate)->deadlineInTicks - TASK_PERIODIC(gHighestPriorityReadyTask)->deadlineInTicks) < 0) {

        // If we are pre-empting a task who just woke up here, or another
        // (nested) interrupt handler, we have to put it in the ready queue.
        // Not need to check _idleTask at this point
        if (gHighestPriorityReadyTask != gCurrentRunningTask) {
          _readyTasks.push_sorted(gHighestPriorityReadyTask, lessAbsoluteDeadlineMS);
        } else {
          // The task who just woke up, might not have pre-empted the current
          // running task, but we have and thus we have to push
          // gCurrentRunningTask to sleep
          _readyTasks.push_sorted(gCurrentRunningTask, lessAbsoluteDeadlineMS);
        }

        gHighestPriorityReadyTask = nextToActivate;

      } else {
        _readyTasks.push_sorted(nextToActivate, lessAbsoluteDeadlineMS);
      }
    } else {
      // DEBUG
      //if ((int16_t)(TASK_PERIODIC(nextToActivate)->nextActivationInTicks - numberOfTicks) < 0)
      //  ASSERT_BREAK;

      uint16_t ticksMissing = (uint16_t)(TASK_PERIODIC(nextToActivate)->nextActivationInTicks - numberOfTicks);
      uint32_t fireInMS = porting::timer::ticksToMS(ticksMissing);
      porting::systick::startAndFireIn(fireInMS);
      IF_DEBUG_ON(track_timer(fireInMS));
      break;
    }
  }
}


template<class container_t>
void
SchedulerImp<container_t>::reschedule(TaskBase* task)
{
  // This is called by objects that previously blocked
  // the given task and now want to schedule it back
  // (mutex, semaphores, delayms etc)

  // Interrups must be disabled by the caller

  // Check for preemption
  if (gCurrentRunningTask == _idleTask) {

    // When preempting, always check if there was already another pre-emption
    // before us, in which case we must put the task back in the ready queue.
    // For example an mutex.unlock just happed right before us
    if (gHighestPriorityReadyTask != _idleTask)
      _readyTasks.push_sorted(gHighestPriorityReadyTask, lessAbsoluteDeadlineMS);

    gHighestPriorityReadyTask = task;

  } else if ((int16_t)(TASK_PERIODIC(task)->deadlineInTicks - TASK_PERIODIC(gHighestPriorityReadyTask)->deadlineInTicks) < 0) {

    // When preempting, always check if there was already another pre-emption
    // before us, in which case we must put the task back in the ready queue
    if (gHighestPriorityReadyTask != gCurrentRunningTask
        && gHighestPriorityReadyTask != _idleTask)
      _readyTasks.push_sorted(gHighestPriorityReadyTask, lessAbsoluteDeadlineMS);

    gHighestPriorityReadyTask = task;
    _readyTasks.push_sorted(gCurrentRunningTask, lessAbsoluteDeadlineMS);

  } else {
    _readyTasks.push_sorted(task, lessAbsoluteDeadlineMS);
  }

  // Pre-emption caused and we are not running from within an interrupt
  if (gHighestPriorityReadyTask == task
      && !IRQNestingLevelTracker::withinIRQ()) {
      // The following will re-enable interrupts
      // and won't come back
      System::requestContextSwitch();
  }
}


static uint8_t gCounter;
template<class container_t>
void
SchedulerImp<container_t>::scheduleNext()
{
  // The caller must always call this with interrupts disabled!

  if (!_readyTasks.empty()) {
    // Set the new highest ready task
    gHighestPriorityReadyTask = _readyTasks.front();
    _readyTasks.pop_front();
  } else {
    // If no other task is ready, schedule IdleTask
    gHighestPriorityReadyTask = _idleTask;
    gCounter++;
  }

  // The following will re-enable interrupts
  System::requestContextSwitch();
}


template<class container_t>
void
SchedulerImp<container_t>::endPeriodicCycle()
{
  // This function is called indirectly by a periodic task
  // at the end of its perioc cycle in order to be
  // put into sleep. There is not need to schedule another task.
  // The caller (usually System) will do that by calling scheduleNext()
  uint16_t numberOfTicks = porting::timer::numberOfTicks();

  ATOMIC_SECTION_BEGIN;
  // Deadline miss?
  // ICTOH: if (e1 - e2) < 0 ===> t(e1) < t(e2)
  if ((int16_t)(TASK_PERIODIC(gCurrentRunningTask)->deadlineInTicks - numberOfTicks) < 0) {
    // TODO: signal deadline miss
    ASSERT_BREAK;

    TASK_PERIODIC(gCurrentRunningTask)->deadlineInTicks += TASK_PERIODIC(gCurrentRunningTask)->periodMS;

    // Reschedule it
    _readyTasks.push_sorted(gCurrentRunningTask, lessAbsoluteDeadlineMS);
  } else {

    // Put the caller into sleep
    // Set the activation time first
    TASK_PERIODIC(gCurrentRunningTask)->nextActivationInTicks
      = TASK_PERIODIC(gCurrentRunningTask)->deadlineInTicks;

    if (_sleepingTasks.empty()) {

      // No task is waiting...

      // DEBUG
      if ((int16_t)(TASK_PERIODIC(gCurrentRunningTask)->nextActivationInTicks - numberOfTicks) < 0)
        ASSERT_BREAK;

      // Always put the task in the queue before enabling the timer
      gCurrentRunningTask->currentState = taskState_t::eSleeping;
      _sleepingTasks.push_back(gCurrentRunningTask);

      // Calculate the delta for setting the timer
      uint32_t ticksMissing = (uint16_t)(TASK_PERIODIC(gCurrentRunningTask)->nextActivationInTicks - numberOfTicks);
      uint32_t fireInMS = porting::timer::ticksToMS(ticksMissing);
      porting::systick::startAndFireIn(fireInMS);
      IF_DEBUG_ON(track_timer(fireInMS));

    } else {

      // Timer is running and at least one task is waiting

      // If the timer fires much later than our delta
      // we need to reprogram the counter
      uint32_t ticksMissing = (uint16_t)(TASK_PERIODIC(gCurrentRunningTask)->nextActivationInTicks - numberOfTicks);
      uint32_t fireInMS = porting::timer::ticksToMS(ticksMissing);
      uint32_t timeLeftMS = porting::systick::currentValueMS();

      // TODO
      // The problem here is that if the timer is still ON.
      // If it fires before we have safely placed the task
      // into the sleeping queue, the task will stay there until
      // next fire, which is not what we want
      //if (timeLeftMS == 0)
      //  ASSERT_BREAK;

      if (fireInMS < timeLeftMS) {

        porting::systick::stop();

        // Always put the task in the queue before enabling the timer
        gCurrentRunningTask->currentState = taskState_t::eSleeping;
        _sleepingTasks.push_front(gCurrentRunningTask);

        porting::systick::startAndFireIn(fireInMS);
        IF_DEBUG_ON(track_timer(fireInMS));

      } else {

        // Push sorted on sleepingTask is based on nextActivationInTicks
        gCurrentRunningTask->currentState = taskState_t::eSleeping;
        _sleepingTasks.push_sorted(gCurrentRunningTask, lessNextActivationMS);
      }
    }
  }
  // The following will re-enable interrupts
  Scheduler::scheduleNext();

  IF_DEBUG_ON(track_deadline(TASK_PERIODIC(gCurrentRunningTask)->deadlineInTicks));
}


template<class container_t>
void
SchedulerImp<container_t>::endAperiodicCycle()
{
  // TODO
}


template<class container_t>
void
SchedulerImp<container_t>::lock()
{
  // TODO
}


template<class container_t>
void
SchedulerImp<container_t>::unlock()
{
  // TODO
}


TaskBase* gCurrentRunningTask;
TaskBase* gHighestPriorityReadyTask;

// Used by push_sorted on readyQueue
bool lessAbsoluteDeadlineMS(TaskBase* left, TaskBase* right)
{
  if ((int16_t)(TASK_PERIODIC(left)->deadlineInTicks - TASK_PERIODIC(right)->deadlineInTicks) < 0)
    return true;
  return false;
}


// Used by push_sorted on sleepingQueue
bool lessNextActivationMS(TaskBase* left, TaskBase* right)
{

  if ((int16_t)(TASK_PERIODIC(left)->nextActivationInTicks - TASK_PERIODIC(right)->nextActivationInTicks) < 0)
    return true;
  return false;
}


SchedulerImp<ItemList<TaskBase*>> gScheduler;

#endif
