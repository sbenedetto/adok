//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <stdint.h>

struct TaskBase;

///
/// \brief The following class defines the interface of the scheduler
///
/// This is interface is supposed to be used by the System object and by any
/// object that implements synchronization mechanism
///
/// \ingroup core
class Scheduler
{
public:

/// \brief Initialize the scheduler.
///
/// This is generally called by System::start() and it is intended of
/// internal use
static  void      init();

///
/// \brief Add the given task to the ready queue.
///
/// It's usually called when the task is added to the system with
/// System::register(). Extra parameter is only used by the idleTask,
/// whose presence is a constraint for every scheduler class implementation
static  void      addTask(TaskBase* task);

/// \brief Remove the given task from the ready queue.
///
/// Currently not used anywhere. Might be removed in the future
static  void      removeTask(TaskBase* task);

///
/// \brief Call the actual scheduler algorithm
///
/// This is usually called by the scheduler timer. It sets
/// gHighestPriorityReadyTask if necessary, thus causing a context switch,
/// that will take place at the exit of outer most currently running
/// interrupt handler
static  void      reschedule();

///
/// \brief Add the given task back to the ready queue
///
/// A preemption will take place if the given task has a higher priority of
/// the current running task. This is usually called by synchronization
/// routines that want to reschedule a previously blocked task.
static  void      reschedule(TaskBase* task);

///
/// \brief Schedule the next highest priority ready task, if any.
///
/// This is usually called by synchronization routines that blocks the current
/// running task. The next highest priority ready task will run, if any.
/// Otherwise the idleTask will be executed.
static  void      scheduleNext();

static  void      start();

static  void      endPeriodicCycle();
static  void      endAperiodicCycle();

static  void      lock();
static  void      unlock();

private:
            Scheduler();
};


///
/// \brief The following pointer holds the object currently running
///
extern TaskBase* gCurrentRunningTask;

///
/// \brief The following pointer holds the task ready with the highest prioirty
///
extern TaskBase* gHighestPriorityReadyTask;

#endif
