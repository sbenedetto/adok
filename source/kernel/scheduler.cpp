//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "scheduler.h"

#include "scheduler_imp.h"


void
Scheduler::init()
{
  gScheduler.init();
}


void
Scheduler::addTask(TaskBase* task)
{
  gScheduler.addTask(task);
}


void
Scheduler::removeTask(TaskBase* task)
{
  gScheduler.removeTask(task);
}


void
Scheduler::reschedule()
{
  gScheduler.reschedule();
}


void
Scheduler::reschedule(TaskBase* task)
{
  // TODO: dispatch task based on the type
  // if we have more than one scheduler
  gScheduler.reschedule(task);
}


void
Scheduler::scheduleNext()
{
  // TODO: convert the return parameter of the following to bool.
  // If return false, it has not task to schedule and another scheduler
  // can be called
  // NOTE: If so, how does the FSM with idle protocol fit in? A lower
  // priority task (soft) can delay the FSM transaction if the idle task
  // is not ran!
  gScheduler.scheduleNext();
}


void
Scheduler::start()
{
  gScheduler.start();
}


void
Scheduler::endPeriodicCycle()
{
  gScheduler.endPeriodicCycle();
}


void
Scheduler::endAperiodicCycle()
{
  gScheduler.endAperiodicCycle();
}


void
Scheduler::lock()
{
  gScheduler.lock();
}


void
Scheduler::unlock()
{
  gScheduler.unlock();
}
