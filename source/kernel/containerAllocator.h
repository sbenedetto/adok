#ifndef __CONTAINER_ALLOCATOR_H__
#define __CONTAINER_ALLOCATOR_H__

#include <stddef.h>  // size_t

#include "memoryBlockAllocator.h"

template <typename T>
class ContainerAllocator
{
public:

    // Allocator traits
    typedef size_t              size_type;
    typedef ptrdiff_t           difference_type;
    typedef T*                  pointer;
    typedef const T*            const_pointer;
    typedef T&                  reference;
    typedef const T&            const_reference;
    typedef T                   value_type;

    /// Default constructor does nothing.
    inline ContainerAllocator(void)                                  { ; }

    /// Copy constructor does nothing.
    inline ContainerAllocator(const ContainerAllocator &)            { ; }

    /// Type converting allocator constructor does nothing.
    template <typename Type1>
    inline ContainerAllocator(const ContainerAllocator<Type1>&)      { ; }

    /// Destructor does nothing.
    inline ~ContainerAllocator()                                     { ; }

    /// Convert an allocator<Type> to an allocator <Type1>.
    template <typename Type1>
    struct rebind
    {
      typedef ContainerAllocator<Type1> other;
    };

    /// Return address of reference to mutable element.
    pointer address(reference elem) const                     { return &elem; }

    /// Return address of reference to const element.
    const_pointer address(const_reference elem) const         { return &elem; }

    pointer allocate(size_type count, const void * hint = 0)
    {
      (void)hint;  // Ignore the hint.
      void * p = MemoryBlockAllocator::allocate(count * sizeof(T));
      return reinterpret_cast<pointer>(p);
    }

    /// Ask allocator to release memory at pointer with size bytes.
    void deallocate(pointer p, size_type size)
    {
      MemoryBlockAllocator::deallocate(p, size * sizeof(T));
    }

    /// Calculate max # of elements allocator can handle.
    size_type max_size(void) const throw()
    {
      const size_type max_bytes = size_type(-1);
      const size_type bytes = max_bytes / sizeof(T);
      return bytes;
    }

    /// Construct an element at the pointer.
    void construct( pointer p, const T& value )
    {
      // A call to global placement new forces a call to copy constructor.
      ::new(p) T(value);
    }

    /// Destruct the object at pointer.
    void destroy(pointer p)
    {
      // If the Type has no destructor, then some compilers complain about
      // an unreferenced parameter, so use the void cast trick to prevent
      // spurious warnings.
      (void)p;
      p->~T();
    }
};

#endif
