//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __IRQ_NESTING_LEVEL_TRACKER_H__
#define __IRQ_NESTING_LEVEL_TRACKER_H__


///
/// \brief This class is used to keep track of interrupts level
///
/// Every interrupt handler must declare an object of this class
/// on its stack before everything else. When the outer interrupt
/// handler will exit, the IRQNestingLevelTracker object will request
/// a context switch if necessary.
///
class IRQNestingLevelTracker
{
public:

  IRQNestingLevelTracker();
  ~IRQNestingLevelTracker();

  ///
  /// \brief Let the caller know if it's running from an interrupt handler or not
  static bool withinIRQ();
};

#endif
