/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utility/delay.h"

#include <stdint.h>

static volatile uint32_t gCounter;
static volatile uint64_t gTimeElapsed;

void sys_tick_timer()
{
  if (gCounter)
    gCounter--;

  gTimeElapsed++;
}


namespace util {

void
delayms(uint32_t millisecond)
{
  NVIC::currentCounterValue()
}


void
delayus(uint32_t microsecond)
{
  gCounter = microsecond;
  while (gCounter > 0);
}

uint64_t
currentCounterValue()
{
  return gTimeElapsed;
}

};
