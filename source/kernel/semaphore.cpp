//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "semaphore.h"

#include "barriers.h"
#include "scheduler.h"
#include "system.h"
#include "task_base.h"


Semaphore::Semaphore(uint8_t initialValue)
{
  _counter = initialValue;
}


Semaphore::~Semaphore()
{
  // TODO
}


void
Semaphore::acquire()
{
  // Disable interrupts
  ATOMIC_SECTION_BEGIN;
  if (_counter == 0) {
    // Block the caller
    gCurrentRunningTask->currentState = taskState_t::eBlocked;
    _waitingTasks.push_back(gCurrentRunningTask);
    // The following will re-enable interrupts and won't return
    // until we have the semaphore
    Scheduler::scheduleNext();
    // We woke up and acquired the semaphore. Interrupts enabled.

  } else {
    _counter--;
    // Re-enable interrupts
    ATOMIC_SECTION_END;
  }
}


// TODO: currently semaphore are used by ISR to "signal"
//       waiting task in I/O, thus interrupts are disabled
//       instead of using a simple mutex on both the waitingTasks
//       and the counter. Provides signal mechanism.


void
Semaphore::release()
{
  // Disable interrupts
  ATOMIC_SECTION_BEGIN;
  if (!_waitingTasks.empty()) {
    // Wake up waiting task
    TaskBase* task = _waitingTasks.front();
    _waitingTasks.pop_front();
    task->currentState = taskState_t::eReady;
    // If the task that we just woke up has higher
    // priority the following won't come back
    Scheduler::reschedule(task);

  } else {
    _counter++;
  }

  ATOMIC_SECTION_END;
}


bool
Semaphore::tryAndAcquire()
{
  bool status = true;

  // Disable interrupts
  ATOMIC_SECTION_BEGIN;

  if (_counter == 0)
    status = false;
  else
    _counter--;

  ATOMIC_SECTION_END;

  return status;
}
