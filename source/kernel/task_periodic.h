//
// This file is part of the adok project.
//
// Copyright (C) 2013 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __TASK_PERIODIC_H__
#define __TASK_PERIODIC_H__

///
/// \brief Specific data needed by periodic tasks
/// \ingroup task
///
/// Periodic task are currently scheduler with the EDF scheduler
///
/// \todo provide a typedef for variable that keep track
/// of time for implementing the ICTOH mechanism.
/// For now, make sure all are uint16 like the timer.
struct TaskPeriodic {
  uint16_t    periodMS;
  uint16_t    startAfterMS;       /// Offset
  uint16_t    nextActivationInTicks;
  uint16_t    deadlineInTicks;

  TaskPeriodic(uint16_t _periodMS = 0, uint16_t _startAfterMS = 0)
    : periodMS(_periodMS),
      startAfterMS(_startAfterMS),
      nextActivationInTicks(0),
      deadlineInTicks(0)
  {
  }

  // Avoid the compiler to link with libc for memcpy
  TaskPeriodic(const TaskPeriodic& lhs)
  {
    periodMS              = lhs.periodMS;
    startAfterMS          = lhs.startAfterMS;
    nextActivationInTicks = lhs.nextActivationInTicks;
    deadlineInTicks       = lhs.deadlineInTicks;
  }
};

#define TASK_PERIODIC(x) ((TaskPeriodic *)(x + 1))


#endif // __TASK_PERIODIC_H__
