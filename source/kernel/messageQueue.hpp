//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "barriers.h"
#include "scheduler.h"
#include "system.h"
#include "task.h"


template<class Message_t, const size_t kNumberOfMessage>
MessageQueue<Message_t, kNumberOfMessage>::MessageQueue()
{
  for (size_t i = 0; i < kNumberOfMessage; i++)
    _isBusy[i] = false;
}


template<class Message_t, const size_t kNumberOfMessage>
Message_t*
MessageQueue<Message_t, kNumberOfMessage>::create()
{
  // TODO: provide an AutoLocker<>
  Message_t* message = NULL;

  _queueMutex.lock();

  for (size_t i = 0; i < kNumberOfMessage; i++) {
    if (_isBusy[i] == false) {
      message = &_messagePool[i];
      _isBusy[i] = true;
      break;
    }
  }

  _queueMutex.unlock();
  return message;
}


template<class Message_t, const size_t kNumberOfMessage>
void
MessageQueue<Message_t, kNumberOfMessage>::destroy(Message_t* message)
{
  _queueMutex.lock();

  for (size_t i = 0; i < kNumberOfMessage; i++) {
    if (&_messagePool[i] == message) {
      _isBusy[i] = false;
      break;
    }
  }

  _queueMutex.unlock();
}


template<class Message_t, const size_t kNumberOfMessage>
Message_t*
MessageQueue<Message_t, kNumberOfMessage>::get()
{
  Message_t* message = NULL;

  _queueMutex.lock();

  // Use IF since this is a many-to-one message queue
  if (_queue.empty()) {

    ASSERT_BREAK;
    // Disable interrupts
    ATOMIC_SECTION_BEGIN;

    // Block caller
    gCurrentRunningTask->currentState = taskState_t::eBlocked;
    _waitingTasks.push_back(gCurrentRunningTask);
    // TODO: Interrupts should be re-enabled here! The scheduler
    //       and every other locking mechanism should always check
    //       the current state!

    // Unlock the queue before going to sleep
    _queueMutex.unlock();
    // The following will re-enable interrupts and won't
    // come back
    Scheduler::scheduleNext();

    // Woke up and get a message
    // Interrupts are enabled.
    _queueMutex.lock();
  }

  // There is a message and the queue is locked
  message = _queue.front();
  _queue.pop_front();
  // We got a message, unlock the queue
  _queueMutex.unlock();

  // Are there task waiting in put() ?
  // NOTE: being a many-to-one message queue, one waiting queue
  //       list is enough, as there cannot be tasks waiting for
  //       a message to be put (queue empty) and tasks waiting
  //       for a message to be get (queue full) at the same time
  _waitingTasksMutex.lock();
  if (!_waitingTasks.empty()) {

    // Wake up waiting task
    TaskBase* task = _waitingTasks.front();
    _waitingTasks.pop_front();
    task->currentState = taskState_t::eReady;
    // Unlock as we might be pre-empted
    _waitingTasksMutex.unlock();
    // The following might not come back if a pre-emption
    // takes place.
    Scheduler::reschedule(task);

  } else {
    _waitingTasksMutex.unlock();
  }

  return message;
}


template<class Message_t, const size_t kNumberOfMessage>
void
MessageQueue<Message_t, kNumberOfMessage>::put(Message_t* message)
{
  _queueMutex.lock();

  // Is Queue full?
  if (_queue.items() == kNumberOfMessage) {

    // Disable interrupts
    ATOMIC_SECTION_BEGIN;
    // Block caller
    gCurrentRunningTask->currentState = taskState_t::eBlocked;
    _waitingTasks.push_back(gCurrentRunningTask);
    // Unlock the queue before going to sleep
    _queueMutex.unlock();
    // The following will re-enable interrupts and won't come back
    Scheduler::scheduleNext();

    // Woke up and put the message. Interrupts are enabled
    _queueMutex.lock();
  }

  _queue.push_back(message);
  _queueMutex.unlock();

  // Are there task waiting in get()
  // NOTE: being a many-to-one message queue, one waiting queue
  //       list is enough, as there cannot be tasks waiting for
  //       a message to be put (queue empty) and tasks waiting
  //       for a message to be get (queue full) at the same time
  _waitingTasksMutex.lock();
  if (!_waitingTasks.empty()) {


    // Wake up waiting task
    TaskBase* task = _waitingTasks.front();
    _waitingTasks.pop_front();
    task->currentState = taskState_t::eReady;
    // Unlock as we might be pre-empted
    _waitingTasksMutex.unlock();
    // The following might cause pre-emption
    Scheduler::reschedule(task);

  } else {
    _waitingTasksMutex.unlock();
  }
}
