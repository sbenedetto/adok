//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __IRQ_MANAGER_H__
#define __IRQ_MANAGER_H__

#include <stdint.h>

namespace IRQManager
{
  // NOTE: interrupt numbers are architecture specific, thus the actual data value
  //       is provided by the driver library as a global object in the same namespace
  enum eIRQNumber_t : uint8_t;

  typedef void (*handler_t)(void *);

  void    callHandler(eIRQNumber_t ISRNumber);
  void    registerHandler(eIRQNumber_t ISRNumber, handler_t handler, void* data);

  void    relocateInterruptVector(uint32_t from, uint32_t to, uint16_t size);
};

#endif
