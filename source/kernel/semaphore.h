//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//
#ifndef __SEMAPHORE_H__
#define __SEMAPHORE_H__

#include <stdint.h>

#include "itemlist.h"

struct TaskBase;

///
/// \brief Semaphore class for Task synchronization.
///
/// The followig class implements the basic functionality of
/// a semaphore.
///
// TODO: policy pattern could be applied
class Semaphore
{
public:
  //
  // \brief Constructor
  //
  // \param initialValue initial value of the semphore. That it, the number
  //        of consecutive acquire() calls (no release() call in between), before
  //        a Task would block on it
  ///
  Semaphore(uint8_t initialValue);
  ~Semaphore();

  //
  // \brief acquire aquire the semaphore. Blocking.
  //
  // Task will be suspended if the semaphore is already acquired, until a release call
  // is made
  ///
  void    acquire();

  //
  // \brief release release the semaphore.
  //
  // If any Task is suspended on this semaphore it will be resumed
  ///
  void    release();

  //
  // \brief tryAndAcquire try to acquire the semaphore. Non-blocking.
  //
  // Task will try to acquire the semamphore. If it is already acquired, false is returned
  // and the Task won't be suspended, otherwise true is returned and semaphore is acquired
  ///
  bool    tryAndAcquire();

private:
  ItemList<TaskBase*>    _waitingTasks;
  uint8_t                _counter;
};

#endif
