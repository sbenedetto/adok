#ifndef __DEBUG_H__
#define __DEBUG_H__

#define DEBUG_ON
#ifdef DEBUG_ON
  #define IF_DEBUG_ON(x) x
#else
  #define IF_DEBUG_ON(x)
#endif

#include <stdint.h>

#include <porting/cpu_defs.h>

namespace adok
{
  void fillUpStack(porting::types::stack_t stackTop, uint16_t stackSize,
                   uint32_t pattern);
}

#endif // __DEBUG_H__
