
#include "memoryBlockAllocator.h"

#include <stdint.h>

#include "adokInternalDefs.h"


// TODO
// The following is supposed to hold the necessary memory
// for a list node container. It needs to be parametized,
// based on the internal structure used (list, list_forward, tree etc..)
struct MemoryBlock
{
  uint8_t       buffer[12];
  bool          busy;
};


// Being in the BSS it is zero-out by the standard
MemoryBlock gMemoryPool[gkNumberOfTasks];


void*
MemoryBlockAllocator::allocate(size_t bytes)
{
  // We have to check if the buffer is big enough (T > bytes)
  void *address = NULL;
  for (uint8_t i = 0; i < gkNumberOfTasks; i++) {

    if (!gMemoryPool[i].busy) {

      gMemoryPool[i].busy = true;
      address = reinterpret_cast<void *>(gMemoryPool[i].buffer);
      break;
    }
  }
  return address;
}


void
MemoryBlockAllocator::deallocate(void* buffer, size_t bytes)
{
  for (uint8_t i = 0; i < gkNumberOfTasks; i++) {

    if (reinterpret_cast<void *>(gMemoryPool[i].buffer) == buffer) {

        gMemoryPool[i].busy = false;
        break;
    }
  }
}
