// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//
#ifndef __MUTEX_H__
#define __MUTEX_H__

#include "itemlist.h"

#include <stdint.h>

struct TaskBase;

///
/// \brief Mutex class for mutual exclusion access.
///
/// The followig class implements the basic functionality of
/// a mutex
///
// TODO: policy pattern could be applied
class Mutex
{
public:

  enum class eState_t : uint8_t;

  ///
  /// \brief Create an unlocked mutex
  ///
  Mutex();

  ///
  /// \brief lock lock the mutex. Blocking.
  ///
  /// Task will be suspended if the mutex is already locked, until a unlock call
  /// is made
  ////
  void    lock();

  ///
  /// \brief unlock unlock the mutex
  ///
  /// If any Task is suspended on this mutex it will be resumed
  ///
  void    unlock();

  ///
  /// \brief tryAndLock try to lock the mutex. Non-blocking.
  ///
  /// Task will try to lock the mutex. If it is already locked, false is returned
  /// and the Task won't be suspended, otherwise true is returned and mutex is locked
  ///
  bool    tryAndLock();

private:
  ItemList<TaskBase*>    _waitingTasks;
  // TODO: use C++11 atomic_flag
  eState_t     _locker;
};

#endif
