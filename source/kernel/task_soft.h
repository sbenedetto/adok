//
// This file is part of the adok project.
//
// Copyright (C) 2013 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __TASK_SOFT_H__
#define __TASK_SOFT_H__

///
/// \brief Extra data needed by soft tasks
/// \ingroup task
///
/// TaskSoft are currently scheduled with the RoundRobin scheduler
struct TaskSoft {
  uint32_t      nextPreemptionMS;
  uint8_t       priority;

  TaskSoft(uint32_t _nextPreemptionMS = 0, uint8_t _priority = 0)
    : nextPreemptionMS(_nextPreemptionMS),
      priority(_priority)
  {
  }

  // Avoid the compiler to link with libc for memcpy
  TaskSoft(const TaskSoft& lhs)
  {
    nextPreemptionMS = lhs.nextPreemptionMS;
    priority = lhs.priority;
  }
};

#endif // __TASK_SOFT_H__
