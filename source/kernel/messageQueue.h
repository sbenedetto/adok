//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __MESSAGE_QUEUE_H__
#define __MESSAGE_QUEUE_H__

#include <stdint.h>

// TODO: rename and move to target=lib
#include <itemlist.h>

#include "mutex.h"

struct TaskBase;

///
/// \brief This object represent the message exchanged through the MessageQueue
///
struct Message
{
  // TODO: let's start with something simple
  uint8_t   data[32];
};

///
/// \brief This class provides a message queue object
///
/// The object implements a one-to-one policy. Let's start simple.
///
/// TODO: Let's watch out for code duplication when other
///       type of messages are added (a.k.a. template code-bloat)
template<
          class Message_t = Message,
          const size_t kNumberOfMessage = 10 // This could be a constant constructor parameter
        >
class MessageQueue
{
public:

  MessageQueue();

  // TODO: move this to it's own class
  /// \brief Return an empty message that can be used for later use
  Message_t*    create();
  /// \brief Allow to give back a message previously obtained with alloc()
  void          destroy(Message_t* message);

  /// \brief Get the first message in the queue. Blocking.
  ///
  /// If there are no message in the queue the caller will be suspended until
  /// a message arrives
  Message_t*    get();

  /// \brief Put a message in the queue. Blocking.
  ///
  /// If the messageQueue is full the caller will be suspended until there
  /// is enough space to put the given message in the queue
  void          put(Message_t* message);

private:

  // TODO: the following are stricly coupled. Enforce the coupling,
  //       but avoid a struct { message_t, bool } [kNumberOfMessage]
  //       because it won't be aligned
  Message_t     _messagePool[kNumberOfMessage];
  bool          _isBusy[kNumberOfMessage];

  Mutex                 _queueMutex;
  ItemList<Message_t*>  _queue;

  Mutex                 _waitingTasksMutex;
  ItemList<TaskBase*>   _waitingTasks;
};

// include implementation
#include <messageQueue.hpp>

#endif
