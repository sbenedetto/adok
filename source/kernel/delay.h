/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

namespace util {

  /**
   * \brief Block the caller for the given millisecond
   *
   * \param millisecond number of millisecond to wait before returning
   */
  void  delayms(uint32_t millisecond);

  /**
   * \brief Block the caller for the given microsecond
   *
   * \param microsecond number of microsecond to wait before returning
   */
  void  delayus(uint32_t microsecond);

  /**
   * \brief Return the current counter value in microsecond
   *
   * \return The current counter value in microsecond. Useful for calculating
   *         the time elapsed since the previous call. Watch out for wrap around.
   */
  uint64_t  currentCounterValue();
};
