//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __SCHEDULER_IMP_H__
#define __SCHEDULER_IMP_H__

#include <atomic>

struct TaskBase;

//
//! The following class defines the interface of the scheduler
//  implementation
//
template<class container_t>
class SchedulerImp
{
public:
            SchedulerImp();
            ~SchedulerImp();

  void      init();

  void      addTask(TaskBase* task);
  void      removeTask(TaskBase* task);

  void      reschedule();
  void      reschedule(TaskBase* task);

  void      scheduleNext();
  void      start();

  void      endPeriodicCycle();
  void      endAperiodicCycle();

  void      lock();
  void      unlock();

private:
  // The following are global for all scheduler implementation
  TaskBase*             _idleTask;
  std::atomic_flag      _lock;
  container_t           _readyTasks;

  // TODO: the following is specific to EDF implementation
  container_t           _sleepingTasks;
};


#ifdef USE_ROUND_ROBIN_SCHEDULER
  #include "scheduler_roundrobin.hpp"
#elif USE_EDF_SCHEDULER
  #include "scheduler_edf.hpp"
#else
  #error "No scheduler choosen"
#endif

#endif // __SCHEDULER_IMP_H__
