//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include "application.h"

#include <stdint.h>

struct TaskBase;
struct Event;

/// \defgroup core

///
/// \class System
///
/// \brief The following class represent the System concept
///
/// \ingroup core
///
class System
{
public:

  ///
  /// \brief Return unique Task ID to be used by the caller
  ///
static uint8_t  getUniqueTaskID();

static void     init();

static Application::eState_t  currentState();
static void                   setState(Application::eState_t state);

static void     registerForEvent(Event* event);

static void     registerTask(TaskBase* task);

static void     requestContextSwitch();

static void     start();

  ///
  /// \brief Suspend current running task for next event it registered for
  ///
static void     waitForNextEvent();

  ///
  /// \brief Suspend caller task until next its next cycle
  ///
static void     waitForNextRun();

static void     waitForState();
static void     wakeTasksForCurrentState();
};

#endif
