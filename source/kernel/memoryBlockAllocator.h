#ifndef __CONTAINER_MEMORY_ALLOCATOR_H__
#define __CONTAINER_MEMORY_ALLOCATOR_H__

#include <cstddef>

class MemoryBlockAllocator
{
public:
  static void* allocate(size_t bytes);
  static void  deallocate(void* buffer, size_t bytes);
};

#endif
