//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __SYSTICK_H__
#define __SYSTICK_H__

#include <stdint.h>

namespace porting {

namespace systick {

///
/// \brief start system timer and make it fire in fireInMS millisecond
///
/// The interrupt handler must call Scheduler::reschedule()
void startAndFireIn(uint32_t fireInMS);

///
/// \brief Stop system timer
///
void stop();

///
/// \brief return the current value of the system timer in millisecond
///
uint16_t currentValueMS();

///
/// \brief return the current value of the system timer in millisecond
///
uint32_t currentValueUS();

} // namespace systick

} // namespace porting

#endif  // __SYSTICK_H__
