//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __ARCH_H__
#define __ARCH_H__

#include <stdint.h>

#include "cpu_defs.h"

namespace porting {

namespace arch {

///
/// \brief Context switcher
///
/// \todo document
void context_switcher();

///
/// \brief Do whatever its necessary to prevent race condition
///
/// \todo complete documenting
void critical_section_begin();

///
/// \brief Undo whatever critical_section_begin did
///
/// \todo complete documenting
void critical_section_end();

///
/// \brief Initialize a given stack
///
/// \todo complete documenting
void initialize_stack(porting::types::stack_t& stackTop, uint16_t stackSize,
                      porting::types::entryPoint_t entryPoint);

///
/// \brief Request context switch
///
/// \todo complete documenting
void request_context_switch(bool fromISR);

///
/// \brief Routine to associate with reset vector entry
///
/// \todo document
void start();

///
/// \brief start scheduler
///
/// \todo complete documenting
void start_scheduler();

} // namespace arch

} // namespace porting

#endif // __ARCH_H__a
