//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "porting.h"

#include <kernel/IRQNestingLevelTracker.h>
#include <kernel/scheduler.h>
#include <kernel/system.h>


void
hard_fault_handler()
{

  asm volatile
  (
    "tst    lr, #4\n\t"
    "ite    eq\n\t"
    "mrseq  r0, msp\n\t"
    "mrsne  r0, psp\n\t"
    "cmp    r0, sp\n\t"         // TODO: FIX THIS
    "it     eq\n\t"
    "addeq  r0, 8\n\t"          // NOTE: This is still a compiled function and
    "ldr    r1, [r0, 0x24]\n\t" // PC
    "ldr    r2, [r0, 0x22]\n\t" // LR
    "bkpt   #0\n\t"
  );
}



// Trampoline for C++ mangled method
extern "C" void quick_and_dirty_setpendSV();
extern "C" void pendable_service_handler_C();
void pendable_service_handler();
void pendable_service_handler_C()
{
  pendable_service_handler();
}

void quick_and_dirty_setpendSV()
{
  volatile uint32_t* interruptControlState = (uint32_t *)0xE000ED04;
  *interruptControlState |= (0x01 << 28);
}


void
swi_handler()
{
  // Retrieve SVC parameter value
  asm volatile
  (
    "cpsid  i\n\t"
    "mov    r0, lr\n\t"       // Are we called from a user application?
    "ands   r0, #4\n\t"       //
    "ite    eq\n\t"           // If-Then-Else
    "mrseq  r0, msp\n\t"      // Load user stack pointer
    "mrsne  r0, psp\n\t"      // Load system stack poitner
    "cmp    r0, sp\n\t"       // TODO: FIX THIS
    "it     eq\n\t"
    "addeq  r0, 8\n\t"        // NOTE: This is still a compiled function and
                              //       thus the compiler will push two more words
                              //       onto the system stack. TODO: fix this
    "add    r0, 24\n\t"        // PC is always at SP + 0x18 (6 words)
    "ldr    r0, [r0, #0]\n\t"  // Load PC into R0
    "ldrb   r0, [r0, #-2]\n\t" // Load the actual instruction into R0

    "cmp    r0, #1\n\t"
    "it     eq\n\t"
    "beq    first_context_switch\n\t"

    "cmp    r0, #2\n\t"
    "it     eq\n\t"
    "bleq   quick_and_dirty_setpendSV\n\t"
    "pop    {r7, lr}\n\t"
    "b      out\n\t"

    // Load the first task
    "first_context_switch:\n\t"
    // Load process stack pointer into R0
    "ldr    r0, =gCurrentRunningTask\n\t"
    "ldr    r0, [r0]\n\t"
    "ldr    r0, [r0]\n\t"
    // Load half of its context
    "ldmia  r0!, {r4-r11}\n\t"
    // Set up PSP in order to let the hardware load the other
    // half of the context
    "msr    psp, r0\n\t"
    // Set the EXC_RETURN[2] bit so that on return the PSP stack
    // will be used
    "orr    lr, lr, #4\n\t"
    "out:\n\t"
    // Re-enable interruts
    "cpsie  i\n\t"
    // Start the task
    "bx     lr"
   : /**/ : /**/ : /* clobber list */ "lr");

}


void
pendable_service_handler()
{
  // TODO: make sure the order of this doesn't change (Barrier)
  porting::arch::context_switcher();
  gCurrentRunningTask = gHighestPriorityReadyTask;
}
