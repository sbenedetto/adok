#ifndef __CPU_DEFS_H__
#define __CPU_DEFS_H__

#include <stdint.h>

namespace porting {

namespace types {

  typedef uint32_t stack_t;
  typedef uint32_t entryPoint_t;
}

}

#endif // __CPU_DEFS_H__
