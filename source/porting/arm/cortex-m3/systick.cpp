//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "systick.h"

#include <kernel/IRQNestingLevelTracker.h>
#include <kernel/scheduler.h>

namespace porting {

namespace systick {

// System tick registers
static volatile uint32_t* gkSysTickControlStatus    = (uint32_t *)0xE000E010;
static volatile uint32_t* gkSysTickReloadValue      = (uint32_t *)0xE000E014;
static volatile uint32_t* gkSysTickCurrentValue     = (uint32_t *)0xE000E018;
static volatile uint32_t* gkSysTickCalibrationValue = (uint32_t *)0xE000E01C;


void sys_tick_timer()
{
  IRQNestingLevelTracker _;

  // Call the scheduler
  Scheduler::reschedule();
}


void startAndFireIn(uint32_t reloadValueInMS)
{
  // TODO: The following is specific to STM32. Need to be decoupled
  // From the manual: The SysTick calibration value is set to 9000,
  // which gives a reference time base of 1 ms with the SysTick clock
  // set to 9 MHz (max HCLK/8).
  uint32_t oneMillisecond = *gkSysTickCalibrationValue & 0x00FFFFFF;
  *gkSysTickReloadValue = oneMillisecond * reloadValueInMS;
  // 0x1 = Enable SysTick timer
  // 0x2 = Enable interrupt when 0 is reached
  *gkSysTickControlStatus |= 0x03;
  // TODO: set priority
}


void stop()
{
  *gkSysTickControlStatus &= ~(0x03);
  // Clean counter and reload value, otherwise at the next
  // reenable the first loop will use the old reload value
  *gkSysTickCurrentValue = 0;
  *gkSysTickReloadValue  = 0;
}


uint16_t currentValueMS()
{
  uint32_t value = *gkSysTickCurrentValue & 0x00FFFFFF;
  uint32_t oneMillisecond = *gkSysTickCalibrationValue & 0x00FFFFFF;
  return value / oneMillisecond;
}


uint32_t currentValueUS()
{
  // TODO
  return currentValueMS() * 1000;
}

} // namespace systick
} // namespace porting


