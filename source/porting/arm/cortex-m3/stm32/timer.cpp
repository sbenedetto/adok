//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "timer.h"

static const uint8_t gkOffsetControl1        = 0x00;
static const uint8_t gkOffsetEventGeneration = 0x14;
static const uint8_t gkOffsetCounter         = 0x24;
static const uint8_t gkOffsetPrescaler       = 0x28;
static const uint8_t gkOffsetAutoReload      = 0x2C;


void
timer2_global_handler()
{
  while (1);
}


namespace porting {

namespace timer {

void start()
{
  // Enable clock for Timer2 controller
  volatile uint8_t* clockBaseAddress   = (uint8_t *)0x40021000;
  uint8_t offsetEnableAPB1             = 0x1C;
  uint8_t timer2                       = 0x01;
  *((volatile uint32_t *)(clockBaseAddress + offsetEnableAPB1)) |= timer2;

  // Set the timer2 controller
  // The timer will be used to keep track of the time for calculating
  // the absolute deadline of periodic tasks.
  volatile uint8_t* timer2BaseAddress   = (uint8_t *)0x40000000;
  // Disable one pulse mode
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetControl1)) &= ~(0x01 << 3);
  // Set counting direction up
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetControl1)) &= ~(0x01 << 4);
  // Set auto-reload on
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetControl1)) |= (0x01 << 7);

  // In upcounting mode this is actually the value
  // at which the timer will round up
  uint16_t autoReloadValue = 0xFFFF;
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetAutoReload)) = autoReloadValue;

  // TODO: This is dependent from the SoC
  // The timer (16bit) will be counting at each 0.5 ms
  uint16_t prescalerValue = 72000 >> 1;
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetPrescaler)) = prescalerValue - 1;

  // Generate manually an update event to make the above modification effective
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetEventGeneration)) |= 0x01;

  // Enable the timer
  *((volatile uint16_t *)(timer2BaseAddress + gkOffsetControl1)) |= 0x01;
}


uint16_t numberOfTicks()
{
  volatile uint8_t* timer2BaseAddress   = (uint8_t *)0x40000000;
  uint16_t tmp = *((volatile uint16_t *)(timer2BaseAddress + gkOffsetCounter));
  return tmp;
}


uint16_t ticksToMS(uint16_t ticks)
{
  // Timer is counting at 0.5ms
  return ticks >> 1;
}


uint16_t MSToTicks(uint16_t milliseconds)
{
  return milliseconds << 1;
}


} // namespace timer

} // namespace porting
