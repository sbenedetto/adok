//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
extern uint32_t _system_stack_base;  /* Stack */
#ifdef __cplusplus
}
#endif


extern void start();
extern void nmi_handler();
extern void hard_fault_handler();
extern void memory_management_handler();
extern void bus_fault_handler();
extern void usage_fault_handler();
extern void swi_handler();
extern void debug_monitor_handler();
extern void pendable_service_handler();
namespace porting {
  namespace systick {
    extern void sys_tick_timer();
  }
}
extern void window_watchdog_handler();
extern void pvd_handler();
extern void tamper_handler();
extern void real_time_clock_handler();
extern void flash_global_handler();
extern void rcc_handler();
extern void external_interrupt_line0_handler();
extern void external_interrupt_line1_handler();
extern void external_interrupt_line2_handler();
extern void external_interrupt_line3_handler();
extern void external_interrupt_line4_handler();
extern void dma1_channel1_handler();
extern void dma1_channel2_handler();
extern void dma1_channel3_handler();
extern void dma1_channel4_handler();
extern void dma1_channel5_handler();
extern void dma1_channel6_handler();
extern void dma1_channel7_handler();
extern void adc_1_and_2_handler();
extern void usb_high_priority_or_can_tx_handler();
extern void usb_low_priority_or_can_rx0_handler();
extern void can_rx1_handler();
extern void can_sce_handler();
extern void external_interrupt_line5_to_9_handler();
extern void timer1_break_handler();
extern void timer1_update_handler();
extern void timer1_trigger_communication_handler();
extern void timer1_capture_compare_handler();
extern void timer2_global_handler();
extern void timer3_global_handler();
extern void timer4_global_handler();
extern void i2c1_event_handler();
extern void i2c1_error_handler();
extern void i2c2_event_handler();
extern void i2c2_error_handler();
extern void spi1_global_handler();
extern void spi2_global_handler();
extern void usart1_global_handler();
extern void usart2_global_handler();
extern void usart3_global_handler();
extern void external_interrupt_line10_to_15_handler();
extern void real_time_clock_alarm_handler();
extern void usb_wakeup_handler();
extern void timer8_break_handler();
extern void timer8_update_handler();
extern void timer8_trigger_communication_handler();
extern void timer8_capture_compare_handler();
extern void adc3_global_handler();
extern void fsmc_global_handler();
extern void sdio_global_handler();
extern void timer5_global_handler();
extern void spi3_global_handler();
extern void usart4_global_handler();
extern void usart5_global_handler();
extern void timer6_global_handler();
extern void timer7_global_handler();
extern void dma2_channel1_handler();
extern void dma2_channel2_handler();
extern void dma2_channel3_handler();
extern void dma2_channel4_5_handler();


//
//  Cortex-M3 only needs the addresses of the ISR handlers.
//  The NVIC (Nested Vector Interrupt Controller) will take
//  care of pushing and popping necessary registers, avoiding
//  the need for handler wrappers in assembly.
//
uint32_t routines[]
__attribute__ ((section("vectors")))
    = {
  (uint32_t)&_system_stack_base,        /* Top stack pointer for NVIC */
  (uint32_t)&start,                     /* Reset entry point */
  (uint32_t)&nmi_handler,               /* NMI */
  (uint32_t)&hard_fault_handler,        /* All falut*/
  (uint32_t)&memory_management_handler, /* Memory Management */
  (uint32_t)&bus_fault_handler,         /* Prefetch/memory access fault */
  (uint32_t)&usage_fault_handler,       /* Usage fault */
  0,                                    /* Reserved */
  0,                                    /* Reserved */
  0,                                    /* Reserved */
  0,                                    /* Reserved */
  (uint32_t)&swi_handler,               /* SWI */
  (uint32_t)&debug_monitor_handler,     /* Debug monitor */
  0,                                    /* Reserved */
  (uint32_t)&pendable_service_handler,  /* Pendable System Service */
  (uint32_t)&porting::systick::sys_tick_timer,            /* System tick timer */
  (uint32_t)&window_watchdog_handler,   /* Window watchdog interrupt */
  (uint32_t)&pvd_handler,               /* PVD */
  (uint32_t)&tamper_handler,            /* Tamper interrupt */
  (uint32_t)&real_time_clock_handler,   /* RTC */
  (uint32_t)&flash_global_handler,      /* Flash global interrupt */
  (uint32_t)&rcc_handler,               /* RCC */
  (uint32_t)&external_interrupt_line0_handler,  /* External interrupt line 0 */
  (uint32_t)&external_interrupt_line1_handler,  /* External interrupt line 1 */
  (uint32_t)&external_interrupt_line2_handler,  /* External interrupt line 2 */
  (uint32_t)&external_interrupt_line3_handler,  /* External interrupt line 3 */
  (uint32_t)&external_interrupt_line4_handler,  /* External interrupt line 4 */
  (uint32_t)&dma1_channel1_handler,             /* DMA 1 channel 1 */
  (uint32_t)&dma1_channel2_handler,             /* DMA 1 channel 2 */
  (uint32_t)&dma1_channel3_handler,             /* DMA 1 channel 3 */
  (uint32_t)&dma1_channel4_handler,             /* DMA 1 channel 4 */
  (uint32_t)&dma1_channel5_handler,             /* DMA 1 channel 5 */
  (uint32_t)&dma1_channel6_handler,             /* DMA 1 channel 6 */
  (uint32_t)&dma1_channel7_handler,             /* DMA 1 channel 7 */
  (uint32_t)&adc_1_and_2_handler,               /* ADC1 and ADC2 */
  (uint32_t)&usb_high_priority_or_can_tx_handler,   /* USB high priority or CAN tx */
  (uint32_t)&usb_low_priority_or_can_rx0_handler,   /* USB low priority or CAN rx 1 */
  (uint32_t)&can_rx1_handler,                       /* CAN rx 1 */
  (uint32_t)&can_sce_handler,                       /* CAN SCE */
  (uint32_t)&external_interrupt_line5_to_9_handler, /* External interrupt line 5 to 9 */
  (uint32_t)&timer1_break_handler,                  /* Timer1 break interrupt */
  (uint32_t)&timer1_update_handler,                 /* Timer1 update interrupt */
  (uint32_t)&timer1_trigger_communication_handler,  /* Timer1 trigger communication interrupt */
  (uint32_t)&timer1_capture_compare_handler,        /* Timer1 capture compare interrupt */
  (uint32_t)&timer2_global_handler,  /* Timer2 global interrupt */
  (uint32_t)&timer3_global_handler,  /* Timer3 global interrupt */
  (uint32_t)&timer4_global_handler,  /* Timer4 global interrupt */
  (uint32_t)&i2c1_event_handler,     /* I2C 1 event interrupt */
  (uint32_t)&i2c1_error_handler,     /* I2C 1 error interrupt */
  (uint32_t)&i2c2_event_handler,     /* I2C 2 event interrupt */
  (uint32_t)&i2c2_error_handler,     /* I2C 2 error interrupt */
  (uint32_t)&spi1_global_handler,    /* SPI 1 global interrupt */
  (uint32_t)&spi2_global_handler,    /* SPI 2 global interrupt */
  (uint32_t)&usart1_global_handler,  /* USART 1 global interrupt */
  (uint32_t)&usart2_global_handler,  /* USART 2 global interrupt */
  (uint32_t)&usart3_global_handler,  /* USART 3 global interrupt */ 
  (uint32_t)&external_interrupt_line10_to_15_handler, /* External interrupt line 10 to 15 */
  (uint32_t)&real_time_clock_alarm_handler, /* Real time clock alarm interrupt */ 
  (uint32_t)&usb_wakeup_handler,                   /* USB wakeup interrupt */
  (uint32_t)&timer8_break_handler,                 /* Timer 8 break interrupt */ 
  (uint32_t)&timer8_update_handler,                /* Timer 8 update interrupt */
  (uint32_t)&timer8_trigger_communication_handler, /* Timer 8 trigger communication interrupt */
  (uint32_t)&timer8_capture_compare_handler,       /* Timer 8 capture compare interrupt */
  (uint32_t)&adc3_global_handler,     /* ADC 3 global interrupt */
  (uint32_t)&fsmc_global_handler,     /* FSMC global interrupt */
  (uint32_t)&sdio_global_handler,     /* SDIO global interrupt */
  (uint32_t)&timer5_global_handler,   /* Timer 5 global interrupt */
  (uint32_t)&spi3_global_handler,     /* SPI 3 global interrupt */
  (uint32_t)&usart4_global_handler,   /* USART 4 global interrupt */ 
  (uint32_t)&usart5_global_handler,   /* USART 5 global interrupt */
  (uint32_t)&timer6_global_handler,   /* TIMER 6 global interrupt */
  (uint32_t)&timer7_global_handler,   /* TIMER 7 global interrupt */
  (uint32_t)&dma2_channel1_handler,   /* DMA 2 Channel 1 interrupt */
  (uint32_t)&dma2_channel2_handler,   /* DMA 2 Channel 2 interrupt */
  (uint32_t)&dma2_channel3_handler,   /* DMA 2 Channel 3 interrupt */
  (uint32_t)&dma2_channel4_5_handler  /* DMA 2 Channel 4 and 5 interrupt */
};
