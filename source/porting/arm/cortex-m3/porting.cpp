//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#include "porting.h"


namespace porting {

namespace arch {

// This is compiler dependent
extern "C" void asm_context_switcher();

void context_switcher()
{
  asm_context_switcher();
}


void critical_section_begin()
{
  asm volatile("cpsid i");
}


void critical_section_end()
{
  asm volatile("cpsie i");
}


void initialize_stack(types::stack_t& stackTop, uint16_t stackSize,
                      types::entryPoint_t entryPoint)
{
  uint32_t* stack = (uint32_t *)stackTop;

  // From Cortex-M3 Technical Reference Manual - section 5.5.1 Stacking,
  // when the processor invokes an exception, it automatically pushes the
  // following eight register to the SP in the following order:
  //    - Processor Status Register (xPSR)
  //    - Program Counter           (r15)
  //    - Link Register (LR)
  //    - r0-r3
  // The SP is decremented by 8 words.
  //
  //    -----------------
  //    |     xPSR      |
  //    |---------------|
  //    |      PC       |
  //    |---------------|
  //    |      LR       |
  //    |---------------|
  //    |      r12      |
  //    |---------------|
  //    |      r3       |
  //    |---------------|
  //    |      r2       |
  //    |---------------|
  //    |      r1       |
  //    |---------------|
  //    |      r0       |
  //    -----------------
  // Thus, the above will be popped out by the hardware on exception
  // return (PENDV)
  *(--stack) = 0x01000000;                // xPSR
  *(--stack) = (uint32_t)entryPoint | 0x1;// PC
  *(--stack) = 0x00000000;                // LR TODO: System::taskExitHandler();
  *(--stack) = 0xCCCCCCCC;                // R12
  *(--stack) = 0x33333333;                // R3
  *(--stack) = 0x22222222;                // R2
  *(--stack) = 0x11111111;                // R1
  *(--stack) = 0x00000000;                // R0

  // Rest of stack will be popped out by the context_switch routine
  *(--stack) = 0xBBBBBBBB;                // R11
  *(--stack) = 0xAAAAAAAA;                // R10
  *(--stack) = 0x99999999;                // R9
  *(--stack) = 0x88888888;                // R8
  *(--stack) = 0x77777777;                // R7
  *(--stack) = 0x66666666;                // R6
  *(--stack) = 0x55555555;                // R5
  *(--stack) = 0x44444444;                // R4

  // Update top of the stack
  stackTop = (uint32_t)stack;
}


void request_context_switch(bool fromISR)
{
  if (fromISR) {
    // Cortex-M3 specific
    // Set PendSV bit
    volatile uint32_t* interruptControlState = (uint32_t *)0xE000ED04;
    *interruptControlState |= (0x01 << 28);
  } else {
    asm volatile
    (
        // Trigger a software interrupt
        "cpsie i\t\n"
        "svc #2"
    );
  }
}


void start_scheduler()
{
  asm volatile
  (
      // Interrupts are disabled at this moment. Re-enable them
      "cpsie i\t\n"
      // Trigger a software interrupt
      "svc #1"
  );
}


} // namespace arch

} // namespace porting
