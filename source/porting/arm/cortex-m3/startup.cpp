//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <stdint.h>

#include <kernel/system.h>

/* The following are defined in the linker script,
 * so their name cannot be translated with the C++ mangling rules */
#ifdef __cplusplus
extern "C" {
#endif

extern uint32_t _bss_start;         /* BSS */
extern uint32_t _bss_end;
extern uint32_t _bss_load;
extern uint32_t _data_start;        /* Data */
extern uint32_t _data_end;
extern uint32_t _data_load;
extern uint32_t _system_stack_base;
extern uint32_t _system_stack_upper_limit;

extern void _init();

extern int main();
struct Task;
extern Task* gCurrentRunningTask;

#ifdef __cplusplus
}
#endif


/*
 * Our entry point.
 */
void start(void)
{
  // Disable interrupt, you never know..
  asm volatile("cpsid i");

/**************** TODO **********************************************************/

  // Boost clock to full speed
  // This is SoC dependent
  // Enable high-speed external oscillator
  volatile uint8_t* clockBaseAddress = (uint8_t *)0x40021000;
  uint8_t offsetControl       = 0x00;
  uint8_t offsetConfiguration = 0x04;
  *((volatile uint32_t *)(clockBaseAddress + offsetControl)) |= (0x01 << 16);
  // Wait for the clock to be stable
  while (1) {
    uint32_t tmp = *((volatile uint32_t *)(clockBaseAddress + offsetControl));
    if (tmp & (0x01 << 17))
      break;
  }

  // Set external clock as source
  // External oscillator is 8Mhz
  uint8_t source = 0x01;
  *((volatile uint32_t *)(clockBaseAddress + offsetConfiguration)) &= ~(0x03 << 16);
  *((volatile uint32_t *)(clockBaseAddress + offsetConfiguration)) |= (source << 16);

  // Multiply by 9 in order to get full speed (72Mhz)
  // TODO: this is board dependent!
  uint8_t factor = 0x07; // x9
  *((volatile uint32_t *)(clockBaseAddress + offsetConfiguration)) &= ~(0x0F << 18);
  *((volatile uint32_t *)(clockBaseAddress + offsetConfiguration)) |= (factor << 18);

  // Enable PLL
  *((volatile uint32_t *)(clockBaseAddress + offsetControl)) |= (0x01 << 24);
  // Return when PLL is locked
  while (true) {
    uint32_t tmp = *((volatile uint32_t *)(clockBaseAddress + offsetControl));
    if (tmp & (0x01 << 25))
      break;
  }

  // Set Prescaler for AHB bus to 1
  *(clockBaseAddress + offsetConfiguration) &= ~(0x0F << 4);

  // Set Prescaler for (faster) APB bus to 1
  *((uint16_t *)(clockBaseAddress + offsetConfiguration)) &= ~(0x07 << 11);

  // Set Prescaler for (slower) APB bus to 2
  *((uint16_t *)(clockBaseAddress + offsetConfiguration)) &= ~(0x07 << 8);
  *((uint16_t *)(clockBaseAddress + offsetConfiguration)) |= (0x04 << 8);

  // Set access wait state at 2
  *(volatile uint32_t *)0x40022000 |= 0x02;

  // Set PLL as system clock
  uint8_t PLL = 0x02;
  *((volatile uint8_t *)(clockBaseAddress + offsetConfiguration)) &= ~0x03;
  *((volatile uint8_t *)(clockBaseAddress + offsetConfiguration)) |= PLL;

  // Wait untill PLL is used a system clock */
  while (1) {
    uint8_t tmp = (*(clockBaseAddress + offsetConfiguration) & (0x03 << 2)) >> 2;
    if (tmp == 0x02)
      break;
  }

/**************** TODO END **********************************************************/

  // Relocate data segment
  uint32_t* data_src = &_data_start;
  uint32_t* data_dst = &_data_load;
  for (; data_src < &_data_end; ++data_src, ++data_dst)
    *data_dst = *data_src;

  // Zero-out BSS area
  uint32_t* bss_src = &_bss_start;
  uint32_t* bss_dst = &_bss_load;
  for (; bss_src < &_bss_end; ++bss_src, ++bss_dst)
    *bss_dst = 0;

  // Fill up system stack with predefined pattern 0xdeadbeef
  // TODO: debug only
  uint32_t* system_stack_start = &_system_stack_upper_limit;
  uint32_t* system_stack_end = &_system_stack_base;
  for (; system_stack_start < system_stack_end; ++system_stack_start)
    *system_stack_start = 0xdeadbeef;

  // Init the system. Jumpt to architecture indipendent code
  System::init();

  // We should never get here
  while (1);
}
