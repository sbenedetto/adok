//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef __TIMER_H__
#define __TIMER_H__

#include <stdint.h>

namespace porting {

namespace timer {

///
/// \brief start timer in upcounting mode
///
/// Once started the timer must wrap around silently without triggering
/// any interrupt
void start();

///
/// \brief return current number of ticks
///
uint16_t numberOfTicks();


///
/// \brief convert given number of ticks to millisecond
///
uint16_t ticksToMS(uint16_t ticks);


///
/// \brief convert given millisecond to ticks
///
uint16_t MSToTicks(uint16_t milliseconds);


} // namespace timer

} // namespace porting

#endif // __TIMER_H__
