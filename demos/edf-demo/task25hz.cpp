
// How it should be done
// TASK(Task25hz)
// TASK_EXECUTE_EVERY_MS(40)
// TASK_STACK_SIZE_BYTE(1024)
// TASK_BODY(body)

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>
#include <drivers/nvic.h>

#include "application_data.h"

class Task25hz
{
private:
  Task        task;
  uint32_t    stackSizeInBytes = 1024;
  stack_t     stack[1024 / sizeof(stack_t)];

public:

  Task25hz()
  {
    task.stackTop       = (stack_t)(&stack[stackSizeInBytes / sizeof(stack_t)]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.periodMS       = 40; // 25Hz
    task.startAfterMS   = 0;
    task.entryPoint     = (entry_point_t)&Task25hz::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA25A25AA));

    System::registerTask(&task);
  }

  static void run()
  {
    GPIO& gpioC = Application::data::gpioC;

    while (1) {

      gpioC.low(GPIO::ePin12);
      NVIC::activeDelayMS(5);
      gpioC.high(GPIO::ePin12);

      System::waitForNextRun();
    }

  }
};

static Task25hz _;
