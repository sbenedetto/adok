
#include <application.h>

#include <drivers/nvic.h>
#include <drivers/stm32/clock.h>
#include <drivers/stm32/gpio.h>

#include <messageQueue.h>


namespace Application
{
  // Define data shared among the tasks
  namespace data {
    GPIO gpioB(GPIO::ePortB);
    GPIO gpioC(GPIO::ePortC);
  }

  void init()
  {
    /* Enable clock for peripherals used */
    clock::enableClockFor(clock::eGPIOB);
    clock::enableClockFor(clock::eGPIOC);

    // Set up pin 12 (User LED) of GPIO Port C in push pull mode
    GPIO& gpioPortC = data::gpioC;
    gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

    GPIO& gpioPortB = data::gpioB;
    gpioPortB.setMode(GPIO::ePin6, GPIO::eOutputPushPull2Mhz);
    gpioPortB.setMode(GPIO::ePin7, GPIO::eOutputPushPull2Mhz);

    gpioPortB.high(GPIO::ePin6);
    gpioPortB.high(GPIO::ePin7);

    gpioPortC.high(GPIO::ePin12);
  }
}
