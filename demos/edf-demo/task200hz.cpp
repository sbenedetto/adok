
// How it should be done
// TASK(Task200hz)
// TASK_EXECUTE_EVERY_MS(5)
// TASK_STACK_SIZE_BYTE(1024)
// TASK_BODY(body)

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>
#include <drivers/nvic.h>

#include "application_data.h"

class Task200hz
{
private:
  Task        task;
  uint32_t    stackSizeInBytes = 1024;
  stack_t     stack[1024 / sizeof(stack_t)];

public:

  Task200hz()
  {
    task.stackTop       = (stack_t)(&stack[stackSizeInBytes / sizeof(stack_t)]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.periodMS       = 5; // 200Hz
    task.startAfterMS   = 0;
    task.entryPoint     = (entry_point_t)&Task200hz::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA200A200));

    System::registerTask(&task);
  }

  static void run()
  {
    GPIO& gpioB = Application::data::gpioB;

    while (1) {

      gpioB.low(GPIO::ePin7);
      NVIC::activeDelayMS(1);
      gpioB.high(GPIO::ePin7);

      System::waitForNextRun();
    }
  }
};

static Task200hz _;
