
// How it should be done
// TASK(Task100hz)
// TASK_EXECUTE_EVERY_MS(10)
// TASK_STACK_SIZE_BYTE(1024)
// TASK_BODY(body)

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>
#include <drivers/nvic.h>

#include "application_data.h"

class Task100hz
{
private:
  Task        task;
  uint32_t    stackSizeInBytes = 1024;
  stack_t     stack[1024 / sizeof(stack_t)];

public:

  Task100hz()
  {
    task.stackTop       = (stack_t)(&stack[stackSizeInBytes / sizeof(stack_t)]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.periodMS       = 10; // 100Hz
    task.startAfterMS   = 0;
    task.entryPoint     = (entry_point_t)&Task100hz::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA1A1A1A1));

    System::registerTask(&task);
  }

  static void run()
  {
    GPIO& portB = Application::data::gpioB;
    while (1) {

      portB.low(GPIO::ePin6);
      NVIC::activeDelayMS(1);
      portB.high(GPIO::ePin6);

      System::waitForNextRun();
    }

  }
};

static Task100hz _;
