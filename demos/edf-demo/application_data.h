
#include <drivers/stm32/usart.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/spi.h>

#include <messageQueue.h>

namespace Application
{
  namespace data
  {
    extern GPIO gpioB;
    extern GPIO gpioC;
  }
}
