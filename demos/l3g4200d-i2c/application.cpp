
#include <application.h>

#include <drivers/nvic.h>
#include <drivers/stm32/clock.h>
#include <drivers/stm32/externalInterrupt.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/usart.h>
#include <drivers/stm32/i2c.h>
#include <drivers/stm32/timer.h>

#include <sensors/l3g4200d.h>

#include <messageQueue.h>


namespace Application
{
  // Define data shared among the tasks
  namespace data {
    GPIO gpioA(GPIO::ePortA);
    GPIO gpioB(GPIO::ePortB);
    GPIO gpioC(GPIO::ePortC);
    USART usart1(USART::eUSART1);
    I2C i2c(I2C::eI2C1);
    L3G4200D gyroscope(&i2c);
    MessageQueue<> loggerQueue;
  }

  void init()
  {
    // Enable clock for peripherals used
    clock::enableClockFor(clock::eGPIOA);
    clock::enableClockFor(clock::eGPIOB);
    clock::enableClockFor(clock::eGPIOC);
    clock::enableClockFor(clock::eUSART1);
    clock::enableClockFor(clock::eI2C1);
    clock::enableClockFor(clock::eTimer3);

    // Set GPIOC::pin12 for measuring taskSensorReader
    GPIO& gpioPortC = data::gpioC;
    gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);
    gpioPortC.high(GPIO::ePin12);

    // Set GPIOA::pin8 for measureing taskDataLogger
    GPIO& gpioPortA = data::gpioA;
    gpioPortA.setMode(GPIO::ePin8, GPIO::eOutputPushPull2Mhz);
    gpioPortA.high(GPIO::ePin8);

    ///////////////////////////
    // Enable I2C controller //
    ///////////////////////////
    /* GPIO B - Configure I2C1 GPIO port pin to correct function */
    GPIO gpioPortB(GPIO::ePortB);
    /* SCL */
    gpioPortB.setMode(GPIO::ePin6, GPIO::eAlternateFunctionOpenDrain2Mhz);
    /* SDA */
    gpioPortB.setMode(GPIO::ePin7, GPIO::eAlternateFunctionOpenDrain2Mhz);

    /* Configure I2C1 */
    I2C& i2c = data::i2c;
    i2c.enableAcknowledge();
    i2c.setMode(I2C::eStandard100Khz);
    i2c.enable();

    //////////////////////////////
    // Enable USART for logging //
    //////////////////////////////
    /* Set GPIO alternate function for usart controller */
    gpioPortA.setMode(GPIO::ePin9, GPIO::eAlternateFunctionPushPull10Mhz);
    gpioPortA.setMode(GPIO::ePin10, GPIO::eInputPullUpPullDown);

    // Configure USART controller
    USART& usart = data::usart1;
    usart.enable();
    // Set 8 bit word
    usart.wordLength9();
    // Set 1 stop bit
    usart.setStopBits(USART::eStopBit1);
    // Set parity bit
    usart.parityControlEnable();
    usart.parityEven();
    // Set baud rate: peripheral clock should be 72Mhz
    uint16_t baudRate = 0x27A; // 115200 bps at 72Mhz
    usart.setBaudRate(baudRate);
    // Set transmitter
    usart.transmitterEnable();
    // Enable interrupts
    NVIC::enableIRQ(37);

    // Init the sensor
    L3G4200D& gyroscope = Application::data::gyroscope;
    gyroscope.init();
  }
}
