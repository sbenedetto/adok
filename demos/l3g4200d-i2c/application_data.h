
#include <drivers/stm32/usart.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/i2c.h>
#include <drivers/stm32/timer.h>

#include <sensors/l3g4200d.h>

#include <messageQueue.h>

namespace Application
{
  // Define data shared among the tasks
  namespace data {
    extern GPIO gpioA;
    extern GPIO gpioB;
    extern GPIO gpioC;
    extern USART usart1;
    extern MessageQueue<> loggerQueue;
    extern I2C i2c;
    extern L3G4200D gyroscope;
  }
}
