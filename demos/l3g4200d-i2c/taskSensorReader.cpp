//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <stdint.h>

#include <drivers/nvic.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/timer.h>

#include <sensors/l3g4200d.h>

#include <messageQueue.h>
#include <task.h>

#include <lib/utility/converter.h>

#include "application_data.h"

#include "barriers.h"


inline static
void
removeZeroOffset(int16_t* samples)
{
  samples[0] -= Application::data::gyroAtZero[0];
  samples[1] -= Application::data::gyroAtZero[1];
  samples[2] -= Application::data::gyroAtZero[2];
}


void TaskSensorReaderInit()
{
  L3G4200D& gyroscope = Application::data::gyroscope;

  // Recognize the sensor
  while (true) {

    const int8_t chipID = 0xD3;
    if (gyroscope.isOnBus(chipID))
      break;

    // else wait and try again
    NVIC::activeDelayUS(100);
  }

  // Calculate zero offset
  const uint8_t kSamples = 128;
  int32_t samples[3] = {0, 0, 0};
  int16_t  current[3];
  for (uint8_t i = 0; i < kSamples; i++) {
    gyroscope.readGyroscopeData(current[0], current[1], current[2]);
    samples[0] += current[0];
    samples[1] += current[1];
    samples[2] += current[2];
    // There is not point in acquiring continously
    NVIC::activeDelayUS(500);
  }
  int8_t (&zeroOffset)[3] = Application::data::gyroAtZero;
  zeroOffset[0] = samples[0] >> 7;
  zeroOffset[1] = samples[1] >> 7;
  zeroOffset[2] = samples[2] >> 7;
}


static inline
void
calculateAngularDisplacement(int16_t* samples)
{
  // Convert to degree-per-second
  float dps[3];
  int8_t (&zeroOffset)[3] = Application::data::gyroAtZero;
  dps[0] = (samples[0] - zeroOffset[0]) * 0.00875;
  dps[1] = (samples[1] - zeroOffset[0]) * 0.00875;
  dps[2] = (samples[2] - zeroOffset[0]) * 0.00875;

  // Calculate current position. We are running with a period of 10ms
  int16 (&currentInclination)[3] = Application::data::currentRotation;
  currentRotation[0] += dps[0] * 0.01;
  currentRotation[1] += dps[1] * 0.01;
  currentRotation[2] += dps[2] * 0.01;

}


void TaskSensorReader()
{
  // Start measure period
  GPIO& gpioPortC = Application::data::gpioC;
  gpioPortC.low(GPIO::ePin12);

  // Put data into the logger task message queue
  MessageQueue<>& queue = Application::data::loggerQueue;
  Message* message = queue.create();
  if (message == NULL) {
    // Stop measure
    gpioPortC.high(GPIO::ePin12);
    return;
  }

  // Data rate is 100Hz, thus T = 0.01s
  int16_t samples[3] = {0, 0, 0};
  L3G4200D& gyroscope = Application::data::gyroscope;
  gyroscope.readGyroscopeData(samples[0], samples[1], samples[2]);
  calculateAngularDisplacement(samples);

  // Copy data to buffer by hand for now...
  //sprintf((char *)message->data, "x=%d - y=%d - z=%d - t=%d\n", x, y, z, temperature);
  int16 (&displacement)[3] = Application::data::currentRotation;
  char* buffer = (char *)message->data;
  uint8_t endAt = utility::fromInt16ToASCII(buffer, displacement[0]);
  buffer[endAt++] = ' ';
  endAt += utility::fromInt16ToASCII(buffer + endAt, displacement[1]);
  buffer[endAt++] = ' ';
  endAt += utility::fromInt16ToASCII(buffer + endAt, displacement[2]);
  buffer[endAt++] = '\n';
  buffer[endAt] = '\0';
  queue.put(message);

  // Stop measure
  gpioPortC.high(GPIO::ePin12);
}


TASK_PERIODIC_EVERY_MS_OFFSET_WITH_INIT(TaskSensorReader, 10, 5, TaskSensorReaderInit);
