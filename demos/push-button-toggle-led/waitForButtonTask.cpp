
#include <semaphore.h>
#include <system.h>
#include <task.h>

#include <drivers/stm32/externalInterrupt.h>
#include <drivers/stm32/gpio.h>

#include <debug.h>

bool gButtonPushed = false;

void external_interrupt_line0_handler()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine0);

  gButtonPushed = true;
}


class WaitForButtonTask
{
public:
  Task task;

  WaitForButtonTask()
  {
    // TODO: automate this
    task.stackTop       = (stack_t)(0x20004000);
    task.msToPreemption = 5;
    task.stackSize      = 0x400;
    task.currentState   = taskState_t::eReady;
    task.id             = 2;
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&WaitForButtonTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA5A5A5A5));

    System::registerTask(&task);
  }

  ~WaitForButtonTask()
  {
  }

static void run()
  {
    GPIO led(GPIO::ePortC);

    extern Semaphore semaphore;

    while (true) {

      // Notify the other task
      semaphore.release();
      // Wait by polling for push button
      while (!gButtonPushed) ;
      // Let's not worry about atomic and polling right now..
      gButtonPushed = false;
    }
  }
};


static WaitForButtonTask _;
