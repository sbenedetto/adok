#include <application.h>

#include <drivers/nvic.h>

#include <drivers/stm32/afio.h>
#include <drivers/stm32/clock.h>
#include <drivers/stm32/externalInterrupt.h>
#include <drivers/stm32/gpio.h>

#include <semaphore.h>

// TODO: Fix this. Provide a way to share data among tasks

Semaphore semaphore(1);

/*
static const Data gkData;


Data*
Application::data()
{
  return gData;
}
*/


void
Application::init()
{
  // Push button is on GPIO Port A
  // Status led is on GPIO Port C
  clock::enableClockFor(clock::eGPIOA);
  clock::enableClockFor(clock::eGPIOC);

  // Configure pin0 as input
  GPIO gpioPortA(GPIO::ePortA);
  gpioPortA.setMode(GPIO::ePin0, GPIO::eInputPullUpPullDown);

  // Since EXTI0 is shared among all pins 0 of all GPIO ports,
  // we need to select port A as source in the multiplexer
  // through the AFIO register
  clock::enableClockFor(clock::eAFIO);
  afio::selectSourceForEvent(afio::eEXTI0, afio::ePortA);

  // Enable the interrupt on line 0 both on rising and falling edge
  // User button is on PA0, thus EXTI0
  ExternalInterrupt exti;
  exti.interruptUnmask(ExternalInterrupt::eLine0);
  exti.triggerRisingEdgeEnable(ExternalInterrupt::eLine0);
  exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine0);

  // Set up pin 12 of GPIO Port C in push pull mode
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

  // Enable interrupts on line 6
  NVIC::enableIRQ(6);
}
