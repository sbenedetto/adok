
#include <semaphore.h>
#include <system.h>
#include <task.h>

#include <drivers/stm32/gpio.h>

#include <debug.h>

class ToggleLedTask
{
public:
  Task task;

  ToggleLedTask()
  {
    // TODO: automate this
    task.stackTop       = (stack_t)(0x20004000 - 0x400);
    task.msToPreemption = 5;
    task.stackSize      = 0x400;
    task.currentState   = taskState_t::eReady;
    task.id             = System::getUniqueTaskID();
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&ToggleLedTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0x12345678));

    System::registerTask(&task);
  }

  ~ToggleLedTask()
  {
  }

static void run()
  {
    GPIO led(GPIO::ePortC);

    extern Semaphore semaphore;

    while (true) {

      semaphore.acquire();
      led.toggle(GPIO::ePin12);
    }
  }
};

static ToggleLedTask _;
