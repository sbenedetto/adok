
#include <stdint.h>

#include <kernel/task_builder.h>
#include <drivers/stm32/gpio.h>

#include <libs/sensors/l3g4200d.h>

#include "../../application_data.h"

void gyroscopeInit()
{
  GPIO gpio(GPIO::ePortC);
  gpio.low(GPIO::ePin12);

  L3G4200D& gyroscope = Application::data::gyroscope;

  // Read samples
  int16_t samples[3];
  gyroscope.readGyroscopeData(samples[0], samples[1], samples[2]);

  // Updates accumulator - It might overflow
  int32_t (&accumulator)[3] = Application::data::gyroAccumulator;
  accumulator[0] += samples[0];
  accumulator[1] += samples[1];
  accumulator[2] += samples[2];

  Application::data::gyroNumberOfSamples++;
  gpio.high(GPIO::ePin12);
}

TASK_PERIOD_STATE(gyroscopeInit, 20, Application::eState_t::Init);
