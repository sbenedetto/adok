
#include <stdint.h>

#include <drivers/stm32/gpio.h>
#include <kernel/task_builder.h>

#include <libs/sensors/bma180.h>

#include "../../application_data.h"

void accelerometerInit()
{
  GPIO gpio(GPIO::ePortA);
  gpio.low(GPIO::ePin8);

  BMA180& accelerometer = Application::data::accelerometer;

  // Read samples
  int16_t samples[3];
  accelerometer.readAccelerationData(samples[0], samples[1], samples[2]);

  // Updates accumulator - It might overflow
  int32_t (&accumulator)[3] = Application::data::accelerometerAccumulator;
  accumulator[0] += samples[0];
  accumulator[1] += samples[1];
  accumulator[2] += samples[2];

  Application::data::accelerometerNumberOfSamples++;
  gpio.high(GPIO::ePin8);
}

TASK_PERIOD_STATE(accelerometerInit, 20, Application::eState_t::Init);
