
#include <stdint.h>

#include <drivers/stm32/gpio.h>
#include <kernel/task_builder.h>

#include <libs/sensors/bma180.h>

#include "../../application_data.h"

void accelerometerReader()
{
  GPIO gpio(GPIO::ePortA);
  gpio.low(GPIO::ePin8);

  BMA180& accelerometer = Application::data::accelerometer;

  // Read samples
  int16_t samples[3];
  accelerometer.readAccelerationData(samples[0], samples[1], samples[2]);

  // TODO: pass it to the position estimator

  gpio.high(GPIO::ePin8);
}

TASK_PERIOD_STATE(accelerometerReader, 10, Application::eState_t::Operative);
