
#include <stdint.h>

#include <kernel/task_builder.h>
#include <drivers/stm32/gpio.h>

#include <libs/sensors/l3g4200d.h>

#include "../../application_data.h"

void gyroscopeReader()
{
  GPIO gpio(GPIO::ePortC);
  gpio.low(GPIO::ePin12);

  L3G4200D& gyroscope = Application::data::gyroscope;

  // Read samples
  int16_t samples[3];
  gyroscope.readGyroscopeData(samples[0], samples[1], samples[2]);

  // TODO: pass it to the position estimator

  gpio.high(GPIO::ePin12);
}

TASK_PERIOD_STATE(gyroscopeReader, 10, Application::eState_t::Operative);
