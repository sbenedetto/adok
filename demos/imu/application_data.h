//
// This file is part of the adok project.
//
// Copyright (C) 2013 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef __APPLICATION_DATA_H__
#define __APPLICATION_DATA_H__

#include <stdint.h>

class L3G4200D;
class BMA180;


namespace Application
{
  enum class eState_t : uint8_t
  {
    Init        = 1,
    Operative   = 2
  };

  namespace data
  {
    extern L3G4200D gyroscope;
    extern BMA180   accelerometer;

    extern int16_t gyroZeroOffset[3];
    extern int32_t gyroAccumulator[3];
    extern int16_t gyroNumberOfSamples;

    extern int16_t accelerometerZeroOffset[3];
    extern int32_t accelerometerAccumulator[3];
    extern int16_t accelerometerNumberOfSamples;

  }
}
#endif // __APPLICATION_DATA_H__

