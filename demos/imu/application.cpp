
#include "application_data.h"

#include <drivers/nvic.h>

#include <drivers/stm32/afio.h>
#include <drivers/stm32/clock.h>
#include <drivers/stm32/externalInterrupt.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/i2c.h>
#include <drivers/stm32/spi.h>
#include <drivers/stm32/usart.h>

#include <kernel/application.h>
#include <kernel/system.h>

#include <libs/sensors/bma180.h>
#include <libs/sensors/l3g4200d.h>

void external_interrupt_line0_handler()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine0);

  // Toggle between init and operative state
  Application::eState_t state = System::currentState();
  if (state & Application::eState_t::Init)
    System::setState(Application::eState_t::Operative);
  else
    System::setState(Application::eState_t::Init);
}

namespace Application {

  namespace data {
    GPIO gpioA(GPIO::ePortA);
    GPIO gpioB(GPIO::ePortB);
    GPIO gpioC(GPIO::ePortC);

    USART usart1(USART::eUSART1);

    I2C i2c(I2C::eI2C1);
    L3G4200D gyroscope(&i2c);

    SPI spi(SPI::eSPI1);
    BMA180 accelerometer(&spi);

    int16_t gyroZeroOffset[3];
    int32_t gyroAccumulator[3];
    int16_t gyroNumberOfSamples;

    int16_t accelerometerZeroOffset[3];
    int32_t accelerometerAccumulator[3];
    int16_t accelerometerNumberOfSamples;
  }


  void init()
  {
    // Enable clock for peripherals used
    clock::enableClockFor(clock::eGPIOA);
    clock::enableClockFor(clock::eGPIOB);
    clock::enableClockFor(clock::eGPIOC);
    clock::enableClockFor(clock::eUSART1);
    clock::enableClockFor(clock::eI2C1);
    clock::enableClockFor(clock::eSPI1);
    clock::enableClockFor(clock::eAFIO);

    // Init I2C
    // GPIO B - Configure I2C1 GPIO port pin to correct function
    // SCL
    data::gpioB.setMode(GPIO::ePin6, GPIO::eAlternateFunctionOpenDrain2Mhz);
    // SDA
    data::gpioB.setMode(GPIO::ePin7, GPIO::eAlternateFunctionOpenDrain2Mhz);

    // Configure I2C1
    I2C& i2c = data::i2c;
    i2c.enableAcknowledge();
    i2c.setMode(I2C::eStandard100Khz);
    i2c.enable();

    // Init SPI
    // GPIO A - Configure SPI GPIO port pin to correct function
    // Clock
    data::gpioA.setMode(GPIO::ePin5, GPIO::eAlternateFunctionPushPull10Mhz);
    // MOSI
    data::gpioA.setMode(GPIO::ePin7, GPIO::eAlternateFunctionPushPull10Mhz);
    // MISO
    data::gpioA.setMode(GPIO::ePin6, GPIO::eInputPullUpPullDown);

    // Configure SPI
    SPI& spi = data::spi;
    spi.clockPhaseSecondEdge();
    spi.clockPolarityIdleHigh();
    spi.dataFormatMSB();
    spi.dataFormat8();
    spi.masterMode();
    // Use GPIO Port C pin 10 as chip select
    spi.manageNSSByHardware(false);
    data::gpioC.setMode(GPIO::ePin10, GPIO::eOutputPushPull10Mhz);
    spi.fullDuplex();
    spi.baudRateControl(SPI::eFpclk256);
    // Finally enable SPI
    spi.enable();
    NVIC::enableIRQ(35);

    // Init gyroscope
    L3G4200D& gyroscope = Application::data::gyroscope;
    gyroscope.init();

    while (true) {
      const int8_t chipID = 0xD3;
      if (gyroscope.isOnBus(chipID))
        break;

      NVIC::activeDelayUS(100);
    }

    // Init accelerometer
    BMA180& bma180 = Application::data::accelerometer;
    // Bring the sensor to a known state
    bma180.softReset();
    bma180.disableI2C();
    bma180.init();

    while (true) {
      const int8_t chipID = 0x03;
      if (bma180.isOnBus(chipID))
        break;

      // else wait and retry
      NVIC::activeDelayUS(100);
    }

    // Configure USART controller
    USART& usart = data::usart1;
    usart.enable();
    // Set 8 bit word
    usart.wordLength9();
    // Set 1 stop bit
    usart.setStopBits(USART::eStopBit1);
    // Set parity bit
    usart.parityControlEnable();
    usart.parityEven();
    // Set baud rate: peripheral clock should be 72Mhz
    uint16_t baudRate = 0x27A; // 115200 bps at 72Mhz
    usart.setBaudRate(baudRate);
    // Set transmitter
    usart.transmitterEnable();
    // Enable interrupts
    NVIC::enableIRQ(37);

    // DEBUG
    data::gpioA.setMode(GPIO::ePin8, GPIO::eOutputPushPull2Mhz);
    data::gpioC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

    // Set initial state
    System::setState(Application::eState_t::Init);

    // Set button - Configure pin0 as input
    data::gpioA.setMode(GPIO::ePin0, GPIO::eInputPullUpPullDown);
    // Since EXTI0 is shared among all pins 0 of all GPIO ports,
    // we need to select port A as source in the multiplexer
    // through the AFIO register
    afio::selectSourceForEvent(afio::eEXTI0, afio::ePortA);

    // Enable the interrupt on line 0 both on rising and falling edge
    // User button is on PA0, thus EXTI0
    ExternalInterrupt exti;
    exti.interruptUnmask(ExternalInterrupt::eLine0);
    exti.triggerRisingEdgeEnable(ExternalInterrupt::eLine0);
    //exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine0);

    // Enable interrupts on line 6
    NVIC::enableIRQ(6);

  }

  void onChangeState(eState_t newState)
  {
    using namespace data;

    // Execute code based on event
    if (newState & eState_t::Operative) {
      gyroZeroOffset[0] = gyroAccumulator[0] / gyroNumberOfSamples;
      gyroZeroOffset[1] = gyroAccumulator[1] / gyroNumberOfSamples;
      gyroZeroOffset[2] = gyroAccumulator[2] / gyroNumberOfSamples;

      accelerometerZeroOffset[0] = accelerometerAccumulator[0] / accelerometerNumberOfSamples;
      accelerometerZeroOffset[1] = accelerometerAccumulator[1] / accelerometerNumberOfSamples;
      accelerometerZeroOffset[2] = accelerometerAccumulator[2] / accelerometerNumberOfSamples;
    }
  }
}
