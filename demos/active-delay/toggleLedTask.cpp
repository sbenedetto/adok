//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <system.h>
#include <task.h>
#include <debug.h>

#include <drivers/nvic.h>
#include <drivers/stm32/gpio.h>


class ToggleLedTask
{
public:
  // Task Control Block.
  Task task;

  ToggleLedTask()
  {
    // TODO: this needs to be automated
    task.stackTop     = (stack_t)(0x20004000);
    task.msToPreemption = 10;
    task.stackSize    = 0x400;
    task.currentState = taskState_t::eReady;
    task.id           = System::getUniqueTaskID();
    task.type         = taskType_t::ePeriodic;
    task.priority     = 10; // TODO
    task.entryPoint   = (entry_point_t)&ToggleLedTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA1A1A1A1));

    System::registerTask(&task);
  }

  // Task body
  static void run()
  {
    GPIO gpio(GPIO::ePortC);

    while (true) {
      // Led ON
      gpio.low(GPIO::ePin12);
      NVIC::activeDelayMS(10);
      gpio.high(GPIO::ePin12);
      NVIC::activeDelayUS(10);
    }
  }
};

ToggleLedTask task1;


void fillUpStack(uint32_t* begin, uint32_t* end, uint32_t pattern)
{
  while (begin != end)
    *begin++ = pattern;
}
