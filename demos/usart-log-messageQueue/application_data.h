
#include <drivers/stm32/usart.h>
#include <drivers/stm32/gpio.h>

#include <messageQueue.h>

namespace Application
{
  namespace data
  {
    extern USART usart1;
    extern GPIO gpioC;
    extern MessageQueue<> producerToLogger;
  }
}
