/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>

#include <messageQueue.h>

#include "application_data.h"

class DataProducerTask
{
private:
  Task          task;
  uint32_t      stackSizeInBytes = 1024;
  stack_t       stack[ 1024 / sizeof(stack_t) ];

public:
  DataProducerTask()
  {
    task.stackTop       = (stack_t)(&stack[ stackSizeInBytes / sizeof(stack_t) ]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.msToPreemption = 1;
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&DataProducerTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xbeeeedda));

    System::registerTask(&task);
  }

  static void run()
  {
      /* Send data */
      const char* dataOut = "abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ\n";
      uint16_t length = 0;
      while (dataOut[length++] != '\0')
          ;
      uint8_t index = 0;

      // Get a reference to the applications data needed
      // TODO: find a better way for sharing data. Almost there...
      //       Data have to be defined only in one place, thus cannot
      //       be a header.
      GPIO& statusLed = Application::data::gpioC;
      MessageQueue<>& queue = Application::data::producerToLogger;

      while (true) {

        // Signal activity
        // TODO: this syntax would be much better and probably would
        //       produce better code (no jump, all inline, no need of
        //       a object)
        // using statusLed = gpio<GPIO::ePortC, GPIO::ePortC>
        // statusLed.toggle();
        statusLed.toggle(GPIO::ePin12);

        // Get a message
        Message* message = queue.create();
        if (message == NULL)
          continue;
 
        // Fillup the message
        for (int8_t i = 0; i < sizeof(message->data); i++) {
          message->data[i] = dataOut[index];
          index = (index + 1) % length;
        }

        // Send it. The receiver is in charge of deallocating it
        queue.put(message);
    }
  }
};


static DataProducerTask _;
