
#include <application.h>

#include <drivers/stm32/gpio.h>
#include <drivers/stm32/clock.h>
#include <drivers/nvic.h>
#include <drivers/stm32/usart.h>

#include <messageQueue.h>


namespace Application
{
  // Define data shared among the tasks
  namespace data {
    GPIO gpioC(GPIO::ePortC);
    USART usart1(USART::eUSART1);
    MessageQueue<> producerToLogger;
  }

  void init()
  {
    /* Enable clock for controller */
    clock::enableClockFor(clock::eGPIOC);
    clock::enableClockFor(clock::eUSART1);

    // TODO: need a way to share data among tasks and application.cpp
    // Set up pin 12 (User LED) of GPIO Port C in push pull mode
    GPIO& gpio = data::gpioC;
    gpio.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);
    gpio.setMode(GPIO::ePin1, GPIO::eOutputPushPull2Mhz);

    /***************************
      Enable USART for logging
    ****************************/

    /* Set GPIO alternate function for usart controller */
    clock::enableClockFor(clock::eGPIOA);
    GPIO gpioPortA(GPIO::ePortA);
    gpioPortA.setMode(GPIO::ePin9, GPIO::eAlternateFunctionPushPull10Mhz);
    gpioPortA.setMode(GPIO::ePin10, GPIO::eInputPullUpPullDown);

    /* Enable USART controller */
    USART& usart = data::usart1;
    usart.enable();
    /* Set 8 bit word */
    usart.wordLength9();
    /* Set 1 stop bit */
    usart.setStopBits(USART::eStopBit1);
    /* Set parity bit */
    usart.parityControlEnable();
    usart.parityEven();
    /* Set baud rate: peripheral clock should be 72Mhz */
    uint16_t baudRate = 0x27A; // 115200 bps at 72Mhz
    usart.setBaudRate(baudRate);
    /* Set transmitter */
    usart.transmitterEnable();
    /* Enable interrupts */
    //NVIC::enableIRQ(37);
  }

}
