/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>
#include <drivers/stm32/usart.h>
#include <drivers/nvic.h>


class USARTLoggerTask
{
private:
  Task          task;
  uint32_t      stackSizeInBytes = 1024;
  stack_t       stack[ 1024 / sizeof(stack_t) ];

public:
  USARTLoggerTask()
  {
    task.stackTop       = (stack_t)(&stack[ stackSizeInBytes ]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();

    task.currentState   = taskState_t::eReady;
    task.msToPreemption = 100;
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&USARTLoggerTask::run;

    // TODO: debug
    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xAA55AA55));

    System::registerTask(&task);
  }

  static void run()
  {
      /* Send data */
    //  const char* dataOut = "abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ\n";
      const char* dataOut = "a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5a5\n";
      uint16_t length = 0;
      while (dataOut[length++] != '\0')
          ;

      // TODO: need a way to share data among tasks and application.cpp
      USART usart(USART::eUSART1);
      NVIC::enableIRQ(37);

      GPIO gpio(GPIO::ePortC);

      while (true) {

        /* Toggle the led */
        gpio.toggle(GPIO::ePin12);
        usart.send(dataOut, length);
        }
    }
};


static USARTLoggerTask _;
