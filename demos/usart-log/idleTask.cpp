//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <system.h>
#include <task.h>

#include <debug.h>

// Taking notes..
//
// A possible way to "describe" the below task in order
// to be easily generated automatically is
// file.adok
// -- BEGIN FILE --
// TASK(IdleTask)                            (1)
// TASK_PERIOD_IN_MS(100)                    (2)
// TASK_STACK_SIZE_IN_BYTE(1024)             (3)
// TASK_MAIN_LOOP(myTaskBody)                (4)
//
// void myTaskBody() {
//     counter++;
// }
// -- END FILE --
//
// (1): It is the name of the class. It could also be used as descripted
//      name (char*) for the task.
// (2): It depends on the scheduler
// (3): Defaults value are also provided
// (4) the myTaskBody goes automatically into an infinite loop.
//
// NOTE:
//    should the user define an offset start or should we generate
//        that automatically? It could be easily generated like task.id
//

class IdleTask
{
private:
  // Task Control Block
  Task     task;                                                   // OK: automated
  uint32_t stackSizeInBytes = 64;                                  // OK: automated
  stack_t  stack[ 64 / sizeof(stack_t) ];                          // OK: automated - stack gets in .BSS section

public:
  IdleTask()
  {
    // TODO: this needs to be automated
    task.stackTop       = (stack_t)(&stack[stackSizeInBytes]);       // OK: automated
    task.msToPreemption = 1;                                         // TODO: this is only here when Round Robin is used
    task.stackSize      = stackSizeInBytes;                          // OK: automated
    task.currentState   = taskState_t::eReady;                       // OK: automated
    task.id             = System::getUniqueTaskID();                 // OK: automated
    task.priority       = 10;                                        // TODO: have not idea right now, but it will be solved along with msToPreemption
    task.entryPoint     = (entry_point_t)&IdleTask::run;             // OK: automated

    // TODO: debug
    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0x1D1E1D1E));

    System::registerTask(&task, true /* isIdle */);  // OK: automated
  }

  // Task body
  static void run()
  {
    while (true) {
      // TODO: It could be used as a idle counter
      //       or it could switch to lower power mode
    }
  }
};


static IdleTask _;  // OK: automated
