/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/gpio.h>
#include <drivers/stm32/usart.h>
#include <drivers/nvic.h>

#include <messageQueue.h>

#include "application_data.h"


void TaskDataLogger()
{
  // TODO: somehow this need to be moved outside the infinite look
  USART& usart = Application::data::usart1;
  GPIO& portA = Application::data::gpioA;
  MessageQueue<>& queue = Application::data::loggerQueue;

  // Start measure
  portA.low(GPIO::ePin8);

  // Block waiting for the message
  Message* message = queue.get();

  // Calculate length
  uint16_t length = 0;
  while (message->data[length++] != '\0') ;
  // Send it out..
  usart.send((const char*)message->data, length);

  // Message consumed
  queue.destroy(message);

  // Stop measure
  portA.high(GPIO::ePin8);
}

TASK_PERIODIC_EVERY_MS_OFFSET(TaskDataLogger, 10, 108);
