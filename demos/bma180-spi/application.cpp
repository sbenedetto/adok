
#include <application.h>

#include <drivers/nvic.h>
#include <drivers/stm32/afio.h>
#include <drivers/stm32/clock.h>
#include <drivers/stm32/externalInterrupt.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/usart.h>
#include <drivers/stm32/spi.h>

#include <sensors/bma180.h>

#include <messageQueue.h>


namespace Application
{
  // Define data shared among the tasks
  namespace data {
    GPIO gpioA(GPIO::ePortA);
    GPIO gpioC(GPIO::ePortC);
    USART usart1(USART::eUSART1);
    MessageQueue<> loggerQueue;
    SPI spi(SPI::eSPI1);
    BMA180 bma180(&spi);
    int8_t accZeroOffset[3];
  }

  void init()
  {
    /* Enable clock for peripherals used */
    clock::enableClockFor(clock::eSPI1);
    clock::enableClockFor(clock::eGPIOA);
    clock::enableClockFor(clock::eGPIOC);
    clock::enableClockFor(clock::eUSART1);

    // Set GPIOC::pin12 for measuring taskSensorReader
    GPIO& gpioPortC = data::gpioC;
    gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);
    gpioPortC.high(GPIO::ePin12);

    // Set GPIOA::pin8 for measureing taskDataLogger
    GPIO& gpioPortA = data::gpioA;
    gpioPortA.setMode(GPIO::ePin8, GPIO::eOutputPushPull2Mhz);
    gpioPortA.high(GPIO::ePin8);

    ///////////////////////////
    // Enable SPI controller //
    ///////////////////////////
    /* GPIO A - Configure SPI GPIO port pin to correct function */
    /* Clock */
    gpioPortA.setMode(GPIO::ePin5, GPIO::eAlternateFunctionPushPull10Mhz);
    /* MOSI */
    gpioPortA.setMode(GPIO::ePin7, GPIO::eAlternateFunctionPushPull10Mhz);
    /* MISO */
    gpioPortA.setMode(GPIO::ePin6, GPIO::eInputPullUpPullDown);

    // Configure SPI
    SPI& spi = data::spi;
    spi.clockPhaseSecondEdge();
    spi.clockPolarityIdleHigh();
    spi.dataFormatMSB();
    spi.dataFormat8();
    spi.masterMode();
    // Use GPIO Port C pin 10 as chip select
    spi.manageNSSByHardware(false);
    gpioPortC.setMode(GPIO::ePin10, GPIO::eOutputPushPull10Mhz);
    spi.fullDuplex();
    spi.baudRateControl(SPI::eFpclk256);
    // Finally enable SPI
    spi.enable();
    NVIC::enableIRQ(35);

    /***************************
      Enable USART for logging
    ****************************/

    /* Set GPIO alternate function for usart controller */
    clock::enableClockFor(clock::eGPIOA);
    gpioPortA.setMode(GPIO::ePin9, GPIO::eAlternateFunctionPushPull10Mhz);
    gpioPortA.setMode(GPIO::ePin10, GPIO::eInputPullUpPullDown);

    /* Enable USART controller */
    USART& usart = data::usart1;
    usart.enable();
    /* Set 8 bit word */
    usart.wordLength9();
    /* Set 1 stop bit */
    usart.setStopBits(USART::eStopBit1);
    /* Set parity bit */
    usart.parityControlEnable();
    usart.parityEven();
    /* Set baud rate: peripheral clock should be 72Mhz */
    uint16_t baudRate = 0x27A; // 115200 bps at 72Mhz
    usart.setBaudRate(baudRate);
    /* Set transmitter */
    usart.transmitterEnable();
    /* Enable interrupts */
    NVIC::enableIRQ(37);
 }
}
