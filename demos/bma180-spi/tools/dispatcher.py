import Queue
import threading
from PyQt4.Qt import QWidget

class Dispatcher(threading.Thread):

  def __init__(self, queue=None):
    threading.Thread.__init__(self)
    self.queue = queue

  def register(self, viewer):
    self.viewer = viewer

  def run(self):
    print "Dispatcher: running"
    self.keepRunning = True

    while (self.keepRunning):
      # Read the incoming message
      current_message = self.queue.get()
      if len(current_message) == 0:
          continue

      current_message = current_message.lstrip()

      # If first character is not a '#' the message contains
      # simple data
      if (current_message[0] != '#'):
        # Put character in TextView
        print "Log: ", current_message
      else:
        current_message = current_message.lstrip("#")
        # Split the message as needed
        # Message is formatted as "var=value var2=value2 ..."
        list_of_variables = current_message.rsplit()

        for variables in list_of_variables:
          separator = variables.rfind('=')
          name = variables[ : separator]
          value = variables[separator + 1 : ]
          # Very coupled...
          self.viewer.updateData(name, value)

  def stop(self):
    self.keepRunning = False

if __name__ == '__main__':
    pass
