import Queue
import serial
import threading
import time

class SerialReader(threading.Thread):

  def __init__(self, queue=None):
    threading.Thread.__init__(self)
    self.queue = queue
    # Set up serial port
    self.port = serial.Serial()
    self.port.port     = '/dev/ttyUSB0'
    self.port.baudrate = 115200
    self.port.bytesize = 8
    self.port.parity   = 'E'
    self.port.stopbits = 1

    self.port.open()
    self.port.flush()

  def run(self):
    print("SerialReader: running...")
    self.keepRunning = True

    while (self.keepRunning):
      # Read the incoming message
      message = self.port.readline()

      if (self.queue):
        # Delivery it to the given queue
        self.queue.put(message)
      else:
        print "SerialReader: ", message

    print("SerialReader: stopping...")
    #self.testFile.close()

  def stop(self):
    self.keepRunning = False

if __name__ == '__main__':
  reader = SerialReader()
  reader.start()
  reader.join()
