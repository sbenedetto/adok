//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <drivers/nvic.h>
#include <drivers/stm32/gpio.h>

#include <sensors/bma180.h>

#include <debug.h>
#include <messageQueue.h>
#include <semaphore.h>
#include <task.h>

#include <lib/utility/converter.h>

#include "application_data.h"

inline static
void
removeZeroOffset(int16_t* samples)
{
  samples[0] -= Application::data::accZeroOffset[0];
  samples[1] -= Application::data::accZeroOffset[1];
  samples[2] -= Application::data::accZeroOffset[2];
}


void TaskSensorReaderInit()
{
  // Start measure period
  GPIO& gpioPortC = Application::data::gpioC;
  gpioPortC.low(GPIO::ePin12);

  // Init sensor
  BMA180& bma180 = Application::data::bma180;
  // Bring the sensor to a known state
  bma180.softReset();
  bma180.disableI2C();
  bma180.init();

  while (true) {

    const int8_t chipID = 0x03;
    if (bma180.isOnBus(chipID))
      break;

    // else wait and retry
    NVIC::activeDelayUS(100);
  }

  // TODO: This should go to another task, to be executed in the initState (mode manager needed)

  // Calculate zero offset
  const uint8_t kSamples = 128;
  int32_t samples[3] = {0, 0, 0};
  int16_t  current[3];
  for (uint8_t i = 0; i < kSamples; i++) {
    bma180.readAccelerationData(current[0], current[1], current[2]);
    samples[0] += current[0];
    samples[1] += current[1];
    samples[2] += current[2];
    // There is not point in acquiring continously
    NVIC::activeDelayUS(500);
  }
  int8_t (&zeroOffset)[3] = Application::data::accZeroOffset;
  zeroOffset[0] = samples[0] >> 7;
  zeroOffset[1] = samples[1] >> 7;
  zeroOffset[2] = samples[2] >> 7;

  // Stop measure
  gpioPortC.high(GPIO::ePin12);
}


void TaskSensorReader()
{
  // Start measure period
  GPIO& gpioPortC = Application::data::gpioC;
  gpioPortC.low(GPIO::ePin12);

  // Put data into the logger task message queue
  MessageQueue<>& queue = Application::data::loggerQueue;
  Message* message = queue.create();
  if (message == NULL) {
    // Stop measure
    gpioPortC.high(GPIO::ePin12);
    return;
  }

   // Each acceleration sample is 12 bits.
  int16_t samples[3] = {0, 0, 0};
  // Sensor output data rate is 2400Hz.
  BMA180& bma180 = Application::data::bma180;
  bma180.readAccelerationData(samples[0], samples[1], samples[2]);
  // Remove offset
  removeZeroOffset(samples);

  /* At +-2g resolution one LSB is 0.00025 g */
  float ax = samples[0] * 0.00025;
  float ay = samples[1] * 0.00025;
  float az = samples[2] * 0.00025;

  // Copy data to buffer by hand for now...
  //sprintf((char *)message->data, "x=%d - y=%d - z=%d - t=%d\n", x, y, z, temperature);
  char* buffer = (char *)message->data;
  uint8_t endAt = utility::fromInt16ToASCII(buffer, samples[0]);
  buffer[endAt++] = ' ';
  endAt += utility::fromInt16ToASCII(buffer + endAt, samples[1]);
  buffer[endAt++] = ' ';
  endAt += utility::fromInt16ToASCII(buffer + endAt, samples[2]);
  buffer[endAt++] = '\n';
  buffer[endAt] = '\0';
  queue.put(message);

  // Stop measure
  gpioPortC.high(GPIO::ePin12);
}


TASK_PERIODIC_EVERY_MS_OFFSET_WITH_INIT(TaskSensorReader, 10, 105, TaskSensorReaderInit);
