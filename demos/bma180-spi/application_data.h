
#include <drivers/stm32/usart.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/spi.h>

#include <sensors/bma180.h>

#include <messageQueue.h>

namespace Application
{
  namespace data
  {
    extern SPI spi;
    extern USART usart1;
    extern GPIO gpioA;
    extern GPIO gpioC;
    extern BMA180 bma180;
    extern MessageQueue<> loggerQueue;
    extern int8_t accZeroOffset[3];
  }
}
