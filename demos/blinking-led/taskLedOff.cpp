//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <kernel/task_builder.h>

#include <drivers/stm32/gpio.h>


void TaskLedOFF()
{
  GPIO gpio(GPIO::ePortC);
  // Led OFF
  gpio.high(GPIO::ePin12);
}

// Execute the above task every 10ms
TASK_PERIODIC_EVERY_MS_OFFSET(TaskLedOFF, 10, 1);
