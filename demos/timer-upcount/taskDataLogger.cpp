/*
 * This file is part of the adok project.
 *
 * Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include <task.h>
#include <system.h>
#include <debug.h>

#include <drivers/stm32/usart.h>
#include <drivers/nvic.h>

#include <messageQueue.h>

#include "application_data.h"

class DataLoggerTask
{
private:
  Task          task;
  uint32_t      stackSizeInBytes = 1024;
  stack_t       stack[ 1024 / sizeof(stack_t) ];

public:
  DataLoggerTask()
  {
    task.stackTop       = (stack_t)(&stack[ stackSizeInBytes / sizeof(stack_t) ]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.msToPreemption = 1;
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&DataLoggerTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xCA05CA05));

    System::registerTask(&task);
  }

  static void run()
  {
      USART& usart = Application::data::usart1;
      MessageQueue<>& queue = Application::data::loggerQueue;

      // TODO: hmmm... 
      NVIC::enableIRQ(37);

      while (true) {

        // Block waiting for the message
        Message* message = queue.get();

        // Send it out..
        usart.send((const char*)message->data, sizeof(message->data));

        // Message consumed
        queue.destroy(message);
      }
    }
};


static DataLoggerTask _;
