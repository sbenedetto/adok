//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <drivers/nvic.h>
#include <drivers/stm32/timer.h>
#include <drivers/stm32/clock.h>

#include <debug.h>
#include <messageQueue.h>
#include <semaphore.h>
#include <task.h>
#include <system.h>

#include <lib/utility/converter.h>

#include "application_data.h"

class TaskTimerReader
{
private:
  Task          task;
  uint32_t      stackSizeInBytes = 1024;
  stack_t       stack[1024 / sizeof(stack_t)];

public:
  TaskTimerReader()
  {
    task.stackTop       = (stack_t)(&stack[ stackSizeInBytes / sizeof(stack_t) ]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.msToPreemption = 1;
    task.priority       = 10;
    task.entryPoint     = (entry_point_t)&TaskTimerReader::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xCA05CA05));

    System::registerTask(&task);
  }

  static void run() {

    MessageQueue<>& queue = Application::data::loggerQueue;

    clock::enableClockFor(clock::eTimer2);

    Timer timer(Timer::eTimer2);
    // Start the timer what will be used to keep track
    // of the time for calculating the absolute deadline
    // of each periodic task
    // The timer (16bit) will be counting at each 0.5 ms
    timer.setPrescaler(72000 >> 1);
    timer.enableOnePulseMode(false);
    timer.setCountingDirection(Timer::eCountup);
    timer.enableAutoReload(true);
    timer.setAutoReload(0xFFFF);
    timer.enable();


    // Main loop
    while (1) {

      // Put data into the logger task message queue
      Message* message = queue.create();
      if (message == NULL)
        continue;

      // Read timer value
      uint16_t value = timer.currentValue();

      // Prepare message in ASCII
      char* buffer = (char *)message->data;
      uint8_t endAt = utility::fromUInt16ToASCII(buffer, value);
      buffer[endAt++] = '\n';
      buffer[endAt] = '\0';

      queue.put(message);

    }
  }
};

static TaskTimerReader _;
