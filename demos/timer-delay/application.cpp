#include <application.h>

#include <drivers/stm32/clock.h>
#include <drivers/stm32/gpio.h>

void
Application::init()
{
  /* Enable clock for the GPIO Port C in order to use it */
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eTimer3);

  // Set up pin 12 of GPIO Port C in push pull mode
  GPIO gpio(GPIO::ePortC);
  gpio.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);
}
