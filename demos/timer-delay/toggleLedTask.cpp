//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include <system.h>
#include <task.h>

#include <drivers/nvic.h>
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/timer.h>

#include <debug.h>


class ToggleLedTask
{
public:
  // Task Control Block
  Task        task;
  uint32_t    stackSizeInBytes = 1024;
  stack_t     stack[ 1024 / sizeof(stack_t) ];

  ToggleLedTask()
  {
    // TODO: this needs to be automated
    task.stackTop       = (stack_t)(&stack[ stackSizeInBytes / sizeof(stack_t) ]);
    task.stackSize      = stackSizeInBytes;
    task.id             = System::getUniqueTaskID();
    task.type           = taskType_t::ePeriodic;

    task.currentState   = taskState_t::eReady;
    task.msToPreemption = 10;
    task.type           = taskType_t::ePeriodic;
    task.priority       = 10; // TODO
    task.entryPoint     = (entry_point_t)&ToggleLedTask::run;

    IF_DEBUG_ON(adok::fillUpStack(task.stackTop, task.stackSize, 0xA1A1A1A1));

    System::registerTask(&task);
  }

  // Task body
  static void run()
  {
    GPIO gpio(GPIO::ePortC);
    Timer timer(Timer::eTimer3);

    // TODO: enable timer interrupt
    timer.enableInterrupt();
    //NVIC::enableIRQ(28); // timer 2
    NVIC::enableIRQ(29);   // timer 3

    timer.enable();

    while (true) {
      // Led ON
      gpio.low(GPIO::ePin12);
      timer.delayms(2);

      gpio.high(GPIO::ePin12);
      timer.delayms(2);

      gpio.low(GPIO::ePin12);
      timer.delayus(20);

      gpio.high(GPIO::ePin12);
      timer.delayus(20);
    }
  }
};

ToggleLedTask task1;
