/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

void
nmi_handler()
{
  while(1);
}


void
memory_management_handler()
{
  while(1);
}


void
bus_fault_handler()
{
  while(1);
}


void
usage_fault_handler()
{
  while(1);
}


void
debug_monitor_handler()
{
  while(1);
}


void
window_watchdog_handler()
{
  while(1);
}


void
pvd_handler()
{
  while(1);
}


void
tamper_handler()
{
  while(1);
}


void
real_time_clock_handler()
{
  while(1);
}


void
flash_global_handler()
{
  while(1);
}


void
rcc_handler()
{
  while(1);
}


void
external_interrupt_line0_handler()
{
  while(1);
}


void
external_interrupt_line1_handler()
{
  while(1);
}


void
external_interrupt_line2_handler()
{
  while(1);
}


void
external_interrupt_line3_handler()
{
  while(1);
}


void
external_interrupt_line4_handler()
{
  while(1);
}


void
dma1_channel1_handler()
{
  while(1);
}


void
dma1_channel2_handler()
{
  while(1);
}


void
dma1_channel3_handler()
{
  while(1);
}


void
dma1_channel4_handler()
{
  while(1);
}


void
dma1_channel5_handler()
{
  while(1);
}


void
dma1_channel6_handler()
{
  while(1);
}


void
dma1_channel7_handler()
{
  while(1);
}


void
adc_1_and_2_handler()
{
  while(1);
}


void
usb_high_priority_or_can_tx_handler()
{
  while(1);
}


void
usb_low_priority_or_can_rx0_handler()
{
  while(1);
}


void
can_rx1_handler()
{
  while(1);
}


void
can_sce_handler()
{
  while(1);
}


void
external_interrupt_line5_to_9_handler()
{
  while(1);
}


void
timer1_break_handler()
{
  while(1);
}


void
timer1_update_handler()
{
  while(1);
}


void
timer1_trigger_communication_handler()
{
  while(1);
}


void
timer1_capture_compare_handler()
{
  while(1);
}


void
external_interrupt_line10_to_15_handler()
{
  while(1);
}


void
real_time_clock_alarm_handler()
{
  while(1);
}


void
usb_wakeup_handler()
{
  while(1);
}


void
timer8_break_handler()
{
  while(1);
}


void
timer8_update_handler()
{
  while(1);
}


void
timer8_trigger_communication_handler()
{
  while(1);
}


void
timer8_capture_compare_handler()
{
  while(1);
}


void
adc3_global_handler()
{
  while(1);
}


void
fsmc_global_handler()
{
  while(1);
}


void
sdio_global_handler()
{
  while(1);
}


void
timer6_global_handler()
{
  while(1);
}


void
timer7_global_handler()
{
  while(1);
}


void
dma2_channel1_handler()
{
  while(1);
}


void
dma2_channel2_handler()
{
  while(1);
}


void
dma2_channel3_handler()
{
  while(1);
}


void
dma2_channel4_5_handler()
{
  while (1);
}

