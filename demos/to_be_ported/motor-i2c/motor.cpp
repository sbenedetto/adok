#include "motor.h"

#include <bus/i2c.h>

Motor::Motor(I2C* i2c)
  : _i2c(i2c)
{
}


void
Motor::setAddress(uint8_t address)
{
  _i2cAddress = address;
}


bool
Motor::run(uint8_t speed)
{
  return _i2c->writeTo(_i2cAddress, speed, (const int8_t *)&speed, 0);
}
