#include <stdio.h>
#include <stdint.h>

#include <afio.h>
#include <bus/i2c.h>
#include <clock.h>
#include <externalInterrupt.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

#include "motor.h"

bool buttonPressed = false;

// Interrupt handler for push button
void external_interrupt_line0_handler()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine0);

  buttonPressed = true;
}

void external_interrupt_line1_handler()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine1);

static uint16_t happened = 0;
  happened++;
}

void external_interrupt_line2_handler()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine2);

static uint16_t plus = 0;
  plus++;
}

extern void low_level_init();
extern void trace(const char* buffer);

int main(void)
{
  low_level_init();

  trace("\n\n\n===== MOTOR DEMO APPLICATION =====\n");

  /* Enable clock for peripherals used */
  clock::enableClockFor(clock::eI2C2);
  // Push button
  clock::enableClockFor(clock::eGPIOA);
  // I2C2 SCK=PB10 SDA=PB11
  clock::enableClockFor(clock::eGPIOB);
  // Status LED
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eAFIO);

  NVIC nvic;
  nvic.enableSysTick();

  /* User button is on PA0, thus EXTI0 */
  /* Configure pin0 as input */
  GPIO gpioPortA(GPIO::ePortA);
  gpioPortA.setMode(GPIO::ePin0, GPIO::eInputPullUpPullDown);
  /*
   * Since EXTI0 is shared among all pins 0 of all GPIO ports,
   * we need to select port A as source in the multiplexer
   * through the AFIO register
   */
  clock::enableClockFor(clock::eAFIO);
  afio::selectSourceForEvent(afio::eEXTI0, afio::ePortA);
  /* Enable the interrupt on line 0 for falling edge */
  ExternalInterrupt exti;
  exti.interruptUnmask(ExternalInterrupt::eLine0);
  exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine0);
  // Enabled interrupt for push button 
  // (6 is the interrupt line number for EXT0)
  nvic.enableIRQ(6);

  /* Set up pin 12 of GPIO Port C in push pull mode */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

  /* Configure GPIO pin for I2C2 controller */
  GPIO gpioPortB(GPIO::ePortB);
  /* SCL */
  gpioPortB.setMode(GPIO::ePin10, GPIO::eAlternateFunctionOpenDrain2Mhz);
  /* SDA */
  gpioPortB.setMode(GPIO::ePin11, GPIO::eAlternateFunctionOpenDrain2Mhz);

  /* Configure I2C */
  I2C i2c(I2C::eI2C2);
  i2c.enableAcknowledge();
  i2c.setMode(I2C::eStandard10Khz);
  i2c.enable();

  /* DEBUGONLY */
  // PA1 interrupt
  gpioPortA.setMode(GPIO::ePin1, GPIO::eInputPullUpPullDown);
  afio::selectSourceForEvent(afio::eEXTI1, afio::ePortA);
  exti.interruptUnmask(ExternalInterrupt::eLine1);
  exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine1);
  nvic.enableIRQ(7);

  // PC2 interrupt
  gpioPortC.setMode(GPIO::ePin2, GPIO::eInputPullUpPullDown);
  afio::selectSourceForEvent(afio::eEXTI2, afio::ePortC);
  exti.interruptUnmask(ExternalInterrupt::eLine2);
  exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine2);
  nvic.enableIRQ(8);

  Motor motor(&i2c);
  // Let the motor start up
  gpioPortC.low(GPIO::ePin12);
  util::delayms(5*1000);

  // Turn off LED
  gpioPortC.high(GPIO::ePin12);

  char bufferOut[128];
  uint8_t addresses[4] = { 0x52, 0x54, 0x56, 0x5A };
  uint8_t i = 0;
  uint8_t speed = 0;

  while (1) {

      uint8_t address = addresses[i++ % 4];
      //speed++;
      speed = 50;
      sprintf(bufferOut, "main: running slave 0x%X at %u\n", address, speed);
      trace(bufferOut);
      motor.setAddress(address);
      motor.run(speed);

      gpioPortC.toggle(GPIO::ePin12);
      util::delayms(10);
  }

  return 0;
}
