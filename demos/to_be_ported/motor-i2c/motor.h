#include <stdint.h>

class I2C;

class Motor
{
  public:
    Motor(I2C* i2c);

    void setAddress(uint8_t i2cAddress);

    bool run(uint8_t speed);

  private:
    I2C*     _i2c;
    uint8_t  _i2cAddress;
};
