/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <clock.h>
#include <gpio.h>
#include <usart.h>

#include <stdint.h>

static USART* gMedia;

void low_level_init()
{
}


void trace(const char* buffer, uint16_t bufferLength)
{
    // TODO: global object initialization required!
    USART usart(USART::eUSART1);
    for (uint16_t i = 0; i < bufferLength; i++)
      usart << uint8_t(buffer[i]);
}


void trace(const char* buffer)
{
    // TODO: global object initialization required!
    USART usart(USART::eUSART1);
    for (uint16_t i = 0; buffer[i] != '\0'; i++)
      usart << uint8_t(buffer[i]);
}


uint8_t itostr(int16_t x, char* output)
{
  if (x < 0) {
    output[0] = '-';
    x = -x;
  } else if (x > 0) {
    output[0] = '+';
  } else {
    output[0] = '0';
    output[1] = '\0';
    return 1;
  }

  uint16_t divs[] = { 10 * 1000, 1000, 100, 10, 1, 0 };
  uint8_t currentDigit = 0;
  uint8_t i, j;
  for (i = 0, j = 1; divs[i] != 0; i++) {

    currentDigit = (x / divs[i]);
    /* We don't want to rule out zeros unless it's
     * the first digit.
     * Ex:
     *  - 543 would yell two zeros first, which we do not want
     *  - 10301 would yell two zeros too, but we want those
     */
    if (currentDigit > 0 || j > 1) {

      output[j++] = currentDigit + '0';
      x -= (currentDigit * divs[i]);
    }
  }
  output[j] = '\0';
  return j;
}
