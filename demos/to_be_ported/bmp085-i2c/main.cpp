
#include <bus/i2c.h>
#include <clock.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

#include <stdio.h>

#include "bmp085.h"

extern void low_level_init();
extern void trace(const char* buffer);

int main(void)
{
  low_level_init();

  trace("\n\n\n===== BMP085 DEMO APPLICATION =====\n");

  clock::enableClockFor(clock::eI2C1);
  clock::enableClockFor(clock::eGPIOB);
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eAFIO);

  /* GPIO C - Configure status led */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /* GPIO B - Configure I2C1 GPIO port pin to correct function */
  GPIO gpioPortB(GPIO::ePortB);
  /* SCL */
  gpioPortB.setMode(GPIO::ePin6, GPIO::eAlternateFunctionOpenDrain2Mhz);
  /* SDA */
  gpioPortB.setMode(GPIO::ePin7, GPIO::eAlternateFunctionOpenDrain2Mhz);

  /* Configure I2C1 */
  I2C i2c(I2C::eI2C1);
  i2c.enableAcknowledge();
  i2c.setMode(I2C::eStandard100Khz);
  i2c.enable();

  NVIC nvic;
  nvic.enableSysTick();

  /* Led off */
  gpioPortC.high(GPIO::ePin12);

  char bufferOut[256];

  uint8_t chipID = 0;
  BMP085 bmp085(&i2c);
  bmp085.init(&chipID, BMP085::eUltraLowPower);
  if (chipID != 0x55) {
    sprintf(bufferOut, "BMP085 not found... found ID=0x%x\n", chipID);
    trace(bufferOut);

    while(1) {
      gpioPortC.toggle(GPIO::ePin12);
      util::delayms(1000);
    }
  } else {
    trace("BMP085 found...\n");
    bmp085.initCalibrationData();
  }

  float altitude = 0;
  int32_t pressure = 0;
  int32_t temperature = 0;
  while (1) {

    bmp085.readPressureAndTemperature(&pressure, &temperature);
    altitude = bmp085.calculateAltitude(pressure);

    sprintf(bufferOut, "#altitude=%f pressure=%d temperature=%d\n", altitude, pressure, temperature);
    trace(bufferOut);

    gpioPortC.toggle(GPIO::ePin12);
    util::delayms(100);
  }

  return 0;
}
