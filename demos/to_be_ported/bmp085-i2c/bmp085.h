#include <stdint.h>

class I2C;


class BMP085
{
public:

  enum eMode {
    eUltraLowPower = 0,
    eStandard,
    eHighResolution,
    eUltraHighResolution
  };

  BMP085(I2C* i2c);

  void     init(uint8_t* chipID, eMode mode);
  void     initCalibrationData();

  float    calculateAltitude(const int32_t& pressure);

  void     readPressureAndTemperature(int32_t* pressure, int32_t* temperature);

private:

  BMP085();
  BMP085(const BMP085&);

  int8_t    readRegister(const uint8_t address);
  void      writeRegister(const uint8_t address, const int8_t value);

  I2C* i2c_;
  eMode mode_;
};
