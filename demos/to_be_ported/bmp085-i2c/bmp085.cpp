#include "bmp085.h"

#include <bus/i2c.h>
#include <utility/delay.h>

#include <math.h>

// Calibration Coefficients addresses
// AC1
static const uint8_t gkAC1MSB = 0xAA;
static const uint8_t gkAC1LSB = 0xAB;
// AC2
static const uint8_t gkAC2MSB = 0xAC;
static const uint8_t gkAC2LSB = 0xAD;
// AC3
static const uint8_t gkAC3MSB = 0xAE;
static const uint8_t gkAC3LSB = 0xAF;
// AC4
static const uint8_t gkAC4MSB = 0xB0;
static const uint8_t gkAC4LSB = 0xB1;
// AC5
static const uint8_t gkAC5MSB = 0xB2;
static const uint8_t gkAC5LSB = 0XB3;
// AC6
static const uint8_t gkAC6MSB = 0xB4;
static const uint8_t gkAC6LSB = 0xB5;
// B1
static const uint8_t gkB1MSB  = 0xB6;
static const uint8_t gkB1LSB  = 0xB7;
// B2
static const uint8_t gkB2MSB  = 0xB8;
static const uint8_t gkB2LSB  = 0xB9;
// MB
static const uint8_t gkMBMSB  = 0xBA;
static const uint8_t gkMBLSB  = 0xBB;
// MC
static const uint8_t gkMCMSB  = 0xBC;
static const uint8_t gkMCLSB  = 0xBD;
// MD
static const uint8_t gkMDMSB  = 0xBE;
static const uint8_t gkMDLSB  = 0xBF;

// Chip ID and Version
static const uint8_t gkChipID  = 0xD0;
static const uint8_t gkVersion = 0xD1; 
// Control register
static const uint8_t gkControl = 0xF4;
// Data register
static const uint8_t gkDataMSB = 0xF6;
static const uint8_t gkDataLSB = 0xF7;


static void compansateValues(int32_t* pressure, int32_t* temperature);


// Calibration coeffients
static int16_t  ac1;
static int16_t  ac2;
static int16_t  ac3;
static uint16_t ac4;
static uint16_t ac5;
static uint16_t ac6;
static int16_t  b1;
static int16_t  b2;
static int16_t  mb;
static int16_t  mc;
static int16_t  md;

const static uint8_t gkBMP085ReadAddress  = 0xEF;
const static uint8_t gkBMP085WriteAddress = 0xEE;

const static uint8_t gkTemperatureCommand = 0x2E;
const static uint8_t gkPressureCommand    = 0x34;

BMP085::BMP085(I2C* i2c)
  : i2c_(i2c)
{
}


void
BMP085::init(uint8_t* chipID, eMode mode)
{
  mode_ = mode;
  *chipID = readRegister(gkChipID);
}


float
BMP085::calculateAltitude(const int32_t& pressure)
{
  // Formula given in the datasheet is
  // altiture = (44330 * (1- (p/p0)^(1/5.255)))
  // with p0 being 1013.12 hPA at sea level
  float a = pressure / 101312;
  float b = pow(a, 0.190295); // 1/5.255
  float c = 44330 * (1 - b);
  return c;
}


void
BMP085::initCalibrationData(void)
{
  // AC1
  ac1 = readRegister(gkAC1MSB) << 8;
  ac1 |= readRegister(gkAC1LSB);
  // AC2
  ac2 = readRegister(gkAC2MSB) << 8;
  ac2 |= readRegister(gkAC2LSB);
  // AC3
  ac3 = readRegister(gkAC3MSB) << 8;
  ac3 |= readRegister(gkAC3LSB);
  // AC4
  ac4 = readRegister(gkAC4MSB) << 8;
  ac4 |= readRegister(gkAC4LSB);
  // AC5
  ac5 = readRegister(gkAC5MSB) << 8;
  ac5 |= readRegister(gkAC5LSB);
  // AC6
  ac6 = readRegister(gkAC6MSB) << 8;
  ac6 |= readRegister(gkAC6LSB);
  // B1
  b1 = readRegister(gkB1MSB) << 8;
  b1 |= readRegister(gkB1LSB);
  // B2
  b2 = readRegister(gkB2MSB) << 8;
  b2 |= readRegister(gkB2LSB);
  // MB
  mb = readRegister(gkMBMSB) << 8;
  mb |= readRegister(gkMBLSB);
  // MC
  mc = readRegister(gkMCMSB) << 8;
  mc |= readRegister(gkMCLSB);
  // MD
  md = readRegister(gkMDMSB) << 8;
  md |= readRegister(gkMDLSB);
}

void
BMP085::readPressureAndTemperature(int32_t* pressure, int32_t* temperature)
{
  // TODO
  // Read the temperature first
  writeRegister(gkControl, gkTemperatureCommand);
  util::delayms(5);
  uint16_t tmp = 0;
  tmp = readRegister(gkDataMSB) << 8;
  tmp |= readRegister(gkDataLSB);
  *temperature = tmp;

  // Now ask for pressure by taking the mode into consideration
  uint8_t command = gkPressureCommand | (mode_ << 6);
  writeRegister(gkControl, command);
  // And now we wait...
  if (mode_ == eUltraLowPower)
    util::delayms(5);
  else if (mode_ == eStandard)
    util::delayms(8);
  else if (mode_ == eHighResolution)
    util::delayms(14);
  else // eUltraHighResolution
    util::delayms(26);

  tmp = readRegister(gkDataMSB) << 8;
  tmp |= readRegister(gkDataLSB);
  *pressure = tmp;

  compansateValues(pressure, temperature);
}


int8_t
BMP085::readRegister(const uint8_t address)
{
  int8_t value = 0;
  const uint8_t numberOfBytes = 1;

  i2c_->readFrom(gkBMP085ReadAddress,
                 gkBMP085WriteAddress,
                 address, &value, numberOfBytes);
  return value;
}


void
BMP085::writeRegister(const uint8_t address, const int8_t value)
{
  const uint8_t numberOfBytes = 1;
  i2c_->writeTo(gkBMP085WriteAddress, address, &value, numberOfBytes);
}


void
compansateValues(int32_t* pressure, int32_t* temperature)
{
  int32_t  x1, x2, x3, b3, b5, b6, p;
  uint32_t  b4, b7;
  uint8_t oss = 3;

  x1 = (*temperature - ac6) * ac5 >> 15;
  x2 = ((int32_t ) mc << 11) / (x1 + md);
  b5 = x1 + x2;
  *temperature = (b5 + 8) >> 4;

  b6 = b5 - 4000;
  x1 = (b2 * (b6 * b6 >> 12)) >> 11; 
  x2 = ac2 * b6 >> 11;
  x3 = x1 + x2;
  b3 = ((((int32_t )ac1 * 4 + x3) << oss) + 2) >> 2;
  x1 = ac3 * b6 >> 13;
  x2 = (b1 * (b6 * b6 >> 12)) >> 16;
  x3 = ((x1 + x2) + 2) >> 2;
  b4 = (ac4 * (uint32_t ) (x3 + 32768)) >> 15;
  b7 = ((uint32_t)*pressure - b3) * (50000 >> oss);
  p = b7 < 0x80000000 ? (b7 * 2) / b4 : (b7 / b4) * 2;

  x1 = (p >> 8) * (p >> 8);
  x1 = (x1 * 3038) >> 16;
  x2 = (-7357 * p) >> 16;
  *pressure = p + ((x1 + x2 + 3791) >> 4);
}
