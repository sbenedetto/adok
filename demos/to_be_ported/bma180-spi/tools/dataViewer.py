from PyQt4.Qt import *

class DataViewer(QWidget):

  def __init__(self, parent=None):
    super(DataViewer, self).__init__()

    # AX
    self.ax = QLabel()
    self.ax.setText(QString("ax: "))
    self.ax.show()

    self.axValue = QLabel()
    self.axValue.setText(QString.number(0))
    self.axValue.show()

    self.axLayout = QHBoxLayout()
    self.axLayout.addWidget(self.ax)
    self.axLayout.addWidget(self.axValue)

    # AY
    self.ay = QLabel()
    self.ay.setText(QString("ay: "))
    self.ay.show()

    self.ayValue = QLabel()
    self.ayValue.setText(QString.number(0))
    self.ayValue.show()

    self.ayLayout = QHBoxLayout()
    self.ayLayout.addWidget(self.ay)
    self.ayLayout.addWidget(self.ayValue)

    self.az = QLabel()
    self.az.setText(QString("az: "))
    self.az.show()

    self.azValue = QLabel()
    self.azValue.setText(QString.number(0))
    self.azValue.show()

    self.azLayout = QHBoxLayout()
    self.azLayout.addWidget(self.az)
    self.azLayout.addWidget(self.azValue)

    self.mainLayout = QVBoxLayout()
    self.mainLayout.addLayout(self.axLayout)
    self.mainLayout.addLayout(self.ayLayout)
    self.mainLayout.addLayout(self.azLayout)

    self.setLayout(self.mainLayout)

  def updateData(self, variable, value):
    if (variable == "ax"):
      self.axValue.setText(QString(value))
    elif (variable == "ay"):
      self.ayValue.setText(QString(value))
    elif (variable == "az"):
      self.azValue.setText(QString(value))
    else:
      print variable, " ", value
