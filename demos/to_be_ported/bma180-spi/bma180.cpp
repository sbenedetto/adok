/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bma180.h"

#include <gpio.h>
#include <utility/delay.h>
#include <bus/spi.h>

static const uint8_t gkChipID           = 0x00;
static const uint8_t gkVersion          = 0x01;
static const uint8_t gkXLSB             = 0x02;
static const uint8_t gkXMSB             = 0x03;
static const uint8_t gkYLSB             = 0x04;
static const uint8_t gkYMSB             = 0x05;
static const uint8_t gkZLSB             = 0x06;
static const uint8_t gkZMSB             = 0x07;
static const uint8_t gkTemperature      = 0x08;
static const uint8_t gkStatusRegister1  = 0x09;
static const uint8_t gkStatusRegister2  = 0x0A;
static const uint8_t gkStatusRegister3  = 0x0B;
static const uint8_t gkStatusRegister4  = 0x0C;
static const uint8_t gkControlRegister0 = 0x0D;
static const uint8_t gkControlRegister1 = 0x0E;
static const uint8_t gkControlRegister2 = 0x0F;
static const uint8_t gkReset            = 0x10;
static const uint8_t gkBandwidth        = 0x20;
static const uint8_t gkControlRegister3 = 0x21;
static const uint8_t gkDisableI2C       = 0x27;
static const uint8_t gkModeConfig       = 0x30;
static const uint8_t gkOffsetXAndRange  = 0x35;


BMA180::BMA180(SPI* spi)
  : spi_(spi)
{
}


void
BMA180::init(uint8_t& chipID)
{
  chipID = readRegister(gkChipID);
  // Set range to +-2g
  uint8_t range = readRegister(gkOffsetXAndRange);
  range |= 0x04;
  writeRegister(gkOffsetXAndRange, range);
  // Set bandwidth to 20Hz
  uint8_t bandwidth = readRegister(gkBandwidth);
  bandwidth &= ~(0xF0);
  bandwidth |= 0x10;
  writeRegister(gkBandwidth, bandwidth);
  // Set mode_config for low noise mode (Date rate 2400Hz)
  uint8_t modeConfig = readRegister(gkModeConfig);
  modeConfig &= ~(0x03);
  writeRegister(gkModeConfig, modeConfig);
}


void
BMA180::disableI2C()
{
  uint8_t value = readRegister(gkDisableI2C);
  value |= 0x01;
  writeRegister(gkDisableI2C, value);
}


void
BMA180::enableNewDataInterrupt()
{
  uint8_t value = readRegister(gkControlRegister3);
  value |= 0x12;
  writeRegister(gkControlRegister3, value);
}


bool
BMA180::newDataInterruptEnabled()
{
  uint8_t value = 0;
  value = readRegister(gkControlRegister3);
  return value & 0x02;
}


void
BMA180::readAccelerationData(int16_t& x, int16_t& y, int16_t& z)
{
  int16_t tmp = 0;
  int16_t value = 0;

  tmp = readRegister(gkXLSB);
  value = (tmp >> 2) & 0x3F;
  tmp = readRegister(gkXMSB);
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000; 
  x = value;

  value = 0;
  tmp = readRegister(gkYLSB);
  value = (tmp >> 2) & 0x3F;
  tmp = readRegister(gkYMSB);
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000;
  y = value;

  value = 0;
  tmp = readRegister(gkZLSB);
  value = (tmp >> 2) & 0x3F;
  tmp = readRegister(gkZMSB);
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000;
  z = value;
}


void
BMA180::readTemperature(int8_t& temperature)
{
  temperature = readRegister(gkTemperature);
}


void
BMA180::softReset()
{
  writeRegister(gkReset, 0xB6); /* Special value to write in order to reset */
  util::delayms(10);
}


int8_t
BMA180::readRegister(uint8_t address)
{
  uint8_t value = 0;
  // Low CS
  GPIO portC(GPIO::ePortC);
  portC.low(GPIO::ePin10);
  util::delayus(1);

  *spi_ << (uint8_t)(0x80 | address);
  util::delayms(1);
  *spi_ >> value;

  // High CS
  util::delayus(1);
  portC.high(GPIO::ePin10);
  return static_cast<int8_t>(value);
}


void
BMA180::writeRegister(uint8_t address, int8_t value)
{
  // Low CS
  GPIO portC(GPIO::ePortC);
  portC.low(GPIO::ePin10);
  util::delayus(1);

  *spi_ << address;
  util::delayms(1);
  *spi_ << static_cast<uint8_t>(value);

  // High CS
  util::delayus(1);
  portC.high(GPIO::ePin10);
}
