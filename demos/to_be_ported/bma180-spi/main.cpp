/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <afio.h>
#include <bus/spi.h>
#include <clock.h>
#include <externalInterrupt.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

#include <stdio.h>

#include "bma180.h"


/* These are shared among interrupt routines and main loop */
static volatile bool gNewDataReady = false;
static volatile uint32_t gNewDataCounter = 0;

extern void low_level_init();
extern void trace(const char* buffer);
extern void trace(const char* buffer, uint16_t bufferLength);
extern int8_t itostr(int16_t x, char* buffer);

/* GPIO C Port 2 is used a interrupt line for BMA180 */
void external_interrupt_line2()
{
  ExternalInterrupt exti;
  exti.clearPendingInterrupt(ExternalInterrupt::eLine2);

  /* Notify the main loop */
  gNewDataReady = true;
  gNewDataCounter++;
  /* Blink the status led */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.toggle(GPIO::ePin12);
}


int main(void)
{
  low_level_init();

  /* Enable clock for peripherals used */
  clock::enableClockFor(clock::eSPI1);
  clock::enableClockFor(clock::eGPIOA);
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eAFIO);

  /* GPIO C - Configure status led */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /* GPIO A - Configure SPI GPIO port pin to correct function */
  GPIO gpioPortA(GPIO::ePortA);
  /* Clock */
  gpioPortA.setMode(GPIO::ePin5, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MOSI */
  gpioPortA.setMode(GPIO::ePin7, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MISO */
  gpioPortA.setMode(GPIO::ePin6, GPIO::eInputPullUpPullDown);

  SPI spi(SPI::eSPI1);
  spi.clockPhaseSecondEdge();
  spi.clockPolarityIdleHigh();
  spi.dataFormatMSB();
  spi.dataFormat8();
  spi.masterMode();
  /* Use GPIO Port C pin 10 as chip select */
  spi.manageNSSByHardware(false);
  gpioPortC.setMode(GPIO::ePin10, GPIO::eOutputPushPull10Mhz);
  spi.fullDuplex();
  spi.baudRateControl(SPI::eFpclk256);
  /* Finally enable SPI */
  spi.enable();

  NVIC nvic;
  nvic.enableSysTick();

  /*
   * Configure interrupt mode for GPIO Port C pin 2.
   * It is used for new_data_int from sensor
   */
  ExternalInterrupt exti;
  exti.interruptUnmask(ExternalInterrupt::eLine2);
  exti.triggerFallingEdgeEnable(ExternalInterrupt::eLine2);
  exti.triggerRisingEdgeEnable(ExternalInterrupt::eLine2);
  afio::selectSourceForEvent(afio::eEXTI2, afio::ePortC);
  gpioPortC.setMode(GPIO::ePin2, GPIO::eInputPullUpPullDown);
  nvic.enableIRQ(8); /* EXTI2 */

  /* Led off */
  gpioPortC.high(GPIO::ePin12);

  /* Set GPIO Port C pin 3 as chip select */
  gpioPortC.setMode(GPIO::ePin3, GPIO::eOutputPushPull10Mhz);

  BMA180 bma180(&spi);

  /* Bring it to a known state */
  bma180.softReset();
  bma180.disableI2C();

  //while (!bma180.newDataInterruptEnabled())
    bma180.enableNewDataInterrupt();

  uint8_t chipID;
  util::delayms(1);
  bma180.init(chipID);
  if (chipID != 3) {
    /* Signal an error by keeping the led ON for 3 seconds */
    trace("ChipID not found!\n");
    while (1) {
      gpioPortC.low(GPIO::ePin12);
      util::delayms(3000);
      gpioPortC.low(GPIO::ePin12);
    }
  } else {
    /* Signal a success by blinking for the 3 seconds */
    int i = 30;
    trace("ChipID found...\n");
    while (i-- > 0) {
      gpioPortC.toggle(GPIO::ePin12);
      util::delayms(100);
    }
  }

  /* Each acceleration sample is 12 bits. */
  int16_t x = 0;
  int16_t y = 0;
  int16_t z = 0;
  int8_t temperature = 0;

  char bufferOut[80];
  while (1) {

    bma180.readAccelerationData(x, y, z);
    bma180.readTemperature(temperature);

    /* At +-2g resolution one LSB is 0.00025 g */
    float ax = x * 0.00025;
    float ay = y * 0.00025;
    float az = z * 0.00025;

    /* Temperature is givin in Kelvin. Resolution is 0.5 per LSB
     * 1 Kelvin = -272.15 Celsius */
    int16_t tempInCelsius = temperature;
    tempInCelsius = (tempInCelsius * 0.5);//- 272;

    sprintf(bufferOut, "#ax=%+f ay=%+f az=%+f\n", ax, ay, az);
    trace(bufferOut);

    gpioPortC.toggle(GPIO::ePin12);

    /* Sensor output data rate is 2400Hz. */
    /* We slow down a bit for the logger */
    util::delayms(200);

    /* Wait for the sensor to signal new data */
    //while (!gNewDataReady);
    gNewDataReady = false;
  }

  return 0;
}
