/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <clock.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

extern void low_level_init();
extern void trace(const char* buffer, uint16_t bufferLength);

int main(void)
{
  /* Set-up clocks, usart1 for logging */
  low_level_init();

  /* Set up pin 12 (User LED) of GPIO Port C in push pull mode */
  clock::enableClockFor(clock::eGPIOC);
  GPIO gpio(GPIO::ePortC);
  gpio.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

  /* Enable systick timer */
  NVIC nvic;
  nvic.enableSysTick();

  /* Send data */
  const char* dataOut = "abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ\n";
  uint16_t length = 0;
  while (dataOut[length++] != '\0')
    ;

  while (1) {
    /* Turn on/off the led */
    gpio.toggle(GPIO::ePin12);
    trace(dataOut, length);
    util::delayms(10);
  }

  return 0;
}
