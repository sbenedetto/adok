/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <afio.h>
#include <bus/spi.h>
#include <clock.h>
#include <externalInterrupt.h>
#include <gpio.h>
#include <nvic.h>


void speedUpClock()
{
  /* Speed up clock to 72Mhz. External clock is 8Mhz */
  clock::enableHighSpeedExternal();
  clock::setPLLMultiplierFactor(clock::eMultiplyBy9);
  clock::setPLLInput(clock::ePLLHighSpeedExternal);
  /* Set wait access state */
  *(volatile uint32_t *)0x40022000 |= 0x02;
  clock::enablePLL();
  clock::setSystemClockSource(clock::ePLL);
}


int main(void)
{
  speedUpClock();

  /* Enable clocks */
  clock::enableClockFor(clock::eGPIOA);
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eAFIO);
  clock::enableClockFor(clock::eSPI1);

  /* Enable led pin */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /* Scale clock on APB2 where SPI is */
  clock::setPrescalerForAPBHigh(clock::eAPBScaleBy8);

  /* Configure SPI GPIO port pin to correct function */
  GPIO gpioPortA(GPIO::ePortA);
  /* Clock */
  gpioPortA.setMode(GPIO::ePin5, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MOSI */
  gpioPortA.setMode(GPIO::ePin7, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MISO */
  gpioPortA.setMode(GPIO::ePin6, GPIO::eInputPullUpPullDown);
  /* NSS */
  gpioPortA.setMode(GPIO::ePin4, GPIO::eAlternateFunctionPushPull10Mhz);

  SPI spi(SPI::eSPI1);
  spi.clockPhaseFirstEdge();
  spi.clockPolarityIdleHigh();
  spi.dataFormatMSB();
  spi.dataFormat8();
  spi.masterMode();
  spi.manageNSSByHardware(true);
  spi.fullDuplex();
  spi.baudRateControl(SPI::eFpclk4);

  /* Finally enable SPI */
  spi.enable();

  /* Led off*/
  gpioPortC.high(GPIO::ePin12);

  while (1) {

    /* Write some data */
    uint8_t data = 0x00;
    spi << (uint8_t)(0x80);
    spi >> data;
    if (data == 0x80) {

       /* Turn led on */
      gpioPortC.low(GPIO::ePin12);

      for (int i = 0; i < 1 * 1000 * 1000; i++)
        asm("nop");
    }

    spi << (uint8_t)(0xA5);
    spi >> data;
    if (data == 0xA5) {

      /* Led off*/
      gpioPortC.high(GPIO::ePin12);

      for (int i = 0; i < 1 * 1000 * 1000; i++)
        asm("nop");
    }

  }

  return 0;
}
