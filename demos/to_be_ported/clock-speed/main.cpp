/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <clock.h>
#include <gpio.h>


int main(void)
{
  /* Enable clock for GPIO Port C */
  clock::enableClockFor(clock::eGPIOC);

  /* Set up pin 12 of GPIO Port C as push pull mode */
  GPIO gpio(GPIO::ePortC);
  gpio.setMode(GPIO::ePin12, GPIO::eOutputPushPull2Mhz);

  /* We start by running with the internal clock at 8Mhz */
  for (int j = 0; j < 10; j++) {

    /* Wait about a second */
    for (int i = 0; i < (100 * 1000); i++)
       asm("nop");

    /* Turn on led */
    gpio.high(GPIO::ePin12);

    /* Wait about a second */
    for (int i = 0; i < (100 * 1000); i++)
       asm("nop");

    /* Turn off led */
    gpio.low(GPIO::ePin12);
  }

  /* Now switch to 72 Mhz */

  /* Enable high-speed external oscillator */
  clock::enableHighSpeedExternal();

  /* External oscillator is 8Mhz */
  clock::ePLLSource src = clock::ePLLHighSpeedExternal;
  clock::setPLLInput(src);

  /* Multiply by 9 in order to get full speed (72Mhz) */
  clock::eMultiplier mult = clock::eMultiplyBy9;
  clock::setPLLMultiplierFactor(mult);

  clock::enablePLL();

  /* Set access wait state at 2 */
  *(volatile uint32_t *)0x40022000 |= 0x02;

  /* Set PLL as system clock */
  clock::setSystemClockSource(clock::ePLL);

  /* Wait untill PLL is used a system clock */
  while (clock::eClockSource source = clock::currentSystemClockSource())
    if (source == clock::ePLL)
      break;

  /* Now blink faster (x9 times faster) */
  while (1) {

    /* Wait about a second */
    for (int i = 0; i < (100 * 1000); i++)
       asm("nop");

    /* Turn on led */
    gpio.low(GPIO::ePin12);

    /* Wait about a second */
    for (int i = 0; i < (100 * 1000); i++)
       asm("nop");

    /* Turn off led */
    gpio.high(GPIO::ePin12);
  }

  return 0;
}
