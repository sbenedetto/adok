/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <clock.h>
#include <gpio.h>
#include <nvic.h>


int main(void)
{
  /* Enable clock for the GPIO Port C in order to use it */
  clock::enableClockFor(clock::eGPIOC);

  GPIO gpio(GPIO::ePortC);

  /* Led */
  gpio.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /* Set up pin 0 of GPIO Port C as output in push pull mode */
  gpio.setMode(GPIO::ePin0, GPIO::eOutputPushPull10Mhz);

  /* Set up pin 1 of GPIO Port C as input in push pull mode */
  gpio.setMode(GPIO::ePin1, GPIO::eInputPullUpPullDown);

  bool pinState = true;
  while (true) {

    /* We are running at 8Mhz. The following is about 1 second */
    for (int i = 0; i < (10 * 10000); i++)
        asm("nop");

    /* Low output */
    gpio.low(GPIO::ePin0);

    pinState = gpio.currentState(GPIO::ePin1);
    if (pinState == false)
      gpio.low(GPIO::ePin12);

    /* Wait about another second */
    for (int i = 0; i < (10 * 10000); i++)
       asm("nop");

    /* High output */
    gpio.high(GPIO::ePin0);

    pinState = gpio.currentState(GPIO::ePin1);
    if (pinState == true)
      gpio.high(GPIO::ePin12);
  }

  return 0;
}
