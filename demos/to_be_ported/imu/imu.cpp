/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "math.h"

#include <utility/delay.h>
#include <stdint.h>

float gPitch = 0;
float gRoll  = 0;
float gYaw   = 0;

/* Estimated values by using accelerometer and gyroscope data */
float estimatedX = 0.0;
float estimatedY = 0.0;
float estimatedZ = 0.0;

static bool gVeryFirstTime = true;
/* Vector compoments calculated using gyroscope data */
static float gyroscopeX = 0.0;
static float gyroscopeY = 0.0;
static float gyroscopeZ = 0.0;

static const float gkWeigth = 5;

static void normalize3(float& x, float& y, float& z);

void estimateOrientation(float& ax, float& ay, float& az,
                         const float& xz, const float& yz, const float& xy,
                         const float& delta)
{
  /* Calculate vector component from accelerometer data */
  normalize3(ax, ay, az);

  if (gVeryFirstTime) {
    estimatedX = ax;
    estimatedY = ay;
    estimatedZ = az;
    gVeryFirstTime = false;
    return;
  }

  /* Evaluate vector from gyroscope data */
  if ( -0.1 < estimatedZ && estimatedZ < 0.1) {
    /* The z component is too small, better use previous estimation,
     * otherwise it will simply amplify the error for pitch and roll */
    gyroscopeX = estimatedX;
    gyroscopeY = estimatedY;
    gyroscopeZ = estimatedZ;
  } else {

    /* Get angle displacement from gyroscope */
    uint64_t t = delta;
    float deltaPitch = (t * xz);
    float deltaRoll = (t * yz);
    float deltaYaw = (t * xy);

    /* Get angle change in degree. atan2() give result within (-pi, pi) */
    gPitch = atan2f(estimatedX, estimatedZ) * 180 / M_PI;
    gRoll = atan2f(estimatedY, estimatedZ) * 180 / M_PI;
    gYaw = atan2f(estimatedX, estimatedY) * 180 / M_PI;

    /* Update angle based on gyroscope readings */
    gPitch += deltaPitch;
    gRoll += deltaRoll;
    gYaw += deltaYaw;

    /* Estimate z component sign by looking in what quadrant pitch (xz) is */
    float zSign = (cos(gPitch * M_PI / 180) >= 0) ? 1 : -1;

    /* And now the fun part... Reverse calculate R components for gyroscope */
    gyroscopeX = sin(gPitch * M_PI / 180);
    gyroscopeX /= sqrt(1 + pow(cos(gPitch * M_PI / 180), 2) * pow(tan(gRoll * M_PI / 180), 2));

    gyroscopeY = sin(gRoll * M_PI / 180);
    gyroscopeY /= sqrt(1 + pow(cos(gRoll * M_PI / 180), 2) * pow(tan(gPitch * M_PI / 180), 2));

    gyroscopeZ = zSign * sqrt(1 - gyroscopeX * gyroscopeX - gyroscopeY * gyroscopeY);
  }

  /* Combine accelerometer data with gyroscope, hopefully getting better result */
  estimatedX = (ax + gkWeigth * gyroscopeX) / (1 + gkWeigth);
  estimatedY = (ay + gkWeigth * gyroscopeY) / (1 + gkWeigth);
  estimatedZ = (az + gkWeigth * gyroscopeZ) / (1 + gkWeigth);

  /* Normalize vector */
  normalize3(estimatedX, estimatedY, estimatedZ);

  return ;
}

void normalize3(float& x, float& y, float& z)
{
  float r = sqrt(x*x + y*y + z*z);
  x /= r;
  y /= r;
  z /= r;
}
