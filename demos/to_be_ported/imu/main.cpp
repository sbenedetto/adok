/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <afio.h>
#include <bus/i2c.h>
#include <bus/spi.h>
#include <clock.h>
#include <externalInterrupt.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

#include <stdio.h>
#include "math.h"

#include "bma180.h"
#include "l3g4200d.h"

extern void low_level_init();
extern void trace(const char* buffer);

void estimateOrientation(float& ax, float& ay, float& az,
                         const float& xz, const float& yz, const float& xy,
                         const float& delta);

/* Angles output from estimateOrientation function */
extern float gPitch;
extern float gRoll;
extern float gYaw;

/* Estimated accelerometer components values compensated with gyroscope data */
extern float estimatedX;
extern float estimatedY;
extern float estimatedZ;

/* Gyroscope zero-level shifting */
float gyroscopeXZ0 = 0;
float gyroscopeYZ0 = 0;
float gyroscopeXY0 = 0;


int main(void)
{
  low_level_init();
  trace("\n\n\n===== IMU APPLICATION =====\n");

  /* Enable clock for peripherals used */
  clock::enableClockFor(clock::eAFIO);
  clock::enableClockFor(clock::eI2C1);
  clock::enableClockFor(clock::eSPI1);
  clock::enableClockFor(clock::eGPIOA);
  clock::enableClockFor(clock::eGPIOB);
  clock::enableClockFor(clock::eGPIOC);

  /* GPIO C - Configure status led */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /*********************
   *   Configure I2C   *
   *********************/
  /* GPIO B - Configure I2C1 GPIO port pin to correct function */
  GPIO gpioPortB(GPIO::ePortB);
  /* SCL */
  gpioPortB.setMode(GPIO::ePin6, GPIO::eAlternateFunctionOpenDrain2Mhz);
  /* SDA */
  gpioPortB.setMode(GPIO::ePin7, GPIO::eAlternateFunctionOpenDrain2Mhz);

  I2C i2c(I2C::eI2C1);
  i2c.enableAcknowledge();
  i2c.setMode(I2C::eStandard100Khz);
  i2c.enable();

  /*********************
   *   Configure SPI   *
   *********************/
  /* GPIO A - Configure SPI GPIO port pin to correct function */
  GPIO gpioPortA(GPIO::ePortA);
  /* Clock */
  gpioPortA.setMode(GPIO::ePin5, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MOSI */
  gpioPortA.setMode(GPIO::ePin7, GPIO::eAlternateFunctionPushPull10Mhz);
  /* MISO */
  gpioPortA.setMode(GPIO::ePin6, GPIO::eInputPullUpPullDown);

  SPI spi(SPI::eSPI1);
  spi.clockPhaseSecondEdge();
  spi.clockPolarityIdleHigh();
  spi.dataFormatMSB();
  spi.dataFormat8();
  spi.masterMode();
  /* Use GPIO Port C pin 3 as chip select */
  spi.manageNSSByHardware(false);
  gpioPortC.setMode(GPIO::ePin10, GPIO::eOutputPushPull10Mhz);
  spi.fullDuplex();
  spi.baudRateControl(SPI::eFpclk256);
  /* Finally enable SPI */
  spi.enable();

  NVIC nvic;
  nvic.enableSysTick();

  /* Turn off status led */
  gpioPortC.high(GPIO::ePin12);

  BMA180 accelerometer(&spi);
  /* Bring it to a known state */
  accelerometer.softReset();
  accelerometer.disableI2C();
  /* Check if accelerometer is correctly connected */
  uint8_t chipID;
  util::delayms(1);
  accelerometer.init(chipID);
  if (chipID != 3) {
    /* Signal an error by blinking the led at interval of 3 seconds */
    trace("BMA180 chip ID not found!\n");
    while (1) {
      gpioPortC.low(GPIO::ePin12);
      util::delayms(3000);
      gpioPortC.low(GPIO::ePin12);
    }
  }
  trace("BMA180 chip ID found...\n");

  L3G4200D gyroscope(&i2c);
  chipID = 0;
  gyroscope.init(&chipID);
  if (chipID != 0xD3) {
    /* Signal an error by blinking the led at interval of 3 seconds */
    trace("L3G4200D chip ID not found!\n");
    while (1) {
      gpioPortC.low(GPIO::ePin12);
      util::delayms(3000);
      gpioPortC.low(GPIO::ePin12);
    }
  }
  trace("L3G4200D chip ID found...\n");

  trace("Start querying for data...\n");

  char bufferOut[256];
  const uint8_t kSamples = 200;
  int16_t sensorValue[3] = {0, 0, 0};
  /* Calculate zero-rate level for gyroscope */
  int32_t gyroAtZero[3] = {0, 0, 0};
  for (uint8_t i = 0; i < kSamples; i++) {
    gyroscope.readGyroscopeData(sensorValue[0], sensorValue[1], sensorValue[2]);
    gyroAtZero[0] += sensorValue[0];
    gyroAtZero[1] += sensorValue[1];
    gyroAtZero[2] += sensorValue[2];
    sprintf(bufferOut, "Gyroscope: zero level value: x = %d - y = %d - z = %d\n",
      gyroAtZero[0], gyroAtZero[1], gyroAtZero[2]);
    trace(bufferOut);
  }
  gyroAtZero[0] /= kSamples;
  gyroAtZero[1] /= kSamples;
  gyroAtZero[2] /= kSamples;
  sprintf(bufferOut, "Gyroscope: zero level value: x = %d - y = %d - z = %d\n",
    gyroAtZero[0], gyroAtZero[1], gyroAtZero[2]);
  trace(bufferOut);

  /* Values from accelerometer */
  float accelerometerValue[3] = {0.0, 0.0, 0.0};
  float& ax = accelerometerValue[0];
  float& ay = accelerometerValue[1];
  float& az = accelerometerValue[2];
  const float kAccNoise = 150;

  /* Values from gyroscope */
  float gyroscopeValue[3] = {0.0, 0.0, 0.0};
  const float kGyroThreshold = 100;
  float timeElapsed = 0;
  uint64_t previousTime = util::currentCounterValue();
  int32_t gyroPrevious[3];
  gyroPrevious[0] = gyroAtZero[0];
  gyroPrevious[1] = gyroAtZero[1];
  gyroPrevious[2] = gyroAtZero[2];

  while (1) {

    /* Accelerometer */
    accelerometer.readAccelerationData(sensorValue[0], sensorValue[1], sensorValue[2]);
    /* Convert data to g */
    /* At +-2g resolution one LSB is 0.00025 g */
    ax = (sensorValue[0] - kAccNoise) * 0.00025;
    ay = (sensorValue[1] - kAccNoise) * 0.00025;
    az = (sensorValue[2] - kAccNoise) * 0.00025;

    /* Gyroscope */
    gyroscope.readGyroscopeData(sensorValue[0], sensorValue[1], sensorValue[2]);

    uint64_t currentValue = util::currentCounterValue();
    uint32_t delta = currentValue - previousTime;
    timeElapsed = (float)delta / 1000000;
    previousTime = currentValue;

    /* Use reference for readibility. Compiler should get ride of them */
    int16_t &xz = sensorValue[0];
    int16_t &yz = sensorValue[1];
    int16_t &xy = sensorValue[2];
    int32_t &xz0 = gyroAtZero[0];
    int32_t &yz0 = gyroAtZero[1];
    int32_t &xy0 = gyroAtZero[2];
    int32_t &xzPrevious = gyroPrevious[0];
    int32_t &yzPrevious = gyroPrevious[1];
    int32_t &xyPrevious = gyroPrevious[2];
    float& gxz = gyroscopeValue[0];
    float& gyz = gyroscopeValue[1];
    float& gxy = gyroscopeValue[2];
    /* Convert data to dps */
    int32_t tmp = 0;
    if ((xz - xz0) > kGyroThreshold || (xz - xz0) < -kGyroThreshold) {
      tmp = xz;
      xz = (xz + xzPrevious) / 2;
      gxz = (xz - xz0) * 0.00875;
      xzPrevious = tmp;
    } else
      gxz = 0;

    if ((yz - yz0) > kGyroThreshold || (yz - yz0) < -kGyroThreshold) {
      tmp = yz;
      yz = (yz + yzPrevious) / 2;
      gyz = (yz - yz0) * 0.00875;
      yzPrevious = tmp;
    } else
      gyz = 0;

    if ((xy - xy0) > kGyroThreshold || (xy - xy0) < -kGyroThreshold) {
      tmp = xy;
      xy = (xy + xyPrevious) / 2;
      gxy = (xy - xy0) * 0.00875;
      xyPrevious = tmp;
    } else
      gxy = 0;

    /* Just for readability. */
    estimateOrientation(ax, ay, az, gxz, gyz, gxy, timeElapsed);

    sprintf(bufferOut,"#ax=%+f ay=%+f az=%+f ex=%+f ey=%+f ez=%+f "
            "pitch=%+f roll=%+f yaw=%+f\n",
            ax, ay, az, estimatedX, estimatedY, estimatedZ, gPitch, gRoll, gYaw);
    trace(bufferOut);

    gpioPortC.toggle(GPIO::ePin12);
  }

  return 0;
}
