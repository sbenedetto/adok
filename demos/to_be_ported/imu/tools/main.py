from dispatcher    import *
from plotter2D     import *
from plotter3D     import *
from textViewer    import *
from PyQt4.Qt      import *
from serialReader  import *
from dataViewer    import *

import Queue

# Create main window
app = QApplication([])

default_length = 500

# Serial reader
queue = Queue.Queue(0)
serialReader = SerialReader(queue)
dataDispatcher = Dispatcher(queue)

# Plot X
scope_x = Plot2d()
queue_ax = [0.0 for i in range(default_length)]
queue_ex = [0.0 for i in range(default_length)]
scope_x.attachSignal(label="ax", signal=queue_ax)
scope_x.attachSignal(label="ex", signal=queue_ex)
dataDispatcher.attachSignal(label="ax", signal=queue_ax)
dataDispatcher.attachSignal(label="ex", signal=queue_ex)
scope_x.show()

# Plot Y
scope_y = Plot2d()
queue_ay = [2.0 for i in range(default_length)]
queue_ey = [0.0 for i in range(default_length)]
scope_y.attachSignal(label="ay", signal=queue_ay)
scope_y.attachSignal(label="ey", signal=queue_ey)
dataDispatcher.attachSignal(label="ay", signal=queue_ay)
dataDispatcher.attachSignal(label="ey", signal=queue_ey)
scope_y.show()

# Plot Z
scope_z = Plot2d()
queue_az = [1.0 for i in range(default_length)]
queue_ez = [1.0 for i in range(default_length)]
scope_z.attachSignal(label="az", signal=queue_az)
scope_z.attachSignal(label="ez", signal=queue_ez)
dataDispatcher.attachSignal(label="az", signal=queue_az)
dataDispatcher.attachSignal(label="ez", signal=queue_ez)
scope_z.show()

# Layout for plotters
dummyWidget = QWidget()
scopes_layout = QVBoxLayout()
scopes_layout.addWidget(scope_x)
scopes_layout.addWidget(scope_y)
scopes_layout.addWidget(scope_z)
dummyWidget.setLayout(scopes_layout)

# 3D View
queue_pitch = [0.0, 0.0]
queue_roll = [0.0, 0.0]
scope_3D = Plot3D()
scope_3D.setPitch(queue_pitch)
scope_3D.setRoll(queue_roll)
dataDispatcher.attachSignal(label="pitch", signal=queue_pitch)
dataDispatcher.attachSignal(label="roll", signal=queue_roll)
scope_3D.show()

# Add scopes and 3D view next to each other
hlayout = QHBoxLayout()
# Put a splitter between 
vsplitter = QSplitter(Qt.Horizontal)
vsplitter.addWidget(dummyWidget)
vsplitter.addWidget(scope_3D)
vsplitter.show()
hlayout.addWidget(vsplitter)

# Log text viewer
log_text_viewer = QTextEdit()
log_text_viewer.setReadOnly(True)
dataDispatcher.setTextViewer(log_text_viewer)
log_text_viewer.show()

# Label dataviewer
dataViewer = DataViewer()
dataViewer.show()
dataDispatcher.register(dataViewer)

# Put a splitter between 
hsplitter = QSplitter(Qt.Vertical)
hsplitter.addWidget(vsplitter)
hsplitter.addWidget(dataViewer)
hsplitter.addWidget(log_text_viewer)
hsplitter.show()

mainLayout = QVBoxLayout()
mainLayout.addWidget(hsplitter)

# Show main window
window = QWidget()
window.setLayout(mainLayout)
window.show()

# Start threads
dataDispatcher.start()
serialReader.start()

app.exec_()
dataDispatcher.stop()
serialReader.stop()
dataDispatcher.join()
serialReader.join()
