from PyQt4.Qt import *

# The following are only used for generating testing waves
import numpy
import math
import random

# TODO: can I make a thread per Plot2d? Is it worth it?
class Plot2dArea(QWidget):

    def __init__(self, parent=None):
        super(Plot2dArea, self).__init__()

        # Create and attach a timer for periodic redraw
        self.timer = QTimer(self)
        self.connect(self.timer, SIGNAL("timeout()"), self.update)
        self.timer.setInterval(100)
        self.timer.start();

        # Scaling factor is signal dependent
        self.scalingFactor = 1
        self.offset = self.height()/2

        self.signals = {}
        self.signalAttached = False

    def resize(self, width, height):
        super(Plot2dArea, self).resize(width, height)
        self.offset = height/2
        if (self.signalAttached):
          self.updateScalingFactor()

    def resizeEvent(self, event):
        self.resize(event.size().width(), event.size().height())

    def zoomIn(self):
        self.scalingFactor += 10

    def zoomOut(self):
        self.scalingFactor -= 10

    def attachSignal(self, label, signal):
        self.signalAttached = True
        self.signals[label] = signal
        self.updateScalingFactor()

    def updateScalingFactor(self):
        # Find the greatest absolute value of all signals
        # that we plot (two at most)
        higher = 0
        for wave in self.signals.itervalues():
          tmp = max(max(wave), abs(min(wave)))
          if (tmp > higher):
            higher = tmp

        if (higher):
          self.scalingFactor = ((self.height() - 10) / 2)  / higher
        else:
          self.scalingFactor = 1


    def paintEvent(self, event):

        p = QPainter(self)

        # Draw white background
        p.setBrush(QBrush(QColor(Qt.white)))
        p.drawRect(event.rect())

        #p.drawLine(0, 0, self.width() - 1, self.height() - 1)

        # Simulate real-time acquisition
        # TODO: move it out
        for wave in self.signals.itervalues():
          t = wave.pop(0)
          wave.append(t)

        p.setBrush(QBrush(QColor(Qt.black)))
        # Draw label
        #p.setPen()
        #p.setFont()
        left = 2
        top = 20
        for labels in self.signals:
          p.drawText(left, top, labels)
          top += 20

# TODO: Fix step time based on the width of the widget
        for wave in self.signals.itervalues():
          # Draw actual signal
          for time, value in enumerate(wave):
            p.drawPoint(QPointF(time, (self.scalingFactor*value) + self.offset))

class Plot2d(QWidget):

    def __init__(self, parent=None):
        super(Plot2d, self).__init__()

        # Default scope size
        width = 320
        height = 240

        # Create scope with the actual draw area
        self.scope = Plot2dArea()
        self.scope.resize(width, height)
        self.scope.show()
        # Attach zoom buttons
        self.zoomIn = QPushButton("+")
        self.zoomOut = QPushButton("-")
        QObject.connect(self.zoomIn, SIGNAL("clicked()"), self.scope.zoomIn)
        QObject.connect(self.zoomOut, SIGNAL("clicked()"), self.scope.zoomOut)
        self.zoomIn.show()
        self.zoomOut.show()
        # Lay everything into a layout
        self.hlayout = QHBoxLayout()
        self.hlayout.addWidget(self.zoomIn)
        self.hlayout.addWidget(self.zoomOut)

        self.vlayout = QVBoxLayout()
        self.vlayout.addWidget(self.scope)
        self.vlayout.addLayout(self.hlayout)

        self.setLayout(self.vlayout)

    def attachSignal(self, label, signal):
        print "Scope: attaching signal ", label
        self.scope.attachSignal(label, signal)

if __name__ == "__main__":

    x1 = [math.sin(t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
    x2 = [math.sin(t)+random.random() for t in numpy.linspace(0.0, 2*math.pi, num=300)]

    y1 = [math.cos(2*t) + math.sin(t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
    y2 = [math.cos(2*t) + math.sin(t) + random.random() for t in numpy.linspace(0.0, 2*math.pi, num=300)]

    z1 = [math.sin(2*t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
    z2 = [math.sin(2*t) + random.random() for t in numpy.linspace(0.0, 2*math.pi, num=300)]

    app = QApplication([])

    vlayout = QVBoxLayout()

    scope_x = Plot2d()
    scope_x.attachSignal(label="x1", signal=x1)
    scope_x.attachSignal(label="x2", signal=x2)
    scope_x.show()
    vlayout.addWidget(scope_x)

    scope_y = Plot2d()
    scope_y.attachSignal(label="y1", signal=y1)
    scope_y.attachSignal(label="y2", signal=y2)
    scope_y.show()
    vlayout.addWidget(scope_y)

    scope_z = Plot2d()
    scope_z.attachSignal(label="z1", signal=z1)
    scope_z.attachSignal(label="z2", signal=z2)
    scope_z.show()
    vlayout.addWidget(scope_z)

    window = QWidget()
    window.setLayout(vlayout)
    window.show()

    app.exec_()
