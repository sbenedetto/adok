#!/usr/bin/env python

from OpenGL.GL import *
from OpenGL.GLU import *
from PyQt4.Qt import *
from PyQt4.QtOpenGL import *

def emitCube():

    # XXX: uses OpenGL immediate mode :/

    glBegin(GL_QUADS)

    # front
    glColor3f(1.0, 0.0, 0.0)
    glVertex3f(-0.5, -0.5, 0.5)
    glVertex3f( 0.5, -0.5, 0.5)
    glVertex3f( 0.5, 0.5, 0.5)
    glVertex3f(-0.5, 0.5, 0.5)
    
    # back
    glColor3f(1.0, 0.0, 1.0)
    glVertex3f(-0.5, -0.5, -0.5)
    glVertex3f(-0.5, 0.5, -0.5)
    glVertex3f( 0.5, 0.5, -0.5)
    glVertex3f( 0.5, -0.5, -0.5)

    # left
    glColor3f(0.0, 1.0, 0.0)
    glVertex3f(-0.5, -0.5, 0.5)
    glVertex3f(-0.5, 0.5, 0.5)
    glVertex3f(-0.5, 0.5, -0.5)
    glVertex3f(-0.5, -0.5, -0.5)
    # right
    glColor3f(1.0, 1.0, 0.0)
    glVertex3f( 0.5, -0.5, -0.5)
    glVertex3f( 0.5, 0.5, -0.5)
    glVertex3f( 0.5, 0.5, 0.5)
    glVertex3f( 0.5, -0.5, 0.5)

    # top
    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(-0.5, 0.5, 0.5)
    glVertex3f( 0.5, 0.5, 0.5)
    glVertex3f( 0.5, 0.5, -0.5)
    glVertex3f(-0.5, 0.5, -0.5)

    # bottom
    glColor3f(0.0, 1.0, 1.0)
    glVertex3f(-0.5, -0.5, 0.5)
    glVertex3f(-0.5, -0.5, -0.5)
    glVertex3f( 0.5, -0.5, -0.5)
    glVertex3f( 0.5, -0.5, 0.5)
    glEnd()

class View3D(QGLWidget):
    def __init__(self, *args):
        QGLWidget.__init__(self, *args)
        self.basepress = None
        self.rx = 0.0
        self.ry = 0.0

    def initializeGL(self):
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)

    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, 1.0 * (w / h), 1.0, 100.0)
        gluLookAt(0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def paintGL(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glRotatef(self.rx, 1.0, 0.0, 0.0)
        glRotatef(self.ry, 0.0, 1.0, 0.0)

        emitCube()

    def mousePressEvent(self, event):
        self.basepress = (event.x(), event.y())

    def mouseReleaseEvent(self, event):
        self.basepress = None

    def mouseMoveEvent(self, event):
        if self.basepress is None:
            return

        x, y = self.basepress
        self.basepress = (event.x(), event.y())
        dx = event.x() - x
        dy = event.y() - y

        self.ry += dx
        self.rx += dy

        self.update()


class Widget(QWidget):
    def __init__(self, *args):
        QWidget.__init__(self, *args)
        self.layout = QGridLayout(self)
        self.layout.addWidget(View3D(), 0, 0)
        self.layout.addWidget(View3D(), 0, 1)
        self.layout.addWidget(View3D(), 1, 0)
        self.layout.addWidget(View3D(), 1, 1)

if __name__ == "__main__":
    app = QApplication([])
    mw = Widget(None)
    mw.resize(400, 400)
    mw.show()
    app.exec_()

