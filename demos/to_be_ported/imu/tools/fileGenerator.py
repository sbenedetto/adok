#!/usr/bin/env python


import numpy
import math
import random


ax = [math.sin(t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
ex = [math.sin(t)+(random.random() - 0.5) for t in numpy.linspace(0.0, 2*math.pi, num=300)]

ay = [math.cos(2*t) + math.sin(t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
ey = [math.cos(2*t) + math.sin(t) + (random.random() - 0.5) for t in numpy.linspace(0.0, 2*math.pi, num=300)]

az = [math.sin(2*t) for t in numpy.linspace(0.0, 2*math.pi, num=300)]
ez = [math.sin(2*t) + (random.random() - 0.5) for t in numpy.linspace(0.0, 2*math.pi, num=300)]

pitch = [i for i in range(300)]
roll = [i for i in range(300)]

print ax
print ex


testFile = open("dataSet.txt", "w")
# Create string
for i in range(len(ax)):
    data = "#ax=" + str(ax[i]) + " ay=" + str(ay[i]) + " az=" + str(az[i])
    data += " ex=" + str(ex[i]) + " ey=" + str(ey[i]) + " ez=" + str(ez[i])
    data += " pitch=" + str(pitch[i]) + " roll=" + str(roll[i])
    data += "\n"
    testFile.write(data)

testFile.close()
