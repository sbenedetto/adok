from OpenGL.GL import *
from OpenGL.GLU import *
from PyQt4.Qt import *
from PyQt4.QtOpenGL import *


def drawAx(base, width, height, origin, color):

    glBegin(GL_QUADS)

    glColor3f(color[0], color[1], color[2])
    # Front face
    glVertex3f(origin[0],        origin[1],          origin[2])
    glVertex3f(origin[0] + base, origin[1],          origin[2])
    glVertex3f(origin[0] + base, origin[1] + height, origin[2])
    glVertex3f(origin[0],        origin[1] + height, origin[2])
    # Left
    glVertex3f(origin[0], origin[1],          origin[2])
    glVertex3f(origin[0], origin[1] + height, origin[2])
    glVertex3f(origin[0], origin[1] + height, origin[2] - width)
    glVertex3f(origin[0], origin[1],          origin[2] - width)
    # Right
    glVertex3f(origin[0] + base, origin[1],          origin[2])
    glVertex3f(origin[0] + base, origin[1] + height, origin[2])
    glVertex3f(origin[0] + base, origin[1] + height, origin[2] - width)
    glVertex3f(origin[0] + base, origin[1],          origin[2] - width)
    # Top
    glVertex3f(origin[0],        origin[1] + height, origin[2])
    glVertex3f(origin[0] + base, origin[1] + height, origin[2])
    glVertex3f(origin[0] + base, origin[1] + height, origin[2] - width)
    glVertex3f(origin[0],        origin[1] + height, origin[2] - width)
    # Bottom
    glVertex3f(origin[0],        origin[1],          origin[2])
    glVertex3f(origin[0] + base, origin[1],          origin[2])
    glVertex3f(origin[0] + base, origin[1],          origin[2] - width)
    glVertex3f(origin[0],        origin[1],          origin[2] - width)
    # Back
    glVertex3f(origin[0],        origin[1],          origin[2] - width)
    glVertex3f(origin[0] + base, origin[1],          origin[2] - width)
    glVertex3f(origin[0] + base, origin[1] + height, origin[2] - width)
    glVertex3f(origin[0],        origin[1] + height, origin[2] - width)

    glEnd()

def emitOrientationAxis():

    origin = [0.0, 0.0, 0.0]
    # X
    color = [1.0, 0.0, 0.0]
    base = 0.2
    width = 0.2
    height = 1.5
    drawAx(base, width, height, origin, color)
    # Y
    color = [0.0, 1.0, 0.0]
    base = 1.5
    width = 0.2
    height = 0.2
    drawAx(base, width, height, origin, color)
    # Z
    color = [0.0, 0.0, 1.0]
    base = 0.2
    width = -1.5
    height = 0.2
    drawAx(base, width, height, origin, color)

class Plot3D(QGLWidget):
    def __init__(self, *args):
        QGLWidget.__init__(self, *args)
        self.basepress = None
        self.rx = 0.0
        self.ry = 0.0

        self.queue_pitch = []
        self.queue_roll = []

        # Create and attach a timer for periodic redraw
        self.timer = QTimer(self)
        self.connect(self.timer, SIGNAL("timeout()"), self.update)
        self.timer.setInterval(250)
        self.timer.start();

    def initializeGL(self):
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)

    def resizeGL(self, w, h):
        glViewport(0, 0, w, h)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, 1.0 * (w / h), 1.0, 100.0)
        gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def setPitch(self, pitch):
        self.queue_pitch = pitch

    def setRoll(self, roll):
        self.queue_roll = roll

    def paintGL(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        # Ugly as hell. If we get here between the pop and the append
        # of the dispatcher we'll crash nicely. Lock is needed.
        self.rx = self.queue_roll[1]
        self.ry = self.queue_pitch[1]

        glRotatef(self.rx, 1.0, 0.0, 0.0)
        glRotatef(self.ry, 0.0, 0.0, 1.0)

        emitOrientationAxis()

if __name__ == "__main__":
    app = QApplication([])

    scope = Plot3D()
    scope.show()

    app.exec_()

