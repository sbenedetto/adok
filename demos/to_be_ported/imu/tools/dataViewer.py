from PyQt4.Qt import *

class DataViewer(QWidget):

  def __init__(self, parent=None):
    super(DataViewer, self).__init__()

    # ax
    self.ax = QLabel()
    self.ax.setText(QString("ax: "))
    self.ax.show()

    # ay
    self.ay = QLabel()
    self.ay.setText(QString("ay: "))
    self.ay.show()

    # az
    self.az = QLabel()
    self.az.setText(QString("az: "))
    self.az.show()

    # ex
    self.ex = QLabel()
    self.ex.setText(QString("ex: "))
    self.ex.show()

    # ey
    self.ey = QLabel()
    self.ey.setText(QString("ey: "))
    self.ey.show()

    # ez
    self.ez = QLabel()
    self.ez.setText(QString("ez: "))
    self.ez.show()

    # pitch
    self.pitch = QLabel()
    self.pitch.setText(QString("pitch: "))
    self.pitch.show()

    # roll
    self.roll = QLabel()
    self.roll.setText(QString("roll: "))
    self.roll.show()

    # yaw
    self.yaw = QLabel()
    self.yaw.setText(QString("yaw: "))
    self.yaw.show()

    self.mainLayout = QHBoxLayout()
    self.mainLayout.addWidget(self.ax)
    self.mainLayout.addWidget(self.ay)
    self.mainLayout.addWidget(self.az)
    self.mainLayout.addWidget(self.ex)
    self.mainLayout.addWidget(self.ey)
    self.mainLayout.addWidget(self.ez)
    self.mainLayout.addWidget(self.pitch)
    self.mainLayout.addWidget(self.roll)
    self.mainLayout.addWidget(self.yaw)

    self.setLayout(self.mainLayout)

  def updateData(self, variable, value):
    if (variable == "ax"):
      self.ax.setText(QString("ax: ") + QString(value))

    elif (variable == "ay"):
      self.ay.setText(QString("ay: ") + QString(value))

    elif (variable == "az"):
      self.az.setText(QString("az: ") + QString(value))

    elif (variable == "ex"):
      self.ex.setText(QString("ex: ") + QString(value))

    elif (variable == "ey"):
      self.ey.setText(QString("ey: ") + QString(value))

    elif (variable == "ez"):
      self.ez.setText(QString("ez: ") + QString(value))

    elif (variable == "pitch"):
      self.pitch.setText(QString("pitch: ") + QString(value))

    elif (variable == "roll"):
      self.roll.setText(QString("roll: ") + QString(value))

    elif (variable == "yaw"):
      self.yaw.setText(QString("yaw: ") + QString(value))

    else:
      print variable, " ", value

