import Queue
import threading

from PyQt4.Qt import *

class Dispatcher(threading.Thread):

  def __init__(self, queue=None):
    threading.Thread.__init__(self)
    self.queue = queue
    self.signals = {}

  def attachSignal(self, label, signal):
      print "Dispatcher: attaching signal ", label
      self.signals[label] = signal

  def register(self, viewer):
    self.viewer = viewer

  def setTextViewer(self, textViewer):
      self.textViewer = textViewer

  def run(self):
    print "Dispatcher: running"
    self.keepRunning = True

    while (self.keepRunning):
      # Read the incoming message
      current_message = self.queue.get()
      if len(current_message) == 0:
          continue

      current_message = current_message.lstrip()
      if len(current_message) == 0:
          continue

      # If first character is not a '#' the message contains
      # simple data
      if (current_message[0] != '#'):
        # Put character in TextView
        print "Log: ", current_message
        self.textViewer.append(QString(current_message))
      else:
        current_message = current_message.lstrip("#")
        # Split the message as needed
        # Update timestamp
        # Message is formatted as "var=value var2=value2 ..."
        list_of_variables = current_message.rsplit()

        for variables in list_of_variables:
          separator = variables.rfind('=')
          name = variables[ : separator]
          value = variables[separator + 1 : ]
          # Very coupled...
          self.viewer.updateData(name, value)
          # TODO: lock/unlock
          # Lock list = DictionaryList[var]
          try:
            tmp = self.signals[name]
          except KeyError:
            continue

          tmp.pop(0)
          tmp.append(float(value))
          # Unlock list = DictionaryList[var]

      # Check timestamp of last update listx, listy, listz
      # If more than threshold put a zero in each list
      # TODO: iteratate through dictionaryList and place a zero

  def stop(self):
    self.keepRunning = False

if __name__ == '__main__':
    pass
