/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

class I2C;


class L3G4200D
{
public:

  L3G4200D(I2C* i2c);

  void    init(uint8_t* chipID);

  void    readGyroscopeData(int16_t& x, int16_t& y, int16_t& z);
  void    readTemperature(int8_t& temperature);

private:

  L3G4200D();
  L3G4200D(const L3G4200D& );

  int8_t   readRegister(uint8_t address);
  void     writeRegister(uint8_t address, int8_t value);

  I2C* i2c_;
};
