/*
 * This file is part of the opencortex-m project.
 *
 * Copyright (C) 2011 Salvatore Benedetto <salvatore.benedetto@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <afio.h>
#include <bus/i2c.h>
#include <clock.h>
#include <externalInterrupt.h>
#include <gpio.h>
#include <nvic.h>
#include <utility/delay.h>

#include <stdio.h>
#include <math.h>

#include "l3g4200d.h"

extern void low_level_init();
extern void trace(const char* buffer);


int main(void)
{
  low_level_init();

  trace("\n\n\n===== L3G200D DEMO APPLICATION =====\n");

  /* Enable clock for peripherals used */
  clock::enableClockFor(clock::eI2C1);
  clock::enableClockFor(clock::eGPIOB);
  clock::enableClockFor(clock::eGPIOC);
  clock::enableClockFor(clock::eAFIO);

  /* GPIO C - Configure status led */
  GPIO gpioPortC(GPIO::ePortC);
  gpioPortC.setMode(GPIO::ePin12, GPIO::eOutputPushPull10Mhz);

  /* GPIO B - Configure I2C1 GPIO port pin to correct function */
  GPIO gpioPortB(GPIO::ePortB);
  /* SCL */
  gpioPortB.setMode(GPIO::ePin6, GPIO::eAlternateFunctionOpenDrain2Mhz);
  /* SDA */
  gpioPortB.setMode(GPIO::ePin7, GPIO::eAlternateFunctionOpenDrain2Mhz);

  /* Configure I2C1 */
  I2C i2c(I2C::eI2C1);
  i2c.enableAcknowledge();
  i2c.setMode(I2C::eStandard100Khz);
  i2c.enable();

  NVIC nvic;
  nvic.enableSysTick();

  /* Led off */
  gpioPortC.high(GPIO::ePin12);

  L3G4200D gyroscope(&i2c);
  uint8_t chipID = 0;

  util::delayms(10);
  while (1) {

    gyroscope.init(&chipID);

    if (chipID == 0xD3) {

      trace("Chip ID found! Start querying for data...\n");

      int16_t x = 0;
      int16_t y = 0;
      int16_t z = 0;
      float wxz = 0;
      float wyz = 0;
      float wxy = 0;
      int32_t x0 = 0;
      int32_t y0 = 0;
      int32_t z0 = 0;
      float xz = 0;
      float yz = 0;
      float xy = 0;
      int8_t temperature = 0;
      char bufferOut[256];
      float timeElapsed = 0;
      uint64_t previousValue = util::currentCounterValue();

      /* Calculate zero-level value first */
      const uint8_t kSamples = 200;
      for (uint8_t i = 0; i < kSamples; i++) {
        gyroscope.readGyroscopeData(x, y, z);
        sprintf(bufferOut, "Zero level value: x = %d - y = %d - z = %d\n", x0, y0, z0);
        trace(bufferOut);
        x0 += x;
        y0 += y;
        z0 += z;
      }
      x0 /= kSamples;
      y0 /= kSamples;
      z0 /= kSamples;
      sprintf(bufferOut, "Zero level value: x = %d - y = %d - z = %d\n", x0, y0, z0);
      trace(bufferOut);

      const float kThreshold = 100;
      int32_t xPrevious = x0;
      int32_t yPrevious = y0;
      int32_t zPrevious = z0;
      while (1) {

        int32_t tmp = 0;
        gyroscope.readGyroscopeData(x, y, z);

        // Data rate is 100Hz, thus T = 0.01s
        uint64_t currentValue = util::currentCounterValue();
        uint32_t delta = currentValue - previousValue;
        timeElapsed = (float)delta / 1000000;

        previousValue = currentValue;

        /* Convert data to dps */
        if ((x - x0) > kThreshold || (x - x0) < -kThreshold) {
          tmp = x;
          x = (x + xPrevious) / 2;
          wxz = (x - x0) * 0.00875;
          xPrevious = tmp;
        } else
          wxz = 0;

        if ((y - y0) > kThreshold || (y - y0) < -kThreshold) {
          tmp = y;
          y = (y + yPrevious) / 2;
          wyz = (y - y0) * 0.00875;
          yPrevious = tmp;
        } else
          wyz = 0;

        if ((z - z0) > kThreshold || (z - z0) < -kThreshold) {
          tmp = z;
          z = (z + zPrevious) / 2;
          wxy = (z - z0) * 0.00875;
          zPrevious = tmp;
        } else
          wxy = 0;

        xz += (wxz * timeElapsed);
        yz += (wyz * timeElapsed);
        xy += (wxy * timeElapsed);

        sprintf(bufferOut, "#wxz=%+f wyz=%+f wxy=%+f xz=%+f yz=%+f xy=%+f\n",
                wxz, wyz, wxy, xz, yz, xy);
        trace(bufferOut);

        gpioPortC.toggle(GPIO::ePin12);
        /* Sensor data rate is set at 100Hz. */
        //util::delayms(1);
      }

    } else {

      trace("Chip ID NOT found! Perhaps sensor is not connected...\n");
      while (1) {
        gpioPortC.toggle(GPIO::ePin12);
        util::delayms(1000);
      }
    }
  }

  return 0;
}
