from PyQt4.Qt import *

from serialReader import *
from dispatcher import *
from dataViewer import *

import Queue

queue = Queue.Queue(0)
# Producer
serialReader = SerialReader(queue)
# Consumer
dispatcher = Dispatcher(queue)

app = QApplication([])
dataViewer = DataViewer()
dataViewer.show()

dispatcher.register(dataViewer)

dispatcher.start()
serialReader.start()

app.exec_()

serialReader.stop()
dispatcher.stop()
