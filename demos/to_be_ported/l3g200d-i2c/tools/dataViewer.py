from PyQt4.Qt import *

class DataViewer(QWidget):

  def __init__(self, parent=None):
    super(DataViewer, self).__init__()

    # WXY
    self.wxy = QLabel()
    self.wxy.setText(QString("wxy: "))
    self.wxy.show()

    self.wxyValue = QLabel()
    self.wxyValue.setText(QString.number(0))
    self.wxyValue.show()

    self.wxyLayout = QHBoxLayout()
    self.wxyLayout.addWidget(self.wxy)
    self.wxyLayout.addWidget(self.wxyValue)

    # WXZ
    self.wxz = QLabel()
    self.wxz.setText(QString("wxz: "))
    self.wxz.show()

    self.wxzValue = QLabel()
    self.wxzValue.setText(QString.number(0))
    self.wxzValue.show()

    self.wxzLayout = QHBoxLayout()
    self.wxzLayout.addWidget(self.wxz)
    self.wxzLayout.addWidget(self.wxzValue)

    # WYZ
    self.wyz = QLabel()
    self.wyz.setText(QString("wyz: "))
    self.wyz.show()

    self.wyzValue = QLabel()
    self.wyzValue.setText(QString.number(0))
    self.wyzValue.show()

    self.wyzLayout = QHBoxLayout()
    self.wyzLayout.addWidget(self.wyz)
    self.wyzLayout.addWidget(self.wyzValue)

    # XY
    self.xy = QLabel()
    self.xy.setText(QString("xy: "))
    self.xy.show()

    self.xyValue = QLabel()
    self.xyValue.setText(QString.number(0))
    self.xyValue.show()

    self.xyLayout = QHBoxLayout()
    self.xyLayout.addWidget(self.xy)
    self.xyLayout.addWidget(self.xyValue)

    # XZ
    self.xz = QLabel()
    self.xz.setText(QString("xz: "))
    self.xz.show()

    self.xzValue = QLabel()
    self.xzValue.setText(QString.number(0))
    self.xzValue.show()

    self.xzLayout = QHBoxLayout()
    self.xzLayout.addWidget(self.xz)
    self.xzLayout.addWidget(self.xzValue)

    # YZ
    self.yz = QLabel()
    self.yz.setText(QString("yz: "))
    self.yz.show()

    self.yzValue = QLabel()
    self.yzValue.setText(QString.number(0))
    self.yzValue.show()

    self.yzLayout = QHBoxLayout()
    self.yzLayout.addWidget(self.yz)
    self.yzLayout.addWidget(self.yzValue)

    self.mainLayout = QVBoxLayout()
    self.mainLayout.addLayout(self.wxyLayout)
    self.mainLayout.addLayout(self.wxzLayout)
    self.mainLayout.addLayout(self.wyzLayout)
    self.mainLayout.addLayout(self.xyLayout)
    self.mainLayout.addLayout(self.xzLayout)
    self.mainLayout.addLayout(self.yzLayout)

    self.setLayout(self.mainLayout)

  def updateData(self, variable, value):
    if (variable == "wxy"):
      self.wxyValue.setText(QString(value))

    elif (variable == "wxz"):
      self.wxzValue.setText(QString(value))

    elif (variable == "wyz"):
      self.wyzValue.setText(QString(value))

    elif (variable == "xy"):
      self.xyValue.setText(QString(value))

    elif (variable == "xz"):
      self.xzValue.setText(QString(value))

    elif (variable == "yz"):
      self.yzValue.setText(QString(value))

    else:
      print variable, " ", value

