------------------------------
           TODO
------------------------------

The following is a list of things to do,
hopefully always updated.

* If doxygen is missing waf will fail the configure step. This is clearly
  not the expected behavior
* Add compiler optimization for constant propagation. Currently -fipa-cp
  is used but we have to make sure that's the one.
* Build script lacks a pdf / html procedure for doxygen documentation.
  - Why do we generate a tar.bz2 and no simple documentation?
* C++ enviroment initialization: global objct are not (currently) allowed
* Extend NVIC class for STM32 support
* SPI support (needed for sensors)
* I2C support (needed for sensors)


== SPI ==
* Add CRC support
* Add DMA support

== GPIO ==
* Add remapping support
