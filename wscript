#! /usr/bin/env python
# encoding: utf-8

VERSION='0.0.1'
APPNAME='adok'

top = '.'
out = 'build'

def options(ctx):

    # Let the user choose the scheduler to compile in adokcore
    # TODO: this should perhaps go into wscript of adokcore
    buildOptions = ctx.get_option_group('build and install options')
    buildOptions.add_option('--scheduler',
                    type='choice',
                    choices=['edf', 'roundrobin'],
                    default='edf',
                    action='store',
                    help='define scheduler to use [default=EDF]')

    # Let the user choose the compiler to use for builingn adok
    conf = ctx.get_option_group('configure options')
    conf.add_option('-c',
                  '--compiler',
                   type='choice',
                   choices=['clang', 'gcc'],
                   default='clang',
                   action='store',
                   help='define compiler to use. clang or gcc [default: clang]')

def configure(ctx):

    # Select compiler
    common_flags = [#'-fdata-sections',
                    #'-ffunction-sections',
                    '-fno-common',
                    '-fno-exceptions',
                    '-fno-rtti',
                    '-g',
                    '-nostdlib',
                    '-nodefaultlibs',
                    '-Wall']
    compiler_specific_flags = []

    if ctx.options.compiler == 'clang':
       ctx.find_program('clang++', var = 'CXX', mandatory = True)
       compiler_specific_flags += ['-ccc-gcc-name',
                                   'arm-none-linux-gnueabi-g++',
                                   '-target',
                                   '-armv7m-none-gnueabi',
                                   '-stdlib=libc++',
                                   '-mcpu=cortex-m3',
                                   '-mthumb',
                                   '-I/home/emitrax/bin/llvm/lib/c++/v1',
                                   '-std=c++11']

    else:
       ctx.find_program('arm-none-linux-gnueabi-g++',
                        var = 'CXX', mandatory = True)
       compiler_specific_flags += ['-mthumb',
                                   '-mcpu=cortex-m3',
                                   '-march=armv7',
                                   '-mfix-cortex-m3-ldrd',
                                   '-std=c++0x']
    # Compiler options
    ctx.env.CXXFLAGS = common_flags + compiler_specific_flags

    # Select ar archives
    # Load the tool after setting the AR and CXX variable
    ctx.find_program('arm-none-linux-gnueabi-ar',
                      var = 'AR', mandatory = True)
    ctx.load('gxx')

    # Select objcopy
    # Load the tool 'after' setting the OBJCOPY var
    ctx.find_program('arm-none-linux-gnueabi-objcopy',
                      var = 'OBJCOPY', mandatory = True)
    ctx.load('objcopy')

    # Select assembly
    # Load the tool 'after' setting the AS var
    ctx.find_program('arm-none-linux-gnueabi-as',
                      var = 'AS', mandatoru = True)
    ctx.load('gas')

    # Tools for documentation
    # ctx.load('doxygen')
    # ctx.load('tex')

    ctx.env.ASFLAGS = '-mcpu=cortex-m3'

    # Linker options
    ctx.env['SHLIB_MARKER'] = ''
    ctx.env['STLIB_MARKER'] = '-static'
    ctx.env.LINKFLAGS = ['-L%s' % ctx.path.find_dir('link').abspath(),
                         '-nostartfiles']

    if ctx.options.compiler == 'clang':
      ctx.env.append_value('LINKFLAGS',
                          ['-target', 'thumbv7m-none-gnueabi',
                           '-ccc-gcc-name', 'arm-none-linux-gnueabi-g++',
                           '-mcpu=cortex-m3', '-mthumb'])

    from waflib.Tools import c_preproc
    c_preproc.go_absolute = True

def build(bld):
    bld.recurse('source')

# Create a build libs command
def buildLibs(bld):
    # Recursively call the build function in the directory libs
    bld.recurse('libs', name='build')

from waflib.Build import BuildContext
class BuildLibs(BuildContext):
    cmd = 'libs'
    fun = 'buildLibs'

from waflib.Build import InstallContext
class BuildLibs(InstallContext):
    cmd = 'install_libs'
    fun = 'buildLibs'

# Create a build demos command
def buildDemos(bld):
    # Recursively call the build function in the directory demos
    bld.recurse('demos', name='build')

from waflib.Build import BuildContext
class BuildDemos(BuildContext):
    cmd = 'demos'
    fun = 'buildDemos'


# Provide rules for interruptRoutines
# This is executed in the task_gen context
# TODO: update this
from waflib import TaskGen
from waflib.Errors import WafError
@TaskGen.feature('interruptRoutines')
@TaskGen.before('process_source', 'process_use')
def add_interrupt_routines(self):
  if self.path.find_node('interruptRoutines.cpp'):
    if type(self.source).__name__ == 'list':
      self.source.append('interruptRoutines.cpp')
    else:
      self.source += ' interruptRoutines.cpp '
  else:
    try:
      self.bld.get_tgen_by_name('interruptRoutines')
    except WafError:
      self.bld.objects(source='source/interruptRoutines.cpp',
                       name='interruptRoutines')


# Change the naming convention for object files.
from waflib.TaskGen import extension
from waflib.Tools import cxx
@extension('.cpp')
def create_the_c_task(self, node):
    tsk = self.create_compiled_task('cxx', node)
    out = node.parent.find_or_declare(node.name + '.o')
    tsk.outputs = [out]
    return tsk


# BuildContext for Doxygen
from waflib import Build
class DocGen(Build.BuildContext):
    cmd = 'docgen'
    fun = 'docgen'


# Build command for doxygen
def docgen(doc):
    doc(features='doxygen',
        doxyfile='doc/doxygen.conf',
        doxy_tar='opencortex-m-docs.tar.bz2')


# BuildContext for Guidelines
class Guidelines(Build.BuildContext):
    cmd = 'guidelines'
    fun = 'guidelines'


# Build command for codingGuidelines
def guidelines(guidelines):
    guidelines.recurse('doc')
