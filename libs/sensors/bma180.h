//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef __BMA180_H__
#define __BMA180_H__

#include <stdint.h>

class SPI;


class BMA180
{
public:

  BMA180(SPI* spi);

  void    init();
  bool    isOnBus(int8_t chipID);

  void    disableI2C();

  void    enableNewDataInterrupt();
  bool    newDataInterruptEnabled();

  void    readAccelerationData(int16_t& x, int16_t& y, int16_t& z);
  void    readTemperature(int8_t& temperature);

  void    softReset();

private:

  BMA180();
  BMA180(const BMA180&);

  int8_t    readRegister(uint8_t address);
  int8_t    readFirst(uint8_t address);
  int8_t    readNext();
  void      writeRegister(uint8_t address, int8_t value);

  SPI* spi_;
};


#endif // __BMA180_H__
