//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "l3g4200d.h"

#include <drivers/stm32/gpio.h>
#include <drivers/stm32/i2c.h>
#include <drivers/nvic.h>


static const uint8_t gkWhoAmI                 = 0x0F;
static const uint8_t gkControlRegister1       = 0x20;
static const uint8_t gkControlRegister2       = 0x21;
static const uint8_t gkControlRegister3       = 0x22;
static const uint8_t gkControlRegister4       = 0x23;
static const uint8_t gkControlRegister5       = 0x24;
static const uint8_t gkReference              = 0x25;
static const uint8_t gkTemperature            = 0x26;
static const uint8_t gkStatusRegister         = 0x27;
static const uint8_t gkXLSB                   = 0x28;
static const uint8_t gkXMSB                   = 0x29;
static const uint8_t gkYLSB                   = 0x2A;
static const uint8_t gkYMSB                   = 0x2B;
static const uint8_t gkZLSB                   = 0x2C;
static const uint8_t gkZMSB                   = 0x2D;
static const uint8_t gkFIFOControlRegister    = 0x2E;
static const uint8_t gkFIFOSourceRegister     = 0x2F;
static const uint8_t gkInterruptConfiguration = 0x30;
static const uint8_t gkInterruptSource        = 0x31;


L3G4200D::L3G4200D(I2C* i2c)
  : i2c_(i2c)
{
  // Never do anything in the constructor
}


void
L3G4200D::init()
{
  // Make sure device has finished power up
  NVIC::activeDelayMS(5);
  // Set data rate (100Hz), bandwidth (12.5Hz), power mode and axis enabled (all)
  writeRegister(gkControlRegister1, 0x0F);

  // Enable interrupt on data ready
  uint8_t tmp = readRegister(gkControlRegister3);
  tmp |= 0x08;
  writeRegister(gkControlRegister3, tmp);

  // Set block data update (BDU) feature in order not
  // read LSB and MSB of different samples.
  // Set scale selection to 250dps, and little endian mode
  writeRegister(gkControlRegister4, 0x80);

  // Set digital filters. Apply low-pass filter
  tmp = readRegister(gkControlRegister5);
  // Make sure high pass filter is disabled
  tmp &= ~(0x01 << 4);
  // Data are low pass filtered
  tmp |= 0x02;
  writeRegister(gkControlRegister5, tmp);
}


bool
L3G4200D::isOnBus(int8_t chipID)
{
  bool found = false;

  if (readRegister(gkWhoAmI) == chipID)
    found = true;

  return found;
}


void
L3G4200D::readGyroscopeData(int16_t& x, int16_t& y, int16_t& z)
{
  int8_t tmp = 0;

  tmp = readRegister(gkXLSB);
  x = tmp;
  tmp = readRegister(gkXMSB);
  x |= tmp << 8;

  tmp = readRegister(gkYLSB);
  y = tmp;
  tmp = readRegister(gkYMSB);
  y |= tmp << 8;

  tmp = readRegister(gkZLSB);
  z = tmp;
  tmp = readRegister(gkZMSB);
  z |= tmp << 8;
}


void
L3G4200D::readTemperature(int8_t& temperature)
{
  temperature = readRegister(gkTemperature);
}


int8_t
L3G4200D::readRegister(uint8_t address)
{
  int8_t value = 0;
  const uint8_t numberOfBytes = 1;

  i2c_->readFrom(0xD3 /* slave address read */,
                 0xD2 /* slave address write*/,
                 address, &value, numberOfBytes);

  return value;
}


void
L3G4200D::writeRegister(const uint8_t address, const int8_t value)
{
  const uint8_t numberOfBytes = 1;
  i2c_->writeTo(0xD2 /* slave address write */,
                address, &value, numberOfBytes);
}
