//
// This file is part of the adok project.
//
// Copyright (C) 2012 Salvatore Benedetto <salvatore.benedetto@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "bma180.h"

// TODO: fix this once an HAL is in place for GPIO and SPI
#include <drivers/stm32/gpio.h>
#include <drivers/stm32/spi.h>
#include <drivers/nvic.h>

static const uint8_t gkChipID           = 0x00;
static const uint8_t gkVersion          = 0x01;
static const uint8_t gkXLSB             = 0x02;
static const uint8_t gkXMSB             = 0x03;
static const uint8_t gkYLSB             = 0x04;
static const uint8_t gkYMSB             = 0x05;
static const uint8_t gkZLSB             = 0x06;
static const uint8_t gkZMSB             = 0x07;
static const uint8_t gkTemperature      = 0x08;
static const uint8_t gkStatusRegister1  = 0x09;
static const uint8_t gkStatusRegister2  = 0x0A;
static const uint8_t gkStatusRegister3  = 0x0B;
static const uint8_t gkStatusRegister4  = 0x0C;
static const uint8_t gkControlRegister0 = 0x0D;
static const uint8_t gkControlRegister1 = 0x0E;
static const uint8_t gkControlRegister2 = 0x0F;
static const uint8_t gkReset            = 0x10;
static const uint8_t gkBandwidth        = 0x20;
static const uint8_t gkControlRegister3 = 0x21;
static const uint8_t gkDisableI2C       = 0x27;
static const uint8_t gkModeConfig       = 0x30;
static const uint8_t gkOffsetXAndRange  = 0x35;


BMA180::BMA180(SPI* spi)
  : spi_(spi)
{
}


void
BMA180::init()
{
  // Set range to +-2g
  uint8_t range = readRegister(gkOffsetXAndRange);
  range |= 0x04;
  writeRegister(gkOffsetXAndRange, range);
  // Set bandwidth to 20Hz
  uint8_t bandwidth = readRegister(gkBandwidth);
  bandwidth &= ~(0xF0);
  bandwidth |= 0x10;
  writeRegister(gkBandwidth, bandwidth);
  // Set mode_config for low noise mode (Date rate 1200Hz)
  uint8_t modeConfig = readRegister(gkModeConfig);
  modeConfig &= ~(0x03);
  writeRegister(gkModeConfig, modeConfig);
}


bool
BMA180::isOnBus(int8_t chipID)
{
  bool found = false;

  if (readRegister(gkChipID) == chipID)
    found = true;

  return found;
}


void
BMA180::disableI2C()
{
  uint8_t value = readRegister(gkDisableI2C);
  value |= 0x01;
  writeRegister(gkDisableI2C, value);
}


void
BMA180::enableNewDataInterrupt()
{
  // Disable Low & High interrupt
  writeRegister(0x25, 0x00);
  NVIC::activeDelayMS(1);
  // Disable slope and tapsens interrupt
  writeRegister(0x24, 0x00);
  NVIC::activeDelayMS(1);

  // Enable new data interrupt and disable Adv_int
  writeRegister(gkControlRegister3, 0x12);
  NVIC::activeDelayMS(1);
}


bool
BMA180::newDataInterruptEnabled()
{
  uint8_t value = 0;
  value = readRegister(gkControlRegister3);
  return value & 0x02;
}


void
BMA180::readAccelerationData(int16_t& x, int16_t& y, int16_t& z)
{
  int16_t tmp = 0;
  int16_t value = 0;

  GPIO portC(GPIO::ePortC);
  portC.low(GPIO::ePin10);
  NVIC::activeDelayUS(1);

  tmp = readFirst(gkXLSB);
  value = (tmp >> 2) & 0x3F;
  tmp = readNext();
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000; 
  x = value;

  value = 0;
  tmp = readNext();
  value = (tmp >> 2) & 0x3F;
  tmp = readNext();
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000;
  y = value;

  value = 0;
  tmp = readNext();
  value = (tmp >> 2) & 0x3F;
  tmp = readNext();
  value |= (tmp << 6) & 0x3FC0;
  if (tmp & 0x80)
    value |= 0xC000;
  z = value;

  portC.high(GPIO::ePin10);
}


void
BMA180::readTemperature(int8_t& temperature)
{
  temperature = readRegister(gkTemperature);
}


void
BMA180::softReset()
{
  writeRegister(gkReset, 0xB6); /* Special value to write in order to reset */
  //util::delayms(10);
  NVIC::activeDelayMS(10);
}


int8_t
BMA180::readFirst(uint8_t address)
{
  uint8_t value = 0;

  *spi_ << (uint8_t)(0x80 | address);
  // We can't go lower than 40us.
  NVIC::activeDelayUS(50);
  *spi_ >> value;
  return static_cast<int8_t>(value);
}


int8_t
BMA180::readNext()
{
  uint8_t value = 0;
  *spi_ >> value;
  return static_cast<int8_t>(value);
}


int8_t
BMA180::readRegister(uint8_t address)
{
  uint8_t value = 0;
  // Low CS
  GPIO portC(GPIO::ePortC);
  portC.low(GPIO::ePin10);
  //util::delayus(1);
  NVIC::activeDelayUS(1);

  *spi_ << (uint8_t)(0x80 | address);
  //util::delayms(1);
  // We can't go lower than 40us.
  NVIC::activeDelayUS(50);
  *spi_ >> value;

  // High CS
  //util::delayus(1);
  NVIC::activeDelayUS(1);
  portC.high(GPIO::ePin10);
  return static_cast<int8_t>(value);
}


void
BMA180::writeRegister(uint8_t address, int8_t value)
{
  // Low CS
  GPIO portC(GPIO::ePortC);
  portC.low(GPIO::ePin10);
  // util::delayus(1);
  NVIC::activeDelayUS(1);

  *spi_ << address;
// util::delayms(1);
  NVIC::activeDelayUS(50);
  *spi_ << static_cast<uint8_t>(value);

  // High CS
 // util::delayus(1);
  NVIC::activeDelayUS(1);
  portC.high(GPIO::ePin10);
}
