
The following series of command download,
configure and install a cross-compiler (not
the binutils) based on LLVM/Clang

TODO: add commands for binutils

git clone http://llvm.org/git/llvm.git

cd llvm/tools
git clone http://llvm.org/git/clang.git

cd ../llvm/projects
git clone http://llvm.org/git/compiler-rt.git

cd ../llvm/projects
git clone http://llvm.org/git/libcxx.git

../llvm/configure --prefix=$HOME/bin/llvm --exec-prefix=$HOME/bin/llvm  --enable-targets=x86_64,arm --enable-optimized --enable-cxx11 --enable-debug-symbols
make
