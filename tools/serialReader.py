import serial

# Set up serial port
port = serial.Serial()
port.port     = '/dev/ttyUSB0'
port.baudrate = 115200
port.bytesize = 8
port.parity   = 'E'
port.stopbits = 1

port.open()
port.flush()

print("SerialReader: running...")

while (True):
  # Read the incoming message
  message = port.readline()
  print message

